function handleSubmit(args, dialog) {
    var jqDialog = jQuery('#' + dialog);
    if (args.validationFailed) {
        jqDialog.effect('shake', {times: 3}, 100);
    } else {
        PF(dialog).hide();
    }
}

function actionAdd() {
    var a, b, c;
    try {
        a = parseInt(document.getElementById("DepositCash:loanRepayment").value);
    } catch (err) {
    }
    try {
        b = parseInt(document.getElementById("DepositCash:shares").value);
    } catch (err) {
    }
    try {
        c = parseInt(document.getElementById("DepositCash:withrawable").value);
    } catch (err) {
    }
    var result = null;
    if (isNaN(a)) {
        a = 0;
    }
    if (isNaN(b)) {
        b = 0;
    }
    if (isNaN(c)) {
        c = 0;
    }
    result = a + b + c;
    if (result !== null) {
        document.getElementById("DepositCash:lp").value = a;
        document.getElementById("DepositCash:sc").value = b;
        document.getElementById("DepositCash:sd").value = c;
        document.getElementById("DepositCash:total").value = result;
    } else {
        document.getElementById("DepositCash:loanRepayment").value = "";
        document.getElementById("DepositCash:shares").value = "";
        document.getElementById("DepositCash:withrawable").value = "";
    }
}
