package myUtils;

import java.util.concurrent.Executor;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;

@Stateless
public class TransactionalExecutor
  implements Executor
{
  @Asynchronous
  @Override
  public void execute(Runnable command)
  {
    command.run();
  }
}
