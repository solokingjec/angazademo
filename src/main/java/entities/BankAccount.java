
package entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "bank_account")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BankAccount.findAll", query = "SELECT b FROM BankAccount b"),
    @NamedQuery(name = "BankAccount.findById", query = "SELECT b FROM BankAccount b WHERE b.id = :id"),
    @NamedQuery(name = "BankAccount.findByBankAccountsName", query = "SELECT b FROM BankAccount b WHERE b.bankAccountsName = :bankAccountsName"),
    @NamedQuery(name = "BankAccount.findByBankAccountsNumber", query = "SELECT b FROM BankAccount b WHERE b.bankAccountsNumber = :bankAccountsNumber"),
    @NamedQuery(name = "BankAccount.findByBankAccountsBranch", query = "SELECT b FROM BankAccount b WHERE b.bankAccountsBranch = :bankAccountsBranch"),
    @NamedQuery(name = "BankAccount.findByBankAccountsBank", query = "SELECT b FROM BankAccount b WHERE b.bankAccountsBank = :bankAccountsBank")})
public class BankAccount implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "random_int")
    private int randomInt;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "bank_accounts_name")
    private String bankAccountsName;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "bank_accounts_number")
    private String bankAccountsNumber;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "bank_accounts_branch")
    private String bankAccountsBranch;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "bank_accounts_bank")
    private String bankAccountsBank;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "account")
    private Collection<BankAccountTransaction> bankAccountTransactionCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "bankAccount")
    private Collection<GeneralTransaction> generalTransactionCollection;

    public BankAccount() {
    }

    public BankAccount(Integer id) {
        this.id = id;
    }

    public BankAccount(Integer id, String bankAccountsName, String bankAccountsNumber, String bankAccountsBranch, String bankAccountsBank) {
        this.id = id;
        this.bankAccountsName = bankAccountsName;
        this.bankAccountsNumber = bankAccountsNumber;
        this.bankAccountsBranch = bankAccountsBranch;
        this.bankAccountsBank = bankAccountsBank;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBankAccountsName() {
        return bankAccountsName;
    }

    public void setBankAccountsName(String bankAccountsName) {
        this.bankAccountsName = bankAccountsName;
    }

    public String getBankAccountsNumber() {
        return bankAccountsNumber;
    }

    public void setBankAccountsNumber(String bankAccountsNumber) {
        this.bankAccountsNumber = bankAccountsNumber;
    }

    public String getBankAccountsBranch() {
        return bankAccountsBranch;
    }

    public void setBankAccountsBranch(String bankAccountsBranch) {
        this.bankAccountsBranch = bankAccountsBranch;
    }

    public String getBankAccountsBank() {
        return bankAccountsBank;
    }

    public void setBankAccountsBank(String bankAccountsBank) {
        this.bankAccountsBank = bankAccountsBank;
    }

    @XmlTransient
    public Collection<BankAccountTransaction> getBankAccountTransactionCollection() {
        return bankAccountTransactionCollection;
    }

    public void setBankAccountTransactionCollection(Collection<BankAccountTransaction> bankAccountTransactionCollection) {
        this.bankAccountTransactionCollection = bankAccountTransactionCollection;
    }

    @XmlTransient
    public Collection<GeneralTransaction> getGeneralTransactionCollection() {
        return generalTransactionCollection;
    }

    public void setGeneralTransactionCollection(Collection<GeneralTransaction> generalTransactionCollection) {
        this.generalTransactionCollection = generalTransactionCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BankAccount)) {
            return false;
        }
        BankAccount other = (BankAccount) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.BankAccount[ id=" + id + " ]";
    }

    public int getRandomInt() {
        return randomInt;
    }

    public void setRandomInt(int randomInt) {
        this.randomInt = randomInt;
    }

}
