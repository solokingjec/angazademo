/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author francis
 */
@Entity
@Table(name = "withdrawal")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Withdrawal.findAll", query = "SELECT w FROM Withdrawal w"),
    @NamedQuery(name = "Withdrawal.findById", query = "SELECT w FROM Withdrawal w WHERE w.id = :id"),
    @NamedQuery(name = "Withdrawal.findByDateWhen", query = "SELECT w FROM Withdrawal w WHERE w.dateWhen = :dateWhen"),
    @NamedQuery(name = "Withdrawal.findByAmount", query = "SELECT w FROM Withdrawal w WHERE w.amount = :amount"),
    @NamedQuery(name = "Withdrawal.findByWithdrawalcharges", query = "SELECT w FROM Withdrawal w WHERE w.withdrawalcharges = :withdrawalcharges")})
public class Withdrawal implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "status_billed")
    private boolean statusBilled;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "date_when")
    @Temporal(TemporalType.DATE)
    private Date dateWhen;
    @Basic(optional = false)
    @NotNull
    @Column(name = "amount")
    private int amount;
    @Lob
    @Size(max = 16777215)
    @Column(name = "comments")
    private String comments;
    @Basic(optional = false)
    @NotNull
    @Column(name = "withdrawalcharges")
    private int withdrawalcharges;
    @JoinColumn(name = "member_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Members memberId;

    public Withdrawal() {
    }

    public Withdrawal(Integer id) {
        this.id = id;
    }

    public Withdrawal(Integer id, Date dateWhen, int amount, int withdrawalcharges) {
        this.id = id;
        this.dateWhen = dateWhen;
        this.amount = amount;
        this.withdrawalcharges = withdrawalcharges;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDateWhen() {
        return dateWhen;
    }

    public void setDateWhen(Date dateWhen) {
        this.dateWhen = dateWhen;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public int getWithdrawalcharges() {
        return withdrawalcharges;
    }

    public void setWithdrawalcharges(int withdrawalcharges) {
        this.withdrawalcharges = withdrawalcharges;
    }

    public Members getMemberId() {
        return memberId;
    }

    public void setMemberId(Members memberId) {
        this.memberId = memberId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Withdrawal)) {
            return false;
        }
        Withdrawal other = (Withdrawal) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Withdrawal[ id=" + id + " ]";
    }

    public boolean getStatusBilled() {
        return statusBilled;
    }

    public void setStatusBilled(boolean statusBilled) {
        this.statusBilled = statusBilled;
    }
    
}
