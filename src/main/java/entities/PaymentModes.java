/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author francis
 */
@Entity
@Table(name = "payment_modes")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PaymentModes.findAll", query = "SELECT p FROM PaymentModes p"),
    @NamedQuery(name = "PaymentModes.findById", query = "SELECT p FROM PaymentModes p WHERE p.id = :id"),
    @NamedQuery(name = "PaymentModes.findByPaymentMode", query = "SELECT p FROM PaymentModes p WHERE p.paymentMode = :paymentMode")})
public class PaymentModes implements Serializable {

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "paymentMode")
    private Collection<GeneralTransaction> generalTransactionCollection;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "payment_mode")
    private String paymentMode;

    public PaymentModes() {
    }

    public PaymentModes(Integer id) {
        this.id = id;
    }

    public PaymentModes(Integer id, String paymentMode) {
        this.id = id;
        this.paymentMode = paymentMode;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PaymentModes)) {
            return false;
        }
        PaymentModes other = (PaymentModes) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.PaymentModes[ id=" + id + " ]";
    }

    @XmlTransient
    public Collection<GeneralTransaction> getGeneralTransactionCollection() {
        return generalTransactionCollection;
    }

    public void setGeneralTransactionCollection(Collection<GeneralTransaction> generalTransactionCollection) {
        this.generalTransactionCollection = generalTransactionCollection;
    }

}
