/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import controllers.util.AppConstants;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.eclipse.persistence.annotations.Cache;

/**
 *
 * @author francis
 */
@Cache(alwaysRefresh = true)
@Entity
@Table(name = "loan_schedules")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LoanSchedules.findAll", query = "SELECT l FROM LoanSchedules l"),
    @NamedQuery(name = "LoanSchedules.findById", query = "SELECT l FROM LoanSchedules l WHERE l.id = :id"),
    @NamedQuery(name = "LoanSchedules.findByDueDate", query = "SELECT l FROM LoanSchedules l WHERE l.dueDate = :dueDate"),
    @NamedQuery(name = "LoanSchedules.findByInterest", query = "SELECT l FROM LoanSchedules l WHERE l.interest = :interest"),
    @NamedQuery(name = "LoanSchedules.findByRandomInt", query = "SELECT l FROM LoanSchedules l WHERE l.randomInt = :randomInt"),
    @NamedQuery(name = "LoanSchedules.findByPrincipleAmount", query = "SELECT l FROM LoanSchedules l WHERE l.principleAmount = :principleAmount"),
    @NamedQuery(name = "LoanSchedules.findByInstallmentAmount", query = "SELECT l FROM LoanSchedules l WHERE l.installmentAmount = :installmentAmount"),
    @NamedQuery(name = "LoanSchedules.findByBalance", query = "SELECT l FROM LoanSchedules l WHERE l.balance = :balance")})
public class LoanSchedules implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "isFirst")
    private boolean isFirst = false;
    @Basic(optional = false)
    @NotNull
    @Column(name = "is_for_last_month")
    private boolean isForLastMonth;
    @Column(name = "due_date")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dueDate;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "scheduleId")
    private Collection<LoanScheduleTransactions> loanScheduleTransactionsCollection;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Interest")
    private double interest;
    @Basic(optional = false)
    @NotNull
    @Column(name = "random_int")
    private int randomInt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "principle_amount")
    private float principleAmount;
    @Basic(optional = false)
    @NotNull
    @Column(name = "installment_amount")
    private float installmentAmount;
    @Basic(optional = false)
    @NotNull
    @Column(name = "balance")
    private float balance;
    @JoinColumn(name = "loan_id", referencedColumnName = "loan_id")
    @ManyToOne(optional = false)
    private Loan loanId;
    @JoinColumn(name = "status_cleared", referencedColumnName = "idschedule_status")
    @ManyToOne(optional = false)
    private ScheduleStatus statusCleared;

    public LoanSchedules() {
    }

    public LoanSchedules(Integer id) {
        this.id = id;
    }

    public LoanSchedules(Integer id, double interest, int randomInt, float principleAmount, float installmentAmount, float balance) {
        this.id = id;
        this.interest = interest;
        this.randomInt = randomInt;
        this.principleAmount = principleAmount;
        this.installmentAmount = installmentAmount;
        this.balance = balance;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public double getInterest() {
        return interest;
    }

    public void setInterest(double interest) {
        this.interest = interest;
    }

    public int getRandomInt() {
        return randomInt;
    }

    public void setRandomInt(int randomInt) {
        this.randomInt = randomInt;
    }

    public float getPrincipleAmount() {
        return principleAmount;
    }

    public void setPrincipleAmount(float principleAmount) {
        this.principleAmount = principleAmount;
    }

    public int getNegotiationPaidReport() {
        if (getTotalCredits() >= getInterest() + getNegotiation()) {
            return getNegotiation();
        }
        if (getTotalCredits() <= getInterest()) {
            return 0;
        }
        if (getTotalCredits() > getInterest() && getTotalCredits() <= getInterest() + getNegotiation()) {
            return (int) (getTotalCredits() - getInterest());
        }
        return 0;
    }

    public float getInstallmentAmount() {
        return installmentAmount;
    }

    public void setInstallmentAmount(float installmentAmount) {
        this.installmentAmount = installmentAmount;
    }

    public float getBalance() {
        return balance;
    }

    public void setBalance(float balance) {
        this.balance = balance;
    }

    public Loan getLoanId() {
        return loanId;
    }

    public void setLoanId(Loan loanId) {
        this.loanId = loanId;
    }

    public ScheduleStatus getStatusCleared() {
        return statusCleared;
    }

    public void setStatusCleared(ScheduleStatus statusCleared) {
        this.statusCleared = statusCleared;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LoanSchedules)) {
            return false;
        }
        LoanSchedules other = (LoanSchedules) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return balance + "\t\t" + principleAmount + "\t\t" + "\t\t" + interest + "\t\t" + installmentAmount;
    }

    @XmlTransient
    public Collection<LoanScheduleTransactions> getLoanScheduleTransactionsCollection() {
        return loanScheduleTransactionsCollection;
    }

    public void setLoanScheduleTransactionsCollection(Collection<LoanScheduleTransactions> loanScheduleTransactionsCollection) {
        this.loanScheduleTransactionsCollection = loanScheduleTransactionsCollection;
    }

    public int getNetAccount() {
        int totalDebit = 0, totalCredit = 0;
        for (LoanScheduleTransactions lst : getLoanScheduleTransactionsCollection()) {
            totalDebit += lst.getAmountDebit();
            totalCredit += lst.getAmountCredit();
        }
        return totalCredit - totalDebit;
    }

    public int getNegotiation() {
        if (getLoanScheduleTransactionsCollection() != null) {
            for (LoanScheduleTransactions lst : getLoanScheduleTransactionsCollection()) {
                try {
                    if (lst.getDetails().equals("loannegotiation")) {
                        return (int) (Math.ceil(lst.getAmountDebit()));
                    }
                } catch (NullPointerException e) {
                    return 0;
                }
            }
        }
        return 0;
    }

    public int getPayment() {
        if (getNetAccount() <= 0) {
            return 0;
        }
        return getNetAccount();
    }

    public int getNegotiationPaid() {
        if (getIsForLastMonth()) {
            if (getTotalCredits() >= getInterest() + getNegotiation()) {
                return getNegotiation();
            }
            if (getTotalCredits() <= getInterest()) {
                return 0;
            }
            if (getTotalCredits() > getInterest() && getTotalCredits() <= getInterest() + getNegotiation()) {
                return (int) (getTotalCredits() - getInterest());
            }
        }
        return 0;
    }

    public int getNegotiationUnpaid() {
        if (getNegotiation() - getNegotiationPaid() > 0) {
            return getNegotiation() - getNegotiationPaid();
        } else {
            return 0;
        }
    }

    public int getUnpaidInterest() {
        AppConstants.logger("total credits" + getTotalCredits());
        AppConstants.logger("Interest=" + getInterest());
        if (getTotalCredits() >= getInterest()) {
            return 0;
        } else {
            return (int) (getInterest() - getTotalCredits());
        }
    }

    public int getPaidInterest() {
        if (getUnpaidInterest() == 0) {
            return (int) interest;
        } else {
            return (int) (interest - getUnpaidInterest());
        }
    }

    public int getTotalDebits() {
        int total = 0;
        if (getLoanScheduleTransactionsCollection() != null) {
            for (LoanScheduleTransactions lst : getLoanScheduleTransactionsCollection()) {
                total += lst.getAmountDebit();
            }
        }
        return total;
    }

    public int getTotalCredits() {
        int total = 0;
        if (getLoanScheduleTransactionsCollection() != null) {
            for (LoanScheduleTransactions lst : getLoanScheduleTransactionsCollection()) {
                AppConstants.logger("tally:" + lst.getAmountCredit());
                total += lst.getAmountCredit();
            }
        }
        return total;
    }

    public int getTotalOtherBills() {
        return getTotalDebits() - (int) getInterest();
    }

    public boolean getIsForLastMonth() {
        return isForLastMonth;
    }

    public void setIsForLastMonth(boolean isForLastMonth) {
        this.isForLastMonth = isForLastMonth;
    }

    public boolean getIsFirst() {
        return isFirst;
    }

    public void setIsFirst(boolean isFirst) {
        this.isFirst = isFirst;
    }
}
