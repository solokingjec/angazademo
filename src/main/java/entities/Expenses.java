/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author francis
 */
@Entity
@Table(name = "expenses")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Expenses.findAll", query = "SELECT e FROM Expenses e"),
    @NamedQuery(name = "Expenses.findById", query = "SELECT e FROM Expenses e WHERE e.id = :id"),
    @NamedQuery(name = "Expenses.findByAmount", query = "SELECT e FROM Expenses e WHERE e.amount = :amount"),
    @NamedQuery(name = "Expenses.findByReceiptNo", query = "SELECT e FROM Expenses e WHERE e.receiptNo = :receiptNo"),
    @NamedQuery(name = "Expenses.findByDateIncured", query = "SELECT e FROM Expenses e WHERE e.dateIncured = :dateIncured")})
public class Expenses implements Serializable {
    @JoinColumn(name = "payment_method", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private ExpensesPaymentMethod paymentMethod;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "description")
    private String description;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "amount")
    private int amount;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "receipt_no")
    private String receiptNo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "date_incured")
    @Temporal(TemporalType.DATE)
    private Date dateIncured;
    @JoinColumn(name = "narration", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private ExpensesCategory narration;

    public Expenses() {
    }

    public Expenses(Integer id) {
        this.id = id;
    }

    public Expenses(Integer id, int amount, String receiptNo, Date dateIncured) {
        this.id = id;
        this.amount = amount;
        this.receiptNo = receiptNo;
        this.dateIncured = dateIncured;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getReceiptNo() {
        return receiptNo;
    }

    public void setReceiptNo(String receiptNo) {
        this.receiptNo = receiptNo;
    }

    public Date getDateIncured() {
        return dateIncured;
    }

    public void setDateIncured(Date dateIncured) {
        this.dateIncured = dateIncured;
    }

    public ExpensesCategory getNarration() {
        return narration;
    }

    public void setNarration(ExpensesCategory narration) {
        this.narration = narration;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Expenses)) {
            return false;
        }
        Expenses other = (Expenses) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Expenses[ id=" + id + " ]";
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ExpensesPaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(ExpensesPaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }
    
}
