/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import org.eclipse.persistence.annotations.Cache;

/**
 *
 * @author francis
 */
@Cache(alwaysRefresh = true)
@Entity
@Table(name = "sacco_ledger_entries")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SaccoLedgerEntries.findAll", query = "SELECT s FROM SaccoLedgerEntries s"),
    @NamedQuery(name = "SaccoLedgerEntries.findById", query = "SELECT s FROM SaccoLedgerEntries s WHERE s.id = :id"),
    @NamedQuery(name = "SaccoLedgerEntries.findByDr", query = "SELECT s FROM SaccoLedgerEntries s WHERE s.dr = :dr"),
    @NamedQuery(name = "SaccoLedgerEntries.findByCr", query = "SELECT s FROM SaccoLedgerEntries s WHERE s.cr = :cr"),
    @NamedQuery(name = "SaccoLedgerEntries.findByDateWhen", query = "SELECT s FROM SaccoLedgerEntries s WHERE s.dateWhen = :dateWhen")})
public class SaccoLedgerEntries implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "is_general")
    private boolean isGeneral;

    @Basic(optional = false)
    @NotNull
    @Column(name = "status_billed")
    private boolean statusBilled;

    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 2147483647)
    @Column(name = "particulars")
    private String particulars="d";
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "dr")
    private int dr;
    @Basic(optional = false)
    @NotNull
    @Column(name = "cr")
    private int cr;
    @Basic(optional = false)
    @NotNull
    @Column(name = "date_when")
    @Temporal(TemporalType.DATE)
    private Date dateWhen;
    @JoinColumn(name = "ledger_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private SaccoLedgers ledgerId;

    public SaccoLedgerEntries() {
    }

    public SaccoLedgerEntries(Integer id) {
        this.id = id;
    }

    public SaccoLedgerEntries(Integer id, int dr, int cr, Date dateWhen) {
        this.id = id;
        this.dr = dr;
        this.cr = cr;
        this.dateWhen = dateWhen;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getDr() {
        return dr;
    }

    public void setDr(int dr) {
        this.dr = dr;
    }

    public int getCr() {
        return cr;
    }

    public void setCr(int cr) {
        this.cr = cr;
    }

    public Date getDateWhen() {
        return dateWhen;
    }

    public void setDateWhen(Date dateWhen) {
        this.dateWhen = dateWhen;
    }

    public SaccoLedgers getLedgerId() {
        return ledgerId;
    }

    public void setLedgerId(SaccoLedgers ledgerId) {
        this.ledgerId = ledgerId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SaccoLedgerEntries)) {
            return false;
        }
        SaccoLedgerEntries other = (SaccoLedgerEntries) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.SaccoLedgerEntries[ id=" + id + " ]";
    }

    public String getParticulars() {
        return particulars;
    }

    public void setParticulars(String particulars) {
        this.particulars = particulars;
    }

    public boolean getStatusBilled() {
        return statusBilled;
    }

    public void setStatusBilled(boolean statusBilled) {
        this.statusBilled = statusBilled;
    }

    public boolean getIsGeneral() {
        return isGeneral;
    }

    public void setIsGeneral(boolean isGeneral) {
        this.isGeneral = isGeneral;
    }

    public boolean getIsBcf() {
        return getParticulars().equals("b.b.f");
    }
}
