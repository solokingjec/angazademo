/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author francis
 */
@Entity
@Table(name = "payment_summary_items")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PaymentSummaryItems.findAll", query = "SELECT p FROM PaymentSummaryItems p"),
    @NamedQuery(name = "PaymentSummaryItems.findById", query = "SELECT p FROM PaymentSummaryItems p WHERE p.id = :id"),
    @NamedQuery(name = "PaymentSummaryItems.findByPaidShares", query = "SELECT p FROM PaymentSummaryItems p WHERE p.paidShares = :paidShares"),
    @NamedQuery(name = "PaymentSummaryItems.findByDepositedAmount", query = "SELECT p FROM PaymentSummaryItems p WHERE p.depositedAmount = :depositedAmount"),
    @NamedQuery(name = "PaymentSummaryItems.findByNormalLoanPaid", query = "SELECT p FROM PaymentSummaryItems p WHERE p.normalLoanPaid = :normalLoanPaid"),
    @NamedQuery(name = "PaymentSummaryItems.findByNormalLoanInterestPaid", query = "SELECT p FROM PaymentSummaryItems p WHERE p.normalLoanInterestPaid = :normalLoanInterestPaid"),
    @NamedQuery(name = "PaymentSummaryItems.findByEmergencyLoanPaid", query = "SELECT p FROM PaymentSummaryItems p WHERE p.emergencyLoanPaid = :emergencyLoanPaid"),
    @NamedQuery(name = "PaymentSummaryItems.findByEmergencyLoanInterestPaid", query = "SELECT p FROM PaymentSummaryItems p WHERE p.emergencyLoanInterestPaid = :emergencyLoanInterestPaid")})
public class PaymentSummaryItems implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "loan_balance_emergency")
    private int loanBalanceEmergency;

    @Basic(optional = false)
    @NotNull
    @Column(name = "interest_charged_emergency")
    private int interestChargedEmergency;
    @Basic(optional = false)
    @NotNull
    @Column(name = "interest_balance_emergency")
    private int interestBalanceEmergency;

    @Basic(optional = false)
    @NotNull
    @Column(name = "loanedemergency")
    private int loanedemergency;

    @Basic(optional = false)
    @NotNull
    @Column(name = "loaned")
    private int loaned;

    @Basic(optional = false)
    @NotNull
    @Column(name = "deposits_total")
    private int depositsTotal;
    @Basic(optional = false)
    @NotNull
    @Column(name = "withdrawn_deposits_amount")
    private int withdrawnDepositsAmount;
    @Basic(optional = false)
    @NotNull
    @Column(name = "withdrawn_shares_amount")
    private int withdrawnSharesAmount;
    @Basic(optional = false)
    @NotNull
    @Column(name = "shares_balance")
    private int sharesBalance;
    @Basic(optional = false)
    @NotNull
    @Column(name = "loan_balance")
    private int loanBalance;
    @Basic(optional = false)
    @NotNull
    @Column(name = "interest_charged")
    private int interestCharged;
    @Basic(optional = false)
    @NotNull
    @Column(name = "interest_balance")
    private int interestBalance;

    @Basic(optional = false)
    @NotNull
    @Column(name = "negotiation")
    private int negotiation;
    @Basic(optional = false)
    @NotNull
    @Column(name = "registration")
    private int registration;
    @Basic(optional = false)
    @NotNull
    @Column(name = "withdrawalincome")
    private int withdrawalincome;
    @Basic(optional = false)
    @NotNull
    @Column(name = "withdrawal")
    private int withdrawal;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "paid_shares")
    private int paidShares;
    @Basic(optional = false)
    @NotNull
    @Column(name = "deposited_amount")
    private int depositedAmount;
    @Basic(optional = false)
    @NotNull
    @Column(name = "normal_loan_paid")
    private int normalLoanPaid;
    @Basic(optional = false)
    @NotNull
    @Column(name = "normal_loan_interest_paid")
    private int normalLoanInterestPaid;
    @Basic(optional = false)
    @NotNull
    @Column(name = "emergency_loan_paid")
    private int emergencyLoanPaid;
    @Basic(optional = false)
    @NotNull
    @Column(name = "emergency_loan_interest_paid")
    private int emergencyLoanInterestPaid;
    @JoinColumn(name = "payment_summary_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private PaymentSummary paymentSummaryId;
    @JoinColumn(name = "member_name", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Members memberName;

    public PaymentSummaryItems() {
    }

    public PaymentSummaryItems(Integer id) {
        this.id = id;
    }

    public PaymentSummaryItems(Integer id, int paidShares, int depositedAmount, int normalLoanPaid, int normalLoanInterestPaid, int emergencyLoanPaid, int emergencyLoanInterestPaid) {
        this.id = id;
        this.paidShares = paidShares;
        this.depositedAmount = depositedAmount;
        this.normalLoanPaid = normalLoanPaid;
        this.normalLoanInterestPaid = normalLoanInterestPaid;
        this.emergencyLoanPaid = emergencyLoanPaid;
        this.emergencyLoanInterestPaid = emergencyLoanInterestPaid;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getPaidShares() {
        return paidShares;
    }

    public void setPaidShares(int paidShares) {
        this.paidShares = paidShares;
    }

    public int getDepositedAmount() {
        return depositedAmount;
    }

    public void setDepositedAmount(int depositedAmount) {
        this.depositedAmount = depositedAmount;
    }

    public int getNormalLoanPaid() {
        return normalLoanPaid;
    }

    public void setNormalLoanPaid(int normalLoanPaid) {
        this.normalLoanPaid = normalLoanPaid;
    }

    public int getNormalLoanInterestPaid() {
        return normalLoanInterestPaid;
    }

    public void setNormalLoanInterestPaid(int normalLoanInterestPaid) {
        this.normalLoanInterestPaid = normalLoanInterestPaid;
    }

    public int getEmergencyLoanPaid() {
        return emergencyLoanPaid;
    }

    public void setEmergencyLoanPaid(int emergencyLoanPaid) {
        this.emergencyLoanPaid = emergencyLoanPaid;
    }

    public int getEmergencyLoanInterestPaid() {
        return emergencyLoanInterestPaid;
    }

    public void setEmergencyLoanInterestPaid(int emergencyLoanInterestPaid) {
        this.emergencyLoanInterestPaid = emergencyLoanInterestPaid;
    }

    public PaymentSummary getPaymentSummaryId() {
        return paymentSummaryId;
    }

    public void setPaymentSummaryId(PaymentSummary paymentSummaryId) {
        this.paymentSummaryId = paymentSummaryId;
    }

    public Members getMemberName() {
        return memberName;
    }

    public void setMemberName(Members memberName) {
        this.memberName = memberName;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PaymentSummaryItems)) {
            return false;
        }
        PaymentSummaryItems other = (PaymentSummaryItems) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.PaymentSummaryItems[ id=" + id + " ]";
    }

    public int getWithdrawalincome() {
        return withdrawalincome;
    }

    public void setWithdrawalincome(int withdrawalincome) {
        this.withdrawalincome = withdrawalincome;
    }

    public int getWithdrawal() {
        return withdrawal;
    }

    public void setWithdrawal(int withdrawal) {
        this.withdrawal = withdrawal;
    }

    public int getNegotiation() {
        return negotiation;
    }

    public void setNegotiation(int negotiation) {
        this.negotiation = negotiation;
    }

    public int getRegistration() {
        return registration;
    }

    public void setRegistration(int registration) {
        this.registration = registration;
    }

    public int getDepositsTotal() {
        return depositsTotal;
    }

    public void setDepositsTotal(int depositsTotal) {
        this.depositsTotal = depositsTotal;
    }

    public int getWithdrawnDepositsAmount() {
        return withdrawnDepositsAmount;
    }

    public void setWithdrawnDepositsAmount(int withdrawnDepositsAmount) {
        this.withdrawnDepositsAmount = withdrawnDepositsAmount;
    }

    public int getWithdrawnSharesAmount() {
        return withdrawnSharesAmount;
    }

    public void setWithdrawnSharesAmount(int withdrawnSharesAmount) {
        this.withdrawnSharesAmount = withdrawnSharesAmount;
    }

    public int getSharesBalance() {
        return sharesBalance;
    }

    public void setSharesBalance(int sharesBalance) {
        this.sharesBalance = sharesBalance;
    }

    public int getLoanBalance() {
        return loanBalance;
    }

    public void setLoanBalance(int loanBalance) {
        this.loanBalance = loanBalance;
    }

    public int getInterestCharged() {
        return interestCharged;
    }

    public void setInterestCharged(int interestCharged) {
        this.interestCharged = interestCharged;
    }

    public int getInterestBalance() {
        return interestBalance;
    }

    public void setInterestBalance(int interestBalance) {
        this.interestBalance = interestBalance;
    }

    public int getLoaned() {
        return loaned;
    }

    public void setLoaned(int loaned) {
        this.loaned = loaned;
    }
    public int getTotalLoanPaidEmergency()
    {
        return getEmergencyLoanPaid();
    }
    public int getTotalLoanPaidNormal()
    {
        return getNormalLoanPaid();
    }
    public int getTotalInterestPaidNormal()
    {
        return getNormalLoanInterestPaid();
    }
    
    public int getTotalInterestPaidEmergency()
    {
        return getEmergencyLoanInterestPaid();
    }
    
    public String getDatePrepared(){
        return getPaymentSummaryId().getDatePreparedStr();
    }

    public int getLoanedemergency() {
        return loanedemergency;
    }

    public void setLoanedemergency(int loanedemergency) {
        this.loanedemergency = loanedemergency;
    }

    public int getInterestChargedEmergency() {
        return interestChargedEmergency;
    }

    public void setInterestChargedEmergency(int interestChargedEmergency) {
        this.interestChargedEmergency = interestChargedEmergency;
    }

    public int getInterestBalanceEmergency() {
        return interestBalanceEmergency;
    }

    public void setInterestBalanceEmergency(int interestBalanceEmergency) {
        this.interestBalanceEmergency = interestBalanceEmergency;
    }

    public int getLoanBalanceEmergency() {
        return loanBalanceEmergency;
    }

    public void setLoanBalanceEmergency(int loanBalanceEmergency) {
        this.loanBalanceEmergency = loanBalanceEmergency;
    }
}
