/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import controllers.util.AppConstants;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.eclipse.persistence.annotations.Cache;

/**
 *
 * @author francis
 */
@Cache(alwaysRefresh = true)
@Entity
@Table(name = "members")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Members.findAll", query = "SELECT m FROM Members m ORDER BY m.id DESC"),
    @NamedQuery(name = "Members.findByMemberNo", query = "SELECT m FROM Members m WHERE m.memberNo = :memberNo"),
    @NamedQuery(name = "Members.findBySurname", query = "SELECT m FROM Members m WHERE m.surname = :surname"),
    @NamedQuery(name = "Members.findByOtherNames", query = "SELECT m FROM Members m WHERE m.otherNames = :otherNames"),
    @NamedQuery(name = "Members.findByResidence", query = "SELECT m FROM Members m WHERE m.residence = :residence"),
    @NamedQuery(name = "Members.findByIdNo", query = "SELECT m FROM Members m WHERE m.idNo = :idNo"),
    @NamedQuery(name = "Members.findByDob", query = "SELECT m FROM Members m WHERE m.dob = :dob"),
    @NamedQuery(name = "Members.findByRegDate", query = "SELECT m FROM Members m WHERE m.regDate = :regDate"),
    @NamedQuery(name = "Members.findByAddress", query = "SELECT m FROM Members m WHERE m.address = :address"),
    @NamedQuery(name = "Members.findByPhone", query = "SELECT m FROM Members m WHERE m.phone = :phone"),
    @NamedQuery(name = "Members.findByAltphone", query = "SELECT m FROM Members m WHERE m.altphone = :altphone"),
    @NamedQuery(name = "Members.findById", query = "SELECT m FROM Members m WHERE m.id = :id"),
    @NamedQuery(name = "Members.findByRandomInt", query = "SELECT m FROM Members m WHERE m.randomInt = :randomInt")})
public class Members implements Serializable {

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "member1")
    private Collection<GeneralTransaction> generalTransactionCollection;

    @JoinColumn(name = "priority_account", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private PriorityAccOptions priorityAccount;

    @Basic(optional = false)
    @NotNull
    @Column(name = "random_int")
    private int randomInt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "deffault_count")
    private int deffaultCount;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "memberId")
    private Collection<LoanRecovery> loanRecoveryCollection;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "memberId")
    private Collection<Withdrawal> withdrawalCollection;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "memberName")
    private Collection<PaymentSummaryItems> paymentSummaryItemsCollection;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "memberId")
    private Collection<Collections> collectionsCollection;
    private static final long serialVersionUID = 1L;
    @Size(max = 100)
    @Column(name = "member_no")
    private String memberNo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "surname")
    private String surname = "";
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "other_names")
    private String otherNames = "erter";
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "residence")
    private String residence = "erter";
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "id_no")
    private String idNo = "erter";
    @Basic(optional = false)
    @NotNull
    @Column(name = "dob")
    @Temporal(TemporalType.DATE)
    private Date dob = new Date();
    @Basic(optional = false)
    @NotNull
    @Column(name = "reg_date")
    @Temporal(TemporalType.DATE)
    private Date regDate = new Date();
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "address")
    private String address = "erter";
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "phone")
    private String phone = "erter";
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "altphone")
    private String altphone = "erter";
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "memeberId")
    private Collection<Loan> loanCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "memberId")
    private Collection<MemberBills> memberBillsCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "memberNo")
    private Collection<DepositCash> depositCashCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "account")
    private Collection<AccountTransitions> accountTransitionsCollection;
    @JoinColumn(name = "gender", referencedColumnName = "idgender")
    @ManyToOne(optional = false)
    private Gender gender;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "guarantorId")
    private Collection<Guarantors> guarantorsCollection;

    public Members() {
    }

    public Members(Integer id) {
        this.id = id;
    }

    public Members(Integer id, String surname, String otherNames, String residence, String idNo, Date dob, Date regDate, String address, String phone, String altphone) {
        this.id = id;
        this.surname = surname;
        this.otherNames = otherNames;
        this.residence = residence;
        this.idNo = idNo;
        this.dob = dob;
        this.regDate = regDate;
        this.address = address;
        this.phone = phone;
        this.altphone = altphone;
    }

    @XmlTransient
    public String getMemberNo() {
        return memberNo;
    }

    public void setMemberNo(String memberNo) {
        this.memberNo = memberNo;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @XmlTransient

    public String getOtherNames() {
        return otherNames;
    }

    public void setOtherNames(String otherNames) {
        this.otherNames = otherNames;
    }

    @XmlTransient
    public String getResidence() {
        return residence;
    }

    public void setResidence(String residence) {
        this.residence = residence;
    }

    @XmlTransient

    public String getIdNo() {
        return idNo;
    }

    public void setIdNo(String idNo) {
        this.idNo = idNo;
    }

    @XmlTransient
    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    @XmlTransient
    public Date getRegDate() {
        return regDate;
    }

    public void setRegDate(Date regDate) {
        this.regDate = regDate;
    }

    public int getTotalNegotiationPaidOnLoans() {
        int total = 0;
        if (getLoanCollection() != null) {
            for (Loan l : getLoanCollection()) {
                for (LoanSchedules ls : l.getLoanSchedulesCollection()) {
                    total += ls.getNegotiationPaidReport();
                }
            }
        }
        return total;
    }

    @XmlTransient
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @XmlTransient
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @XmlTransient
    public String getAltphone() {
        return altphone;
    }

    public void setAltphone(String altphone) {
        this.altphone = altphone;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @XmlTransient
    public Integer getRandomInt() {
        return randomInt;
    }

    public void setRandomInt(Integer randomInt) {
        this.randomInt = randomInt;
    }

    @XmlTransient
    public Collection<Loan> getLoanCollection() {
        return loanCollection;
    }

    public void setLoanCollection(Collection<Loan> loanCollection) {
        this.loanCollection = loanCollection;
    }

    @XmlTransient
    public Collection<MemberBills> getMemberBillsCollection() {
        return memberBillsCollection;
    }

    public void setMemberBillsCollection(Collection<MemberBills> memberBillsCollection) {
        this.memberBillsCollection = memberBillsCollection;
    }

    @XmlTransient
    public Collection<DepositCash> getDepositCashCollection() {
        return depositCashCollection;
    }

    public void setDepositCashCollection(Collection<DepositCash> depositCashCollection) {
        this.depositCashCollection = depositCashCollection;
    }

    @XmlTransient
    public Collection<AccountTransitions> getAccountTransitionsCollection() {
        return accountTransitionsCollection;
    }

    public void setAccountTransitionsCollection(Collection<AccountTransitions> accountTransitionsCollection) {
        this.accountTransitionsCollection = accountTransitionsCollection;
    }

    @XmlTransient
    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    @XmlTransient
    public Collection<Guarantors> getGuarantorsCollection() {
        return guarantorsCollection;
    }

    public void setGuarantorsCollection(Collection<Guarantors> guarantorsCollection) {
        this.guarantorsCollection = guarantorsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Members)) {
            return false;
        }
        Members other = (Members) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    public String getTtoString() {
        return "Members{" + "collectionsCollection=" + collectionsCollection + ", loanCollection=" + loanCollection + ", depositCashCollection=" + depositCashCollection + '}';
    }

    public int getBillsTotal() {
        int total = 0;
        if (getMemberBillsCollection() != null) {
            for (MemberBills mb : getMemberBillsCollection()) {
                total += mb.getAmount();
            }
        }
        return total;
    }

    public int getBillsTotalArrears() {
        int total = 0;
        if (getMemberBillsCollection() != null) {
            for (MemberBills mb : getMemberBillsCollection()) {
                total += mb.getNetAcc();
            }
        }
        return total;
    }

    @XmlTransient
    public Collection<Collections> getCollectionsCollection() {
        java.util.Collections.sort((List<Collections>) this.collectionsCollection, Collections.Total);
        return collectionsCollection;
    }

    public void setCollectionsCollection(Collection<Collections> collectionsCollection) {
        this.collectionsCollection = collectionsCollection;
    }

    public int getTotalAccounted() {
        int total = 0;
        if (getCollectionsCollection() != null) {
            for (Collections c : getCollectionsCollection()) {
                total += c.getAmount();
            }
        }
        return total;
    }

    public int getTotalUnAccounted() {

        return getTotalUnAccountedEmergencyLoans() + getTotalUnAccountedNormalLoans();
    }

    public int getTotalUnAccountedEmergencyLoans() {
        int total = 0;
        if (getCollectionsCollection() != null) {
            for (Collections c : getCollectionsCollection()) {
                if (!c.getStatusBilled() && c.getCollectionstype().getId() == 2) {
                    total += c.getAmount();
                }
            }
        }
        return total;
    }

    public int getTotalUnAccountedNormalLoans() {
        int total = 0;
        if (getCollectionsCollection() != null) {
            for (Collections c : getCollectionsCollection()) {
                if (!c.getStatusBilled() && c.getCollectionstype().getId() == 3) {
                    total += c.getAmount();
                }
            }
        }
        return total;
    }

    public int getTotalDeposit() {
        int total = 0;
        if (getDepositCashCollection() != null) {
            for (DepositCash dp : getDepositCashCollection()) {
                total += dp.getAmount();
            }
        }
        return total;
    }

    public int getTotalDepositWithrawable() {
        int total = 0;
        if (getDepositCashCollection() != null) {
            for (DepositCash dp : getDepositCashCollection()) {
                if (dp.getIsWithdrawable()) {
                    total += dp.getAmount();
                }
            }
        }
        return total;
    }

    public int getTotalDepositNonWithrawable() {
        return getTotalDeposit() - getTotalDepositWithrawable();
    }

    public int getTotalShares() {
        return getTotalDeposit() - getTotalDepositWithrawable();
    }

    public Loan getActiveLoanEmergency() {
        if (getLoanCollection() != null) {
            for (Loan l : getLoanCollection()) {
                if (l.getLoantype().getId() == 1 && !l.getLoanStatus()) {
                    return l;
                }
            }
        }
        return null;
    }

    public Loan getActiveLoanNormal() {
        if (getLoanCollection() != null) {
            for (Loan l : getLoanCollection()) {
                if (l.getLoantype().getId() == 2 && !l.getLoanStatus()) {
                    return l;
                }
            }
        }
        return null;
    }

    public int getEstimatedUnPaidInterestEmergency() {
        if (getActiveLoanEmergency() != null) {
            if (getActiveLoanEmergency().getCurrentSchedule() != null) {
                Loan l = getActiveLoanEmergency();
                LoanSchedules ls = l.getCurrentSchedule();
                if (getTotalUnAccountedEmergencyLoans() > 0) {
                    AppConstants.logger("TT unaccounted:" + getTotalUnAccountedEmergencyLoans());
                    LoanScheduleTransactions lst = new LoanScheduleTransactions();
                    AppConstants.logger("Trans bf:" + ls.getLoanScheduleTransactionsCollection().size());
                    lst.setAmountCredit((double) getTotalUnAccountedEmergencyLoans());
                    ls.getLoanScheduleTransactionsCollection().add(lst);
                    AppConstants.logger("Trans after:" + ls.getLoanScheduleTransactionsCollection().size());
                    int returnInt = (int) ((int) ls.getUnpaidInterest());
                    ls.getLoanScheduleTransactionsCollection().remove(lst);
                    return returnInt;
                }
                return (int) getActiveLoanEmergency().getCurrentSchedule().getUnpaidInterest();
            }
        }
        return 0;
    }

    public int getEstimatedUnPaidNegotiationEmergency() {
        if (getActiveLoanEmergency() != null) {
            if (getActiveLoanEmergency().getCurrentSchedule() != null) {
                Loan l = getActiveLoanEmergency();
                LoanSchedules ls = l.getCurrentSchedule();
                if (getTotalUnAccountedEmergencyLoans() > 0) {
                    AppConstants.logger("TT unaccounted:" + getTotalUnAccountedEmergencyLoans());
                    LoanScheduleTransactions lst = new LoanScheduleTransactions();
                    AppConstants.logger("Trans bf:" + ls.getLoanScheduleTransactionsCollection().size());
                    lst.setAmountCredit((double) getTotalUnAccountedEmergencyLoans());
                    ls.getLoanScheduleTransactionsCollection().add(lst);
                    AppConstants.logger("Trans after:" + ls.getLoanScheduleTransactionsCollection().size());
                    int returnInt = (int) ((int) ls.getNegotiationUnpaid());
                    ls.getLoanScheduleTransactionsCollection().remove(lst);
                    return returnInt;
                }
                return (int) getActiveLoanEmergency().getCurrentSchedule().getNegotiationUnpaid();
            }
        }
        return 0;
    }

    //**************
    public int getEstimatedUnPaidInterestNormal() {
        if (getActiveLoanNormal() != null) {
            if (getActiveLoanNormal().getCurrentSchedule() != null) {
                Loan l = getActiveLoanNormal();
                LoanSchedules ls = l.getCurrentSchedule();
                if (getTotalUnAccountedNormalLoans() > 0) {
                    AppConstants.logger("TT unaccounted:" + getTotalUnAccountedNormalLoans());
                    LoanScheduleTransactions lst = new LoanScheduleTransactions();
                    AppConstants.logger("Trans bf:" + ls.getLoanScheduleTransactionsCollection().size());
                    lst.setAmountCredit((double) getTotalUnAccountedNormalLoans());
                    ls.getLoanScheduleTransactionsCollection().add(lst);
                    AppConstants.logger("Trans after:" + ls.getLoanScheduleTransactionsCollection().size());
                    int returnInt = (int) ((int) ls.getUnpaidInterest());
                    ls.getLoanScheduleTransactionsCollection().remove(lst);
                    return returnInt;
                }
                return (int) getActiveLoanNormal().getCurrentSchedule().getUnpaidInterest();
            }
        }
        return 0;
    }

    public int getEstimatedUnPaidNegotiationNormal() {
        if (getActiveLoanNormal() != null) {
            if (getActiveLoanNormal().getCurrentSchedule() != null) {
                Loan l = getActiveLoanNormal();
                LoanSchedules ls = l.getCurrentSchedule();
                if (getTotalUnAccountedNormalLoans() > 0) {
                    AppConstants.logger("TT unaccounted:" + getTotalUnAccountedNormalLoans());
                    LoanScheduleTransactions lst = new LoanScheduleTransactions();
                    AppConstants.logger("Trans bf:" + ls.getLoanScheduleTransactionsCollection().size());
                    lst.setAmountCredit((double) getTotalUnAccountedNormalLoans());
                    ls.getLoanScheduleTransactionsCollection().add(lst);
                    AppConstants.logger("Trans after:" + ls.getLoanScheduleTransactionsCollection().size());
                    int returnInt = (int) ((int) ls.getNegotiationUnpaid());
                    ls.getLoanScheduleTransactionsCollection().remove(lst);
                    return returnInt;
                }
                return (int) getActiveLoanNormal().getCurrentSchedule().getNegotiationUnpaid();
            }
        }
        return 0;
    }

    public int getEstimatedLoanBalanceEmergencyWithoutInterest() {
        if (getActiveLoanEmergency() != null) {
            if (getActiveLoanEmergency().getCurrentSchedule() != null) {
                Loan l = getActiveLoanEmergency();
                LoanSchedules ls = l.getCurrentSchedule();
                if (getTotalUnAccountedEmergencyLoans() > 0) {
                    LoanScheduleTransactions lst = new LoanScheduleTransactions();
                    lst.setAmountCredit((double) getTotalUnAccountedEmergencyLoans());
                    ls.getLoanScheduleTransactionsCollection().add(lst);
                    int returnInt = (int) ((int) ls.getBalance() - ls.getNetAccount());
                    ls.getLoanScheduleTransactionsCollection().remove(lst);
                    return returnInt;
                }
                return (int) getActiveLoanEmergency().getCurrentSchedule().getBalance();
            }
        }
        return 0;
    }

    public int getEstimatedLoanBalanceEmergency() {
        if (getActiveLoanEmergency() != null) {
            Loan l = getActiveLoanEmergency();
            LoanSchedules ls = l.getCurrentSchedule();
            if (getTotalUnAccountedEmergencyLoans() > 0) {
                LoanScheduleTransactions lst = new LoanScheduleTransactions();
                lst.setAmountCredit((double) getTotalUnAccountedEmergencyLoans());
                ls.getLoanScheduleTransactionsCollection().add(lst);
                int returnInt = (int) ((int) ls.getBalance() - ls.getNetAccount());
                ls.getLoanScheduleTransactionsCollection().remove(lst);
                return returnInt;
            }
            return (int) getActiveLoanEmergency().getCurrentSchedule().getBalance() - ls.getNetAccount();
        }
        return 0;
    }

    public int getEstimatedLoanBalanceNormal() {
        if (getActiveLoanNormal() != null) {
            Loan l = getActiveLoanNormal();
            LoanSchedules ls = l.getCurrentSchedule();
            if (getTotalUnAccountedNormalLoans() > 0) {
                LoanScheduleTransactions lst = new LoanScheduleTransactions();
                lst.setAmountCredit((double) getTotalUnAccountedNormalLoans());
                ls.getLoanScheduleTransactionsCollection().add(lst);
                int returnInt = (int) ((int) ls.getBalance() - ls.getNetAccount());
                ls.getLoanScheduleTransactionsCollection().remove(lst);
                return returnInt;
            }
            return (int) getActiveLoanNormal().getCurrentSchedule().getBalance() - ls.getNetAccount();
        }
        return 0;
    }

    public int getEstimatedLoanBalanceNormalWithoutInterests() {
        if (getActiveLoanEmergency() != null) {
            if (getActiveLoanEmergency().getCurrentSchedule() != null) {
                Loan l = getActiveLoanNormal();
                LoanSchedules ls = l.getCurrentSchedule();
                if (getTotalUnAccountedNormalLoans() > 0) {
                    LoanScheduleTransactions lst = new LoanScheduleTransactions();
                    lst.setAmountCredit((double) getTotalUnAccountedNormalLoans());
                    ls.getLoanScheduleTransactionsCollection().add(lst);
                    int returnInt = (int) ((int) ls.getBalance() - ls.getNetAccount());
                    ls.getLoanScheduleTransactionsCollection().remove(lst);
                    return returnInt;
                }
                return (int) getActiveLoanNormal().getCurrentSchedule().getBalance();
            }
        }
        return 0;
    }

    public int getTotalLoans() {
        int sum = 0;
        if (getLoanCollection() != null) {
            for (Loan l : getLoanCollection()) {
                sum += (int) l.getAmount();
            }
        }
        return sum;
    }

    public int getTotalLoansInterestPaid() {
        int sum = 0;
        if (getLoanCollection() != null) {
            for (Loan l : getLoanCollection()) {
                for (LoanSchedules ls : l.getLoanSchedulesCollection()) {
                    sum += (int) ls.getPaidInterest();
                }
            }
        }
        return sum;
    }

    public int getTotalUnpaidLoan() {
        int sum = 0;
        if (getLoanCollection() != null) {
            for (Loan l : getLoanCollection()) {
                if (!l.getLoanStatus()) {
                    sum += l.getBalEst();
                }
            }
        }
        return sum;
    }

    public int getNoOfGuaranteedLoans() {
        int sum = 0;
        if (getGuarantorsCollection() != null) {
            for (Guarantors g : getGuarantorsCollection()) {
                if (!g.getLoanId().getLoanStatus()) {
                    sum += 1;
                }
            }
        }
        return sum;
    }

    public int getAmountOfGuaranteedLoans() {
        int sum = 0;
        if (getGuarantorsCollection() != null) {
            for (Guarantors g : getGuarantorsCollection()) {
                if (!g.getLoanId().getLoanStatus()) {
                    sum += g.getGuaranteedAmount();
                }
            }
        }
        return sum;
    }

    @XmlTransient
    public Collection<PaymentSummaryItems> getPaymentSummaryItemsCollection() {
        return paymentSummaryItemsCollection;
    }

    public void setPaymentSummaryItemsCollection(Collection<PaymentSummaryItems> paymentSummaryItemsCollection) {
        this.paymentSummaryItemsCollection = paymentSummaryItemsCollection;
    }

    public String getMembershipStatus() {
        if (getAccountTransitionsCollection() != null) {
            for (AccountTransitions acct : getAccountTransitionsCollection()) {
                if (acct.getIsCurrent()) {
                    return acct.getStatus().getStatus();
                }
            }
        }
        return "Unknown";
    }

    public boolean getIsactive() {
        return getMembershipStatus().equals("Closed");
    }

    public int getInterestOnNormalLoansPreviuosMonth() {
        if (getActiveLoanNormal().getGuarantorsCollection() != null) {
            Loan l = getActiveLoanNormal();
            LoanSchedules ls = l.getCurrentSchedule();
            for (LoanSchedules lsc : l.getLoanSchedulesCollection()) {
                if (ls.getBalance() == lsc.getPrincipleAmount() && !Objects.equals(lsc.getId(), ls.getId())) {
                    ls = lsc;
                    break;
                }
            }
            return ls.getPaidInterest();
        }
        return 0;
    }

    public int getNormalLoansPaidPreviuosMonth() {
        if (getActiveLoanNormal().getGuarantorsCollection() != null) {
            Loan l = getActiveLoanNormal();
            LoanSchedules ls = l.getCurrentSchedule();
            for (LoanSchedules lsc : l.getLoanSchedulesCollection()) {
                if (ls.getBalance() == lsc.getPrincipleAmount() && !Objects.equals(lsc.getId(), ls.getId())) {
                    ls = lsc;
                    break;
                }
            }
            return ls.getPayment();
        }
        return 0;
    }

    public int getInterestOnEmergencyLoansPreviuosMonth() {
        if (getActiveLoanEmergency().getGuarantorsCollection() != null) {
            Loan l = getActiveLoanEmergency();
            LoanSchedules ls = l.getCurrentSchedule();
            for (LoanSchedules lsc : l.getLoanSchedulesCollection()) {
                if (ls.getBalance() == lsc.getPrincipleAmount() && !Objects.equals(lsc.getId(), ls.getId())) {
                    ls = lsc;
                    break;
                }
            }
            return ls.getPaidInterest();
        }
        return 0;
    }

    public int getEmergencyLoansPaidPreviuosMonth() {
        if (getActiveLoanEmergency().getGuarantorsCollection() != null) {
            Loan l = getActiveLoanEmergency();
            LoanSchedules ls = l.getCurrentSchedule();
            for (LoanSchedules lsc : l.getLoanSchedulesCollection()) {
                if (ls.getBalance() == lsc.getPrincipleAmount() && !Objects.equals(lsc.getId(), ls.getId())) {
                    ls = lsc;
                    break;
                }
            }
            return ls.getPayment();
        }
        return 0;
    }

    public int getSharesPaid() {
        int total = 0;
        for (MemberBills mb : getMemberBillsCollection()) {
            for (BillTransactions bt : mb.getBillTransactionsCollection()) {
                if (bt.getNaration().equals("shares") && bt.getAmountCredit() > 0) {
                    total += bt.getAmountCredit();
                }
            }
        }
        return total;
    }

    public int getRegPaid() {
        int total = 0;
        for (MemberBills mb : getMemberBillsCollection()) {
            for (BillTransactions bt : mb.getBillTransactionsCollection()) {
                if (bt.getNaration().equals("registration") && bt.getAmountCredit() > 0) {
                    total += bt.getAmountCredit();
                }
            }
        }
        return total;
    }

    public int getTotalInterestPaidOnLoans() {
        int total = 0;
        if (getLoanCollection() != null) {
            for (Loan l : getLoanCollection()) {
                for (LoanSchedules ls : l.getLoanSchedulesCollection()) {
                    total += ls.getPaidInterest();
                }
            }
        }
        return total;
    }

    @XmlTransient
    public Collection<Withdrawal> getWithdrawalCollection() {
        return withdrawalCollection;
    }

    public void setWithdrawalCollection(Collection<Withdrawal> withdrawalCollection) {
        this.withdrawalCollection = withdrawalCollection;
    }

    public int getTotalWithdrawals() {
        int total = 0;
        if (getWithdrawalCollection() != null) {
            for (Withdrawal with : getWithdrawalCollection()) {
                total += with.getAmount();
            }
        }
        return total;
    }

    public int getTotalWithdrawalsIncome() {
        int total = 0;
        if (getWithdrawalCollection() != null) {
            for (Withdrawal with : getWithdrawalCollection()) {
                total += with.getWithdrawalcharges();
            }
        }
        return total;
    }

    public int getTotalLoanRecovery() {
        return 0;
    }

    public int getLoanDifferenceWithDeposits() {
        return getTotalDeposit() - getTotalUnpaidLoan();
    }

    public boolean getCanWitDraw() {
        return getMembershipStatus().equals("Closed");
    }

    @XmlTransient
    public Collection<LoanRecovery> getLoanRecoveryCollection() {
        return loanRecoveryCollection;
    }

    public void setLoanRecoveryCollection(Collection<LoanRecovery> loanRecoveryCollection) {
        this.loanRecoveryCollection = loanRecoveryCollection;
    }

    public float getTotalRecoverableFromLoan() {
        float total = (float) 0.0;
        if (getGuarantorsCollection() != null) {
            for (Guarantors g : getGuarantorsCollection()) {
                total += g.getRecoveryEst();
            }
        }
        return total;
    }

    public void setRandomInt(int randomInt) {
        this.randomInt = randomInt;
    }

    @XmlTransient
    public int getDeffaultCount() {
        return deffaultCount;
    }

    public void setDeffaultCount(int deffaultCount) {
        this.deffaultCount = deffaultCount;
    }

    public String getTrimmedSurname() {
        return getSurname().replace(" ", "");
    }

    @XmlTransient
    public PriorityAccOptions getPriorityAccount() {
        return priorityAccount;
    }

    public void setPriorityAccount(PriorityAccOptions priorityAccount) {
        this.priorityAccount = priorityAccount;
    }

    public boolean hasPendingEmergency() {
        for (Loan l : getLoanCollection()) {
            if (l.getPedding() && l.getLoantype().getId() == 1) {
                return true;
            }
        }
        return false;
    }

    public boolean hasPendingNormal() {
        for (Loan l : getLoanCollection()) {
            if (l.getPedding() && l.getLoantype().getId() == 2) {
                return true;
            }
        }
        return false;
    }

    public int getBalanceForPendingLoanNormal() {
        for (Loan l : getLoanCollection()) {
            if (l.getPedding() && l.getLoantype().getId() == 2) {
                return (int) (l.getCurrentSchedule().getTotalDebits() + l.getCurrentSchedule().getBalance());
            }
        }
        return 0;
    }

    public int getBalanceForPendingLoanEmergency() {
        for (Loan l : getLoanCollection()) {
            if (l.getPedding() && l.getLoantype().getId() == 1) {
                return (int) (l.getCurrentSchedule().getTotalDebits() + l.getCurrentSchedule().getBalance());
            }
        }
        return 0;
    }

    @XmlTransient
    public Collection<GeneralTransaction> getGeneralTransactionCollection() {
        return generalTransactionCollection;
    }

    public void setGeneralTransactionCollection(Collection<GeneralTransaction> generalTransactionCollection) {
        this.generalTransactionCollection = generalTransactionCollection;
    }
}
