/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author francis
 */
@Entity
@Table(name = "bankings")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Bankings.findAll", query = "SELECT b FROM Bankings b"),
    @NamedQuery(name = "Bankings.findById", query = "SELECT b FROM Bankings b WHERE b.id = :id"),
    @NamedQuery(name = "Bankings.findByAmount", query = "SELECT b FROM Bankings b WHERE b.amount = :amount"),
    @NamedQuery(name = "Bankings.findBySlipNo", query = "SELECT b FROM Bankings b WHERE b.slipNo = :slipNo"),
    @NamedQuery(name = "Bankings.findByComments", query = "SELECT b FROM Bankings b WHERE b.comments = :comments")})
public class Bankings implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "date_banked")
    @Temporal(TemporalType.DATE)
    private Date dateBanked;
    @Basic(optional = false)
    @NotNull
    @Column(name = "date_collected")
    @Temporal(TemporalType.DATE)
    private Date dateCollected;
    @Basic(optional = false)
    @NotNull
    @Column(name = "transaction")
    @Temporal(TemporalType.TIMESTAMP)
    private Date transactiondate=new Date();
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "amount")
    private int amount;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "slip_no")
    private String slipNo;
    @Size(max = 45)
    @Column(name = "comments")
    private String comments;

    public Bankings() {
    }

    public Bankings(Integer id) {
        this.id = id;
    }

    public Bankings(Integer id, Date dateWhen, int amount, String slipNo) {
        this.id = id;
        this.amount = amount;
        this.slipNo = slipNo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getSlipNo() {
        return slipNo;
    }

    public void setSlipNo(String slipNo) {
        this.slipNo = slipNo;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Bankings)) {
            return false;
        }
        Bankings other = (Bankings) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Bankings[ id=" + id + " ]";
    }

    public Date getDateBanked() {
        return dateBanked;
    }

    public void setDateBanked(Date dateBanked) {
        this.dateBanked = dateBanked;
    }

    public Date getDateCollected() {
        return dateCollected;
    }

    public void setDateCollected(Date dateCollected) {
        this.dateCollected = dateCollected;
    }

    public Date getTransaction() {
        return transactiondate;
    }

    public void setTransaction(Date transactionDate) {
        this.transactiondate = transactionDate;
    }
    
}
