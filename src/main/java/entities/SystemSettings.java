/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author francis
 */
@Entity
@Table(name = "system_settings")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SystemSettings.findAll", query = "SELECT s FROM SystemSettings s"),
    @NamedQuery(name = "SystemSettings.findById", query = "SELECT s FROM SystemSettings s WHERE s.id = :id"),
    @NamedQuery(name = "SystemSettings.findByTitle", query = "SELECT s FROM SystemSettings s WHERE s.title = :title"),
    @NamedQuery(name = "SystemSettings.findByValue", query = "SELECT s FROM SystemSettings s WHERE s.value = :value")})
public class SystemSettings implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "title")
    private String title;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "value")
    private String value;

    public SystemSettings() {
    }

    public SystemSettings(Integer id) {
        this.id = id;
    }

    public SystemSettings(Integer id, String title, String value) {
        this.id = id;
        this.title = title;
        this.value = value;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SystemSettings)) {
            return false;
        }
        SystemSettings other = (SystemSettings) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.SystemSettings[ id=" + id + " ]";
    }
    
}
