/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author francis
 */
@Entity
@Table(name = "payment_summary")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PaymentSummary.findAll", query = "SELECT p FROM PaymentSummary p ORDER BY p.id DESC"),
    @NamedQuery(name = "PaymentSummary.findById", query = "SELECT p FROM PaymentSummary p WHERE p.id = :id"),
    @NamedQuery(name = "PaymentSummary.findByDatePrepared", query = "SELECT p FROM PaymentSummary p WHERE p.datePrepared = :datePrepared")})
public class PaymentSummary implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "date_prepared")
    @Temporal(TemporalType.DATE)
    private Date datePrepared;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "paymentSummaryId")
    private Collection<PaymentSummaryItems> paymentSummaryItemsCollection;

    public PaymentSummary() {
    }

    public PaymentSummary(Integer id) {
        this.id = id;
    }

    public PaymentSummary(Integer id, Date datePrepared) {
        this.id = id;
        this.datePrepared = datePrepared;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDatePrepared() {
        return datePrepared;
    }

    public String getDatePreparedStr() {
        return new SimpleDateFormat("dd-MM-yyyy").format(datePrepared);
    }

    public String getDatePreparedStrMonth() {
        return new SimpleDateFormat("MMMM yyyy").format(datePrepared);
    }
    
    public void setDatePrepared(Date datePrepared) {
        this.datePrepared = datePrepared;
    }

    @XmlTransient
    public Collection<PaymentSummaryItems> getPaymentSummaryItemsCollection() {
        return paymentSummaryItemsCollection;
    }

    public void setPaymentSummaryItemsCollection(Collection<PaymentSummaryItems> paymentSummaryItemsCollection) {
        this.paymentSummaryItemsCollection = paymentSummaryItemsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PaymentSummary)) {
            return false;
        }
        PaymentSummary other = (PaymentSummary) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.PaymentSummary[ id=" + id + " ]";
    }

    public int getTotalShares() {
        int total = 0;
        if (getPaymentSummaryItemsCollection() != null) {
            for (PaymentSummaryItems item : getPaymentSummaryItemsCollection()) {
                total += item.getPaidShares();
            }
        }
        return total;
    }

    public int getTotalDeposits() {
        int total = 0;
        if (getPaymentSummaryItemsCollection() != null) {
            for (PaymentSummaryItems item : getPaymentSummaryItemsCollection()) {
                total += item.getDepositedAmount();
            }
        }
        return total;
    }

    public int getTotalLoanNormal() {
        int total = 0;
        if (getPaymentSummaryItemsCollection() != null) {
            for (PaymentSummaryItems item : getPaymentSummaryItemsCollection()) {
                total += item.getNormalLoanPaid();
            }
        }
        return total;
    }

    public int getTotalLoanNormalInterest() {
        int total = 0;
        if (getPaymentSummaryItemsCollection() != null) {
            for (PaymentSummaryItems item : getPaymentSummaryItemsCollection()) {
                total += item.getNormalLoanInterestPaid();
            }
        }
        return total;
    }

    public int getTotalLoanEmergency() {
        int total = 0;
        if (getPaymentSummaryItemsCollection() != null) {
            for (PaymentSummaryItems item : getPaymentSummaryItemsCollection()) {
                total += item.getEmergencyLoanPaid();
            }
        }
        return total;
    }

    public int getTotalEmergency() {
        int total = 0;
        if (getPaymentSummaryItemsCollection() != null) {
            for (PaymentSummaryItems item : getPaymentSummaryItemsCollection()) {
                total += item.getEmergencyLoanPaid();
            }
        }
        return total;
    }

    public int getTotalEmergencyInterest() {
        int total = 0;
        if (getPaymentSummaryItemsCollection() != null) {
            for (PaymentSummaryItems item : getPaymentSummaryItemsCollection()) {
                total += item.getEmergencyLoanInterestPaid();
            }
        }
        return total;
    }

    public int getTotalWithDrawal() {
        int total = 0;
        if (getPaymentSummaryItemsCollection() != null) {
            for (PaymentSummaryItems item : getPaymentSummaryItemsCollection()) {
                total += item.getWithdrawal();
            }
        }
        return total;
    }

    public int getTotalWithDrawalIncome() {
        int total = 0;
        if (getPaymentSummaryItemsCollection() != null) {
            for (PaymentSummaryItems item : getPaymentSummaryItemsCollection()) {
                total += item.getWithdrawalincome();
            }
        }
        return total;
    }
    public int getTotalNegotiationPaid() {
        int total = 0;
        if (getPaymentSummaryItemsCollection() != null) {
            for (PaymentSummaryItems item : getPaymentSummaryItemsCollection()) {
                total += item.getNegotiation();
            }
        }
        return total;
    }
    public int getTotalRegistrationPaid() {
        int total = 0;
        if (getPaymentSummaryItemsCollection() != null) {
            for (PaymentSummaryItems item : getPaymentSummaryItemsCollection()) {
                total += item.getRegistration();
            }
        }
        return total;
    }
}
