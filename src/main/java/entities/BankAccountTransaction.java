
package entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "bank_account_transaction")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BankAccountTransaction.findAll", query = "SELECT b FROM BankAccountTransaction b"),
    @NamedQuery(name = "BankAccountTransaction.findById", query = "SELECT b FROM BankAccountTransaction b WHERE b.id = :id"),
    @NamedQuery(name = "BankAccountTransaction.findByDescription", query = "SELECT b FROM BankAccountTransaction b WHERE b.description = :description"),
    @NamedQuery(name = "BankAccountTransaction.findByAmountCredit", query = "SELECT b FROM BankAccountTransaction b WHERE b.amountCredit = :amountCredit"),
    @NamedQuery(name = "BankAccountTransaction.findByAmountDebit", query = "SELECT b FROM BankAccountTransaction b WHERE b.amountDebit = :amountDebit"),
    @NamedQuery(name = "BankAccountTransaction.findByAccountBalance", query = "SELECT b FROM BankAccountTransaction b WHERE b.accountBalance = :accountBalance"),
    @NamedQuery(name = "BankAccountTransaction.findByDateWhen", query = "SELECT b FROM BankAccountTransaction b WHERE b.dateWhen = :dateWhen"),
    @NamedQuery(name = "BankAccountTransaction.findByDateCreated", query = "SELECT b FROM BankAccountTransaction b WHERE b.dateCreated = :dateCreated")})
public class BankAccountTransaction implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "isCurrent")
    private boolean isCurrent;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "description")
    private String description;
    @Basic(optional = false)
    @NotNull
    @Column(name = "amount_credit")
    private int amountCredit;
    @Basic(optional = false)
    @NotNull
    @Column(name = "amount_debit")
    private int amountDebit;
    @Basic(optional = false)
    @NotNull
    @Column(name = "account_balance")
    private int accountBalance;
    @Basic(optional = false)
    @NotNull
    @Column(name = "date_when")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateWhen;
    @Basic(optional = false)
    @NotNull
    @Column(name = "date_created")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreated;
    @JoinColumn(name = "account", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private BankAccount account;

    public BankAccountTransaction() {
    }

    public BankAccountTransaction(Integer id) {
        this.id = id;
    }

    public BankAccountTransaction(Integer id, String description, int amountCredit, int amountDebit, int accountBalance, Date dateWhen, Date dateCreated) {
        this.id = id;
        this.description = description;
        this.amountCredit = amountCredit;
        this.amountDebit = amountDebit;
        this.accountBalance = accountBalance;
        this.dateWhen = dateWhen;
        this.dateCreated = dateCreated;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getAmountCredit() {
        return amountCredit;
    }

    public void setAmountCredit(int amountCredit) {
        this.amountCredit = amountCredit;
    }

    public int getAmountDebit() {
        return amountDebit;
    }

    public void setAmountDebit(int amountDebit) {
        this.amountDebit = amountDebit;
    }

    public int getAccountBalance() {
        return accountBalance;
    }

    public void setAccountBalance(int accountBalance) {
        this.accountBalance = accountBalance;
    }

    public Date getDateWhen() {
        return dateWhen;
    }

    public void setDateWhen(Date dateWhen) {
        this.dateWhen = dateWhen;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public BankAccount getAccount() {
        return account;
    }

    public void setAccount(BankAccount account) {
        this.account = account;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BankAccountTransaction)) {
            return false;
        }
        BankAccountTransaction other = (BankAccountTransaction) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.BankAccountTransaction[ id=" + id + " ]";
    }

    public boolean getIsCurrent() {
        return isCurrent;
    }

    public void setIsCurrent(boolean isCurrent) {
        this.isCurrent = isCurrent;
    }

}
