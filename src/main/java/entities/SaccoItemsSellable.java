/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author francis
 */
@Entity
@Table(name = "sacco_items_sellable")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SaccoItemsSellable.findAll", query = "SELECT s FROM SaccoItemsSellable s"),
    @NamedQuery(name = "SaccoItemsSellable.findById", query = "SELECT s FROM SaccoItemsSellable s WHERE s.id = :id"),
    @NamedQuery(name = "SaccoItemsSellable.findByTitle", query = "SELECT s FROM SaccoItemsSellable s WHERE s.title = :title"),
    @NamedQuery(name = "SaccoItemsSellable.findByDescription", query = "SELECT s FROM SaccoItemsSellable s WHERE s.description = :description"),
    @NamedQuery(name = "SaccoItemsSellable.findByValue", query = "SELECT s FROM SaccoItemsSellable s WHERE s.value = :value")})
public class SaccoItemsSellable implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "title")
    private String title;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "description")
    private String description;
    @Basic(optional = false)
    @NotNull
    @Column(name = "value")
    private int value;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "source")
    private Collection<SaccoIncome> saccoIncomeCollection;

    public SaccoItemsSellable() {
    }

    public SaccoItemsSellable(Integer id) {
        this.id = id;
    }

    public SaccoItemsSellable(Integer id, String title, String description, int value) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.value = value;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @XmlTransient
    public Collection<SaccoIncome> getSaccoIncomeCollection() {
        return saccoIncomeCollection;
    }

    public void setSaccoIncomeCollection(Collection<SaccoIncome> saccoIncomeCollection) {
        this.saccoIncomeCollection = saccoIncomeCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SaccoItemsSellable)) {
            return false;
        }
        SaccoItemsSellable other = (SaccoItemsSellable) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.SaccoItemsSellable[ id=" + id + " ]";
    }
    
}
