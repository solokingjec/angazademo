
package entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "general_transaction")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "GeneralTransaction.findAll", query = "SELECT g FROM GeneralTransaction g"),
    @NamedQuery(name = "GeneralTransaction.findById", query = "SELECT g FROM GeneralTransaction g WHERE g.id = :id"),
    @NamedQuery(name = "GeneralTransaction.findByDateCollected", query = "SELECT g FROM GeneralTransaction g WHERE g.dateCollected = :dateCollected"),
    @NamedQuery(name = "GeneralTransaction.findBySlipNo", query = "SELECT g FROM GeneralTransaction g WHERE g.slipNo = :slipNo"),
    @NamedQuery(name = "GeneralTransaction.findByChequeNo", query = "SELECT g FROM GeneralTransaction g WHERE g.chequeNo = :chequeNo"),
    @NamedQuery(name = "GeneralTransaction.findByTransactedBy", query = "SELECT g FROM GeneralTransaction g WHERE g.transactedBy = :transactedBy"),
    @NamedQuery(name = "GeneralTransaction.findByDateWhen", query = "SELECT g FROM GeneralTransaction g WHERE g.dateWhen = :dateWhen")})
public class GeneralTransaction implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "random_int")
    private int randomInt;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "date_collected")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCollected;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "slip_no")
    private String slipNo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "cheque_no")
    private String chequeNo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "transacted_by")
    private String transactedBy;
    @Basic(optional = false)
    @NotNull
    @Column(name = "date_when")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateWhen;
    @JoinColumn(name = "member", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Members member1;
    @JoinColumn(name = "payment_mode", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private PaymentModes paymentMode;
    @JoinColumn(name = "bank_account", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private BankAccount bankAccount;

    public GeneralTransaction() {
    }

    public GeneralTransaction(Integer id) {
        this.id = id;
    }

    public GeneralTransaction(Integer id, Date dateCollected, String slipNo, String chequeNo, String transactedBy, Date dateWhen) {
        this.id = id;
        this.dateCollected = dateCollected;
        this.slipNo = slipNo;
        this.chequeNo = chequeNo;
        this.transactedBy = transactedBy;
        this.dateWhen = dateWhen;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDateCollected() {
        return dateCollected;
    }

    public void setDateCollected(Date dateCollected) {
        this.dateCollected = dateCollected;
    }

    public String getSlipNo() {
        return slipNo;
    }

    public void setSlipNo(String slipNo) {
        this.slipNo = slipNo;
    }

    public String getChequeNo() {
        return chequeNo;
    }

    public void setChequeNo(String chequeNo) {
        this.chequeNo = chequeNo;
    }

    public String getTransactedBy() {
        return transactedBy;
    }

    public void setTransactedBy(String transactedBy) {
        this.transactedBy = transactedBy;
    }

    public Date getDateWhen() {
        return dateWhen;
    }

    public void setDateWhen(Date dateWhen) {
        this.dateWhen = dateWhen;
    }

    public Members getMember1() {
        return member1;
    }

    public void setMember1(Members member1) {
        this.member1 = member1;
    }

    public PaymentModes getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(PaymentModes paymentMode) {
        this.paymentMode = paymentMode;
    }

    public BankAccount getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GeneralTransaction)) {
            return false;
        }
        GeneralTransaction other = (GeneralTransaction) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.GeneralTransaction[ id=" + id + " ]";
    }

    public int getRandomInt() {
        return randomInt;
    }

    public void setRandomInt(int randomInt) {
        this.randomInt = randomInt;
    }

}
