/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import controllers.util.AppConstants;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import org.eclipse.persistence.annotations.Cache;

/**
 *
 * @author francis
 */
@Cache(alwaysRefresh = true)
@Entity
@Table(name = "loan_schedule_transactions")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LoanScheduleTransactions.findAll", query = "SELECT l FROM LoanScheduleTransactions l"),
    @NamedQuery(name = "LoanScheduleTransactions.findById", query = "SELECT l FROM LoanScheduleTransactions l WHERE l.id = :id"),
    @NamedQuery(name = "LoanScheduleTransactions.findByAmountDebit", query = "SELECT l FROM LoanScheduleTransactions l WHERE l.amountDebit = :amountDebit"),
    @NamedQuery(name = "LoanScheduleTransactions.findByDateCollected", query = "SELECT l FROM LoanScheduleTransactions l WHERE l.dateCollected = :dateCollected"),
    @NamedQuery(name = "LoanScheduleTransactions.findByAmountCredit", query = "SELECT l FROM LoanScheduleTransactions l WHERE l.amountCredit = :amountCredit"),
    @NamedQuery(name = "LoanScheduleTransactions.findByDetails", query = "SELECT l FROM LoanScheduleTransactions l WHERE l.details = :details")})
public class LoanScheduleTransactions implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "amount_debit")
    private double amountDebit;
    @Basic(optional = false)
    @NotNull
    @Column(name = "date_collected")
    @Temporal(TemporalType.DATE)
    private Date dateCollected=new Date();
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "amount_credit")
    private Double amountCredit;
    @Size(max = 45)
    @Column(name = "details")
    private String details;
    @JoinColumn(name = "schedule_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private LoanSchedules scheduleId;

    public LoanScheduleTransactions() {
    }

    public LoanScheduleTransactions(Integer id) {
        this.id = id;
    }

    public LoanScheduleTransactions(Integer id, double amountDebit, Date dateCollected) {
        this.id = id;
        this.amountDebit = amountDebit;
        this.dateCollected = dateCollected;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public double getAmountDebit() {
        return amountDebit;
    }

    public void setAmountDebit(double amountDebit) {
        this.amountDebit = amountDebit;
    }

    public Date getDateCollected() {
        return dateCollected;
    }

    public void setDateCollected(Date dateCollected) {
        this.dateCollected = dateCollected;
    }

    public Double getAmountCredit() {
        return amountCredit;
    }

    public void setAmountCredit(Double amountCredit) {
        this.amountCredit = amountCredit;
    }

    public String getDetails() {
        AppConstants.logger(details);
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public LoanSchedules getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(LoanSchedules scheduleId) {
        this.scheduleId = scheduleId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LoanScheduleTransactions)) {
            return false;
        }
        LoanScheduleTransactions other = (LoanScheduleTransactions) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "LoanScheduleTransactions{"
                + "id=" + id
                + ", amountDebit=" + amountDebit
                + ", amountCredit=" + amountCredit
                + ", details=" + details
                + ", Schedule Id=" + getScheduleId().getId() + '}';
    }

}
