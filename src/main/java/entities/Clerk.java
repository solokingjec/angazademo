package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "clerk")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Clerk.findAll", query = "SELECT c FROM Clerk c"),
    @NamedQuery(name = "Clerk.findById", query = "SELECT c FROM Clerk c WHERE c.id = :id"),
    @NamedQuery(name = "Clerk.findByName", query = "SELECT c FROM Clerk c WHERE c.name = :name"),
    @NamedQuery(name = "Clerk.findByPhoneNumber", query = "SELECT c FROM Clerk c WHERE c.phoneNumber = :phoneNumber"),
    @NamedQuery(name = "Clerk.findByIdNumber", query = "SELECT c FROM Clerk c WHERE c.idNumber = :idNumber")})
public class Clerk implements Serializable {

    @Column(name = "deleted")
    private Boolean deleted;

    @Basic(optional = false)
    @NotNull
    @Column(name = "status_active")
    private boolean statusActive;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "phone_number")
    private String phoneNumber;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "id_number")
    private String idNumber;
    @JoinColumn(name = "username", referencedColumnName = "username")
    @ManyToOne
    private Users username;
    public Clerk() {
    }

    public Clerk(Integer id) {
        this.id = id;
    }

    public Clerk(Integer id, String name, String phoneNumber, String idNumber) {
        this.id = id;
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.idNumber = idNumber;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public Users getUsername() {
        return username;
    }

    public void setUsername(Users username) {
        this.username = username;
    }

    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Clerk)) {
            return false;
        }
        Clerk other = (Clerk) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Clerk[ id=" + id + " ]";
    }
    

    public boolean getStatusActive() {
        return statusActive;
    }

    public void setStatusActive(boolean statusActive) {
        this.statusActive = statusActive;
    }

    public String getStatus() {
        if (getStatusActive()) {
            return "Active";
        }
        return "Inactive";
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}
