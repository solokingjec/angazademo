/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author francis
 */
@Entity
@Table(name = "account_transitions")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AccountTransitions.findAll", query = "SELECT a FROM AccountTransitions a"),
    @NamedQuery(name = "AccountTransitions.findById", query = "SELECT a FROM AccountTransitions a WHERE a.id = :id"),
    @NamedQuery(name = "AccountTransitions.findByDateWhen", query = "SELECT a FROM AccountTransitions a WHERE a.dateWhen = :dateWhen")})
public class AccountTransitions implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "isCurrent")
    private boolean isCurrent;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "date_when")
    @Temporal(TemporalType.DATE)
    private Date dateWhen;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 2147483647)
    @Column(name = "comments")
    private String comments;
    @JoinColumn(name = "account", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Members account;
    @JoinColumn(name = "status", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private AccountStatus status;

    public AccountTransitions() {
    }

    public AccountTransitions(Integer id) {
        this.id = id;
    }

    public AccountTransitions(Integer id, Date dateWhen, String comments, boolean isCurrent) {
        this.id = id;
        this.dateWhen = dateWhen;
        this.comments = comments;
        this.isCurrent = isCurrent;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDateWhen() {
        return dateWhen;
    }

    public void setDateWhen(Date dateWhen) {
        this.dateWhen = dateWhen;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Members getAccount() {
        return account;
    }

    public void setAccount(Members account) {
        this.account = account;
    }

    public AccountStatus getStatus() {
        return status;
    }

    public void setStatus(AccountStatus status) {
        this.status = status;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AccountTransitions)) {
            return false;
        }
        AccountTransitions other = (AccountTransitions) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.AccountTransitions[ id=" + id + " ]";
    }

    public boolean getIsCurrent() {
        return isCurrent;
    }

    public void setIsCurrent(boolean isCurrent) {
        this.isCurrent = isCurrent;
    }

}
