/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import controllers.util.AppConstants;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.eclipse.persistence.annotations.Cache;

/**
 *
 * @author francis
 */
@Cache(alwaysRefresh = true)
@Entity
@Table(name = "loan")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Loan.findAll", query = "SELECT l FROM Loan l"),
    @NamedQuery(name = "Loan.findByLoanId", query = "SELECT l FROM Loan l WHERE l.loanId = :loanId"),
    @NamedQuery(name = "Loan.findByDuration", query = "SELECT l FROM Loan l WHERE l.duration = :duration"),
    @NamedQuery(name = "Loan.findByIssueDate", query = "SELECT l FROM Loan l WHERE l.issueDate = :issueDate"),
    @NamedQuery(name = "Loan.findByAmount", query = "SELECT l FROM Loan l WHERE l.amount = :amount"),
    @NamedQuery(name = "Loan.findByInterestRate", query = "SELECT l FROM Loan l WHERE l.interestRate = :interestRate")})
public class Loan implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "loan_status")
    private boolean loanStatus;

    @Basic(optional = false)
    @NotNull
    @Column(name = "stop_billing")
    private boolean stopBilling;

    @Basic(optional = false)
    @NotNull
    @Column(name = "pedding")
    private boolean pedding;

    @Basic(optional = false)
    @NotNull
    @Column(name = "grace_period_count")
    private int gracePeriodCount;

    @Basic(optional = false)
    @NotNull
    @Column(name = "deffault_count")
    private int deffaultCount;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "loanId")
    private Collection<LoanRecovery> loanRecoveryCollection;

    @Basic(optional = false)
    @NotNull
    @Column(name = "negotiation_fee")
    private int negotiationFee;
    @JoinColumn(name = "loantype", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private LoanTypes loantype;
    @Basic(optional = false)
    @NotNull
    @Column(name = "random_int")
    private int randomInt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "issue_date")
    @Temporal(TemporalType.DATE)
    private Date issueDate = new Date();
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "loan_id")
    private Integer loanId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "duration")
    private int duration = 36;
    @Basic(optional = false)
    @NotNull
    @Column(name = "amount")
    private double amount;
    @Basic(optional = false)
    @NotNull
    @Column(name = "interest_rate")
    private double interestRate;
    @JoinColumn(name = "memeber_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Members memeberId;
    @OneToMany(mappedBy = "loanId")
    private Collection<LoanSchedules> loanSchedulesCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "loanId")
    private Collection<Guarantors> guarantorsCollection;

    public Loan() {
    }

    public Loan(Integer loanId) {
        this.loanId = loanId;
    }

    public Loan(Integer loanId, int duration, Date issueDate, double amount, double interestRate) {
        this.loanId = loanId;
        this.duration = duration;
        this.issueDate = issueDate;
        this.amount = amount;
        this.interestRate = interestRate;
    }

    public Integer getLoanId() {
        return loanId;
    }

    public void setLoanId(Integer loanId) {
        this.loanId = loanId;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(double interestRate) {
        this.interestRate = interestRate;
    }

    public Members getMemeberId() {
        return memeberId;
    }

    public void setMemeberId(Members memeberId) {
        this.memeberId = memeberId;
    }

    @XmlTransient
    public Collection<LoanSchedules> getLoanSchedulesCollection() {
        return loanSchedulesCollection;
    }

    public void setLoanSchedulesCollection(Collection<LoanSchedules> loanSchedulesCollection) {
        this.loanSchedulesCollection = loanSchedulesCollection;
    }

    @XmlTransient
    public Collection<Guarantors> getGuarantorsCollection() {
        return guarantorsCollection;
    }

    public void setGuarantorsCollection(Collection<Guarantors> guarantorsCollection) {
        this.guarantorsCollection = guarantorsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (loanId != null ? loanId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Loan)) {
            return false;
        }
        Loan other = (Loan) object;
        if ((this.loanId == null && other.loanId != null) || (this.loanId != null && !this.loanId.equals(other.loanId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Loan{" + "deffaultCount=" + deffaultCount + ", negotiationFee=" + negotiationFee + ", loantype=" + loantype + ", loanId=" + loanId + ", loanSchedulesCollection=" + loanSchedulesCollection + '}';
    }

    public Date getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }

    public int getRandomInt() {
        return randomInt;
    }

    public void setRandomInt(int randomInt) {
        this.randomInt = randomInt;
    }

    public LoanSchedules getCurrentSchedule() {
        if (getLoanSchedulesCollection() != null) {
            for (LoanSchedules schedule : getLoanSchedulesCollection()) {
                if (schedule.getStatusCleared().getStatus().equalsIgnoreCase("Current")) {
                    return schedule;
                }
            }
        }
        return new LoanSchedules();
    }

    public LoanTypes getLoantype() {
        return loantype;
    }

    public void setLoantype(LoanTypes loantype) {
        this.loantype = loantype;
    }

    public int getBalEst() {
        if (!getLoanStatus()) {
            if (getLoantype() != null) {
                if (getLoantype().getId() == 1) {
                    if (!getPedding() && getMemeberId().hasPendingEmergency()) {
                        AppConstants.logger("has pending:");
                        return 0;
                    } else if (!getPedding() && !getMemeberId().hasPendingEmergency()) {
                        return getMemeberId().getEstimatedLoanBalanceEmergency();
                    } else {
                        AppConstants.logger("getMemeberId().getBalanceForPendingLoanEmergency()---:" + getMemeberId().getBalanceForPendingLoanEmergency());
                        AppConstants.logger("getCollected()---:" + getCollected());
                        return (int) (Math.floor((getMemeberId().getBalanceForPendingLoanEmergency() - getCollectedActual())));
                    }
                } else if (!getLoanStatus() && getLoantype().getId() == 2) {
                    if (!getPedding() && getMemeberId().hasPendingNormal()) {
                        AppConstants.logger("has pending:");
                        return 0;
                    } else if (!getPedding() && !getMemeberId().hasPendingNormal()) {
                        AppConstants.logger("has no pending:");
                        return getMemeberId().getEstimatedLoanBalanceNormal();
                    } else {
                        AppConstants.logger("calc");

                        return (int) (Math.floor((getMemeberId().getBalanceForPendingLoanNormal() - getCollectedActual())));
                    }
                }
            }
        }
        return 0;
    }

    public int getBalEstWithoutInterest() {
        if (!getLoanStatus() && getLoantype().getId() == 1) {
            if (!getPedding() && getMemeberId().hasPendingEmergency()) {
                return 0;
            } else if (!getPedding() && !getMemeberId().hasPendingEmergency()) {
                return getMemeberId().getEstimatedLoanBalanceEmergencyWithoutInterest();
            } else if (getPedding()) {
                return (int) (Math.floor((getMemeberId().getBalanceForPendingLoanEmergency() - getCollected())));
            }
        } else if (!getLoanStatus() && getLoantype().getId() == 2) {
            if (!getPedding() && getMemeberId().hasPendingNormal()) {
                return 0;
            } else if (!getPedding() && !getMemeberId().hasPendingNormal()) {
                return getMemeberId().getEstimatedLoanBalanceNormalWithoutInterests();
            } else {
                AppConstants.logger("loan normal pending:" + getMemeberId().getBalanceForPendingLoanNormal());
                return (int) (Math.floor((getMemeberId().getBalanceForPendingLoanNormal() - getCollectedActual())));
            }
        }
        return 0;
//
//        if (getLoanStatus() != null && getLoanStatus().getIdloanStatus() == 2 && getLoantype().getId() == 1) {
//            return getMemeberId().getEstimatedLoanBalanceEmergencyWithoutInterest();
//        } else if (getLoanStatus() != null && getLoanStatus().getIdloanStatus() == 2 && getLoantype().getId() == 2) {
//            return getMemeberId().getEstimatedLoanBalanceNormalWithoutInterests();
//        } else {
//            return 0;
//        }
    }

    public int getCollectedActual() {
        if (!getLoanStatus()) {
            if (!getPedding()) {
                AppConstants.logger("Loan not pending");
                if (!getMemeberId().hasPendingEmergency() && !getMemeberId().hasPendingNormal()) {
                    AppConstants.logger("member has no pending");
                    return getCollected();
                } else if (getLoantype().getId() == 1 && getMemeberId().hasPendingEmergency()) {
                    AppConstants.logger("Loan is emegency and member has pending");
                    return (int) ((getMemeberId().getActiveLoanEmergency().getCurrentSchedule().getTotalDebits() + getMemeberId().getActiveLoanEmergency().getCurrentSchedule().getBalance()));
                } else if (getLoantype().getId() == 2 && getMemeberId().hasPendingNormal()) {
                    AppConstants.logger("Loan is normal and member has pending");
                    return (int) (getCurrentSchedule().getTotalDebits() + getCurrentSchedule().getBalance());
                }
            } else {
                AppConstants.logger("Loan is pending");
                if (getLoantype().getId() == 1 && getMemeberId().hasPendingEmergency()) {
                    return (int) (getCollected() - (getMemeberId().getActiveLoanEmergency().getCurrentSchedule().getTotalDebits() + getMemeberId().getActiveLoanEmergency().getCurrentSchedule().getBalance()));
                } else if (getLoantype().getId() == 2 && getMemeberId().hasPendingNormal()) {
                    return (int) (getCollected() - (getMemeberId().getActiveLoanNormal().getCurrentSchedule().getTotalDebits() + getMemeberId().getActiveLoanNormal().getCurrentSchedule().getBalance()));
                }

            }
        }
        return 0;
    }

    public int getCollected() {
        if (getLoantype().getId() == 1 && !getLoanStatus()) {
            return getMemeberId().getTotalUnAccountedEmergencyLoans();
        } else if (getLoantype().getId() == 2 && !getLoanStatus()) {
            return getMemeberId().getTotalUnAccountedNormalLoans();
        }
        return 0;
    }

    public int getTotalInterestEarned() {
        int sum = 0;
        for (LoanSchedules ls : getLoanSchedulesCollection()) {
            sum += ls.getPaidInterest();
        }
        return sum;
    }

    public int getNegotiationFee() {
        return negotiationFee;
    }

    public void setNegotiationFee(int negotiationFee) {
        this.negotiationFee = negotiationFee;
    }

    public int getTotalGuaranteedAmount() {
        int total = 0;
        if (getGuarantorsCollection() != null) {
            for (Guarantors g : getGuarantorsCollection()) {
                total += g.getGuaranteedAmount();
            }
        }
        return total;
    }

    @XmlTransient
    public Collection<LoanRecovery> getLoanRecoveryCollection() {
        return loanRecoveryCollection;
    }

    public void setLoanRecoveryCollection(Collection<LoanRecovery> loanRecoveryCollection) {
        this.loanRecoveryCollection = loanRecoveryCollection;
    }

    public int getDeffaultCount() {
        return deffaultCount;
    }

    public void setDeffaultCount(int deffaultCount) {
        this.deffaultCount = deffaultCount;
    }

    public boolean getIsDeffaulted() {
        return getDeffaultCount() > 2;
    }

    public int getGracePeriodCount() {
        return gracePeriodCount;
    }

    public void setGracePeriodCount(int gracePeriodCount) {
        this.gracePeriodCount = gracePeriodCount;
    }

    public boolean getPedding() {
        return pedding;
    }

    public void setPedding(boolean pedding) {
        this.pedding = pedding;
    }

    public boolean getStopBilling() {
        return stopBilling;
    }

    public void setStopBilling(boolean stopBilling) {
        this.stopBilling = stopBilling;
    }

    public boolean getLoanStatus() {
        return loanStatus;
    }

    public void setLoanStatus(boolean loanStatus) {
        this.loanStatus = loanStatus;
    }
    public String getStatusStr(){
        if(getLoanStatus()){
            return "Cleared";
        }
            return "Ongoing";
    }
    public String getBillingStatusStr(){
        if(!getStopBilling()){
            return "Billing";
        }
            return "Paused";
    }
}
