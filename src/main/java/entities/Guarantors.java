/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import org.eclipse.persistence.annotations.Cache;

/**
 *
 * @author francis
 */
@Cache(alwaysRefresh = true)
@Entity
@Table(name = "guarantors")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Guarantors.findAll", query = "SELECT g FROM Guarantors g"),
    @NamedQuery(name = "Guarantors.findByGuaranteedAmount", query = "SELECT g FROM Guarantors g WHERE g.guaranteedAmount = :guaranteedAmount"),
    @NamedQuery(name = "Guarantors.findById", query = "SELECT g FROM Guarantors g WHERE g.id = :id")})
public class Guarantors implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "recovered_amount")
    private float recoveredAmount;
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "guaranteed_amount")
    private int guaranteedAmount;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @JoinColumn(name = "guarantor_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Members guarantorId;
    @JoinColumn(name = "loan_id", referencedColumnName = "loan_id")
    @ManyToOne(optional = false)
    private Loan loanId;

    public Guarantors() {
    }

    public Guarantors(Integer id) {
        this.id = id;
    }

    public Guarantors(Integer id, int guaranteedAmount) {
        this.id = id;
        this.guaranteedAmount = guaranteedAmount;
    }

    public int getGuaranteedAmount() {
        return guaranteedAmount;
    }

    public void setGuaranteedAmount(int guaranteedAmount) {
        this.guaranteedAmount = guaranteedAmount;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Members getGuarantorId() {
        return guarantorId;
    }

    public void setGuarantorId(Members guarantorId) {
        this.guarantorId = guarantorId;
    }

    public Loan getLoanId() {
        return loanId;
    }

    public void setLoanId(Loan loanId) {
        this.loanId = loanId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Guarantors)) {
            return false;
        }
        Guarantors other = (Guarantors) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Guarantors[ id=" + id + " ]";
    }
    public float getRatio()
    {
        return (float) ((float)getGuaranteedAmount()/(float)getLoanId().getTotalGuaranteedAmount());
    }

    public float getRecoveredAmount() {
        return recoveredAmount;
    }

    public void setRecoveredAmount(float recoveredAmount) {
        this.recoveredAmount = recoveredAmount;
    }
    public int getRecoverFromMembers() {
        if (getLoanId() != null) {
            if (getLoanId().getBalEst() > getLoanId().getMemeberId().getTotalDeposit()) {
                return getLoanId().getBalEst() - getLoanId().getMemeberId().getTotalDeposit();
            }
        }
        return 0;
    }
    public float getRecoveryEst()
    {
        return (float) (Math.ceil((float) getRecoverFromMembers() * getRatio()));
    }
}
