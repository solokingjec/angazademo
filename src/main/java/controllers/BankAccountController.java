package controllers;

import controllers.util.JsfUtil;
import controllers.util.JsfUtil.PersistAction;
import entities.BankAccount;
import entities.BankAccountTransaction;
import facades.BankAccountFacade;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Named;

@Named("bankAccountController")
@SessionScoped
public class BankAccountController implements Serializable {

    @EJB
    private facades.BankAccountFacade ejbFacade;
    private List<BankAccount> items = null;
    private BankAccount selected;
    private int openingBalance;

    public int getOpeningBalance() {
        return openingBalance;
    }

    public void setOpeningBalance(int openingBalance) {
        this.openingBalance = openingBalance;
    }

    public BankAccountController() {
    }

    public BankAccount getSelected() {
        return selected;
    }

    public void setSelected(BankAccount selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private BankAccountFacade getFacade() {
        return ejbFacade;
    }

    public BankAccount prepareCreate() {
        selected = new BankAccount();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        int rand = new Random().nextInt(99999999);
        selected.setRandomInt(rand);
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("BankAccountCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
            selected = getFacade().findByRandom(rand);
            selected.setRandomInt(-1);
            getFacade().edit(selected);
            BankAccountTransaction bankAccountTransaction = new BankAccountTransaction();
            bankAccountTransaction.setAccount(selected);
            bankAccountTransaction.setAccountBalance(openingBalance);
            bankAccountTransaction.setDateCreated(new Date());
            bankAccountTransaction.setDateWhen(new Date());
            bankAccountTransaction.setDescription("Opening balance");
            bankAccountTransaction.setIsCurrent(true);
            if (openingBalance > 0) {
                bankAccountTransaction.setAmountCredit(openingBalance);
            } else {
                bankAccountTransaction.setAmountDebit(openingBalance);
            }
            getFacade().createTransaction(bankAccountTransaction);
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("BankAccountUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("BankAccountDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<BankAccount> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public BankAccount getBankAccount(java.lang.Integer id) {
        return getFacade().find(id);
    }

    public List<BankAccount> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<BankAccount> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = BankAccount.class)
    public static class BankAccountControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            BankAccountController controller = (BankAccountController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "bankAccountController");
            return controller.getBankAccount(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof BankAccount) {
                BankAccount o = (BankAccount) object;
                return getStringKey(o.getId());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), BankAccount.class.getName()});
                return null;
            }
        }

    }

    public List<BankAccountTransaction> getTransactionsSelected() {
        return getFacade().findTransactions(selected);
    }

}
