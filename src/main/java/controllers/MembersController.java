package controllers;

import controllers.util.AppConstants;
import controllers.util.JsfUtil;
import controllers.util.JsfUtil.PersistAction;
import entities.AccountStatus;
import entities.AccountTransitions;
import entities.BillTransactions;
import entities.CollectionTypes;
import entities.Collections;
import entities.DepositCash;
import entities.GeneralTransaction;
import entities.Guarantors;
import entities.Loan;
import entities.LoanRecovery;
import entities.LoanScheduleTransactions;
import entities.LoanSchedules;
import entities.LoanTypes;
import entities.MemberBills;
import entities.Members;
import entities.PaymentSummary;
import entities.PaymentSummaryItems;
import entities.SaccoLedgerEntries;
import entities.SaccoLedgers;
import entities.ScheduleStatus;
import entities.SystemSettings;
import entities.Withdrawal;
import facades.MembersFacade;
import java.io.IOException;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.concurrent.Executor;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.joda.time.DateTime;
import org.primefaces.event.RowEditEvent;

@Named("membersController")
@SessionScoped
public class MembersController implements Serializable {

    private static ArrayList<LoanSchedules> list;

    @EJB
    private facades.MembersFacade ejbFacade;
    private List<Members> items = null;
    private Members selected;
    private int entryfee, registrationfee;
    private DepositCash depo = new DepositCash();
    private Loan loan = new Loan();
    private Date sdate = new Date(), edate = new Date(), lastUpdated;
    private int InterestRate, negotiation, withdrawalcharges, gracePeriod;
    private LoanScheduleTransactions itemtransaction = new LoanScheduleTransactions();
    private int noOfGuarantors = 5;
    private ArrayList<Guarantors> listg = new ArrayList<>();
    private PaymentSummary p;
    private CollectionTypes type;
    private String message;
    private Guarantors guarantor;
    private MemberBills bill;
    private ArrayList<Members> memberSelection = new ArrayList<>();
    private ArrayList<LoanSchedules> scheduleSelection = new ArrayList<>();
    private PaymentSummaryItems selectedPaymentSummaryItem;
    private int billAmount;
    private Date whenBilled;
    private String billNarration;
    private int loandeposit, sharedeposit, withrawable;
    private GeneralTransaction generalTransaction;

    public GeneralTransaction getGeneralTransaction() {
        return generalTransaction;
    }

    public void setGeneralTransaction(GeneralTransaction generalTransaction) {
        this.generalTransaction = generalTransaction;
    }

    public int getLoandeposit() {
        return loandeposit;
    }

    public void prepareCreateGeneralTransaction() {
        loandeposit = 0;
        sharedeposit = 0;
        withrawable = 0;
        generalTransaction = new GeneralTransaction();
        generalTransaction.setMember1(selected);
    }

    public void createGeneralTransaction() {

    }
    public void setLoandeposit(int loandeposit) {
        this.loandeposit = loandeposit;
    }

    public int getSharedeposit() {
        return sharedeposit;
    }

    public void setSharedeposit(int sharedeposit) {
        this.sharedeposit = sharedeposit;
    }

    public int getWithrawable() {
        return withrawable;
    }

    public void setWithrawable(int withrawable) {
        this.withrawable = withrawable;
    }

    public int getBillAmount() {
        return billAmount;
    }

    public void setBillAmount(int billAmount) {
        this.billAmount = billAmount;
    }

    public Date getWhenBilled() {
        return whenBilled;
    }

    public void setWhenBilled(Date whenBilled) {
        this.whenBilled = whenBilled;
    }

    public String getBillNarration() {
        return billNarration;
    }

    public void setBillNarration(String billNarration) {
        this.billNarration = billNarration;
    }

    @PostConstruct
    public void loadProfits() {
        SystemSettings st = getFacade().getSettings(1);
        entryfee = Integer.parseInt(st.getValue());
        st = getFacade().getSettings(2);
        registrationfee = Integer.parseInt(st.getValue());
        st = getFacade().getSettings(3);
        InterestRate = Integer.parseInt(st.getValue());
        st = getFacade().getSettings(4);
        negotiation = Integer.parseInt(st.getValue());
        //st = getFacade().getSettings(5);
        //loanformcharges = Integer.parseInt(st.getValue());
        st = getFacade().getSettings(6);
        withdrawalcharges = Integer.parseInt(st.getValue());
        st = getFacade().getSettings(8);
        emegencyinterestrate = Integer.parseInt(st.getValue());
        //st = getFacade().getSettings(9);
        //gracePeriod = Integer.parseInt(st.getValue());
        st = getFacade().getSettings(7);
        try {
            lastUpdated = new SimpleDateFormat("yyyy-MM-dd").parse(st.getValue());
        } catch (ParseException ex) {
            Logger.getLogger(MembersController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public PaymentSummaryItems getSelectedPaymentSummaryItem() {
        return selectedPaymentSummaryItem;
    }

    public void setSelectedPaymentSummaryItem(PaymentSummaryItems selectedPaymentSummaryItem) {
        this.selectedPaymentSummaryItem = selectedPaymentSummaryItem;
    }

    public ArrayList<LoanSchedules> getScheduleSelection() {
        return scheduleSelection;
    }

    public void setScheduleSelection(ArrayList<LoanSchedules> scheduleSelection) {
        this.scheduleSelection = scheduleSelection;
    }

    public CollectionTypes getType() {
        return type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public MemberBills getBill() {
        return bill;
    }

    public void setBill(MemberBills bill) {
        this.bill = bill;
    }

    public void setType(CollectionTypes type) {
        this.type = type;
    }

    public void createGuarantorSingle() {
        getFacade().storeGuarator(guarantor);
        selected = getFacade().findSelected(selected);
        loan = getFacade().findLoan(loan.getLoanId());
        FacesContext.getCurrentInstance().addMessage(
                null,
                new FacesMessage(FacesMessage.SEVERITY_INFO,
                        "Guarantors added", null));
    }

    @Inject
    private Executor executor;
    private int emegencyinterestrate;

    public int getNoOfGuarantors() {
        return noOfGuarantors;
    }

    public List<DepositCash> getAllDepositsShare() {
        return getFacade().getAllMembersDepositsNonWithDrawable(selected);
    }

    public List<DepositCash> getAllDepositsWithDrawable() {
        return getFacade().getAllMembersDepositsWithDrawable(selected);
    }

    public int getTotalSharesSelectedIn() {
        return getFacade().getTotalSharesIn(selected);
    }

    public int getTotalSharesSelectedOut() {
        return getFacade().getTotalSharesOut(selected);
    }

    public int getTotalWithDrawableIn() {
        return getFacade().getTotalWithDrawableIn(selected);
    }

    public int getTotalWithDrawableOut() {
        return getFacade().getTotalWithDrawableOut(selected);
    }

    public ArrayList<Members> getMemberSelection() {
        return memberSelection;
    }

    public void setMemberSelection(ArrayList<Members> memberSelection) {
        this.memberSelection = memberSelection;
    }

    public void deleteSchedule() {
        for (LoanSchedules l : scheduleSelection) {
            getFacade().deleteSchedule(l);
        }
        scheduleSelection.clear();
        loan = getFacade().findLoan(loan.getLoanId());
        JsfUtil.addSuccessMessage("Records Deleted");
    }

    public ArrayList<Guarantors> getGuarators() {
        if (listg.isEmpty()) {
            for (int x = 0; x < noOfGuarantors; x++) {
                Guarantors g = new Guarantors();
                g.setId(x);
                g.setGuaranteedAmount(0);
                listg.add(g);
            }
            return listg;
        } else {
            return listg;
        }
    }

    public ArrayList<Guarantors> getGuaratorsForTheLoan() {
        ArrayList<Guarantors> loanGuarantor = new ArrayList<>();
        for (Guarantors g : loan.getGuarantorsCollection()) {
            loanGuarantor.add(g);
            AppConstants.logger("G id:" + g.getId());
        }
        return loanGuarantor;
    }

    public void prepareCreateGuarantor() {
        listg = getGuaratorsForTheLoan();
    }

    public void prepareCreateGuarantorSingle() {
        guarantor = new Guarantors();
        guarantor.setLoanId(loan);
    }

    public Guarantors getGuarantor() {
        return guarantor;
    }

    public void setGuarantor(Guarantors guarantor) {
        this.guarantor = guarantor;
    }

    public void createLoan() {
        if (loan.getAmount() == 0) {
            FacesContext.getCurrentInstance().addMessage(
                    null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Loan has 0 amount", null));
            return;
        }
        if (loan.getLoantype().getId() == 1) {
            loan.setInterestRate(emegencyinterestrate);
        } else {
            loan.setInterestRate(InterestRate);
        }
        if (loan.getLoantype().getId() == 1 && selected.getActiveLoanEmergency() != null) {
            if (selected.getActiveLoanEmergency().getBalEst() == 0) {
                AppConstants.logger("member has an emergency loan but fully paid");
                loan.setPedding(true);
            } else {
                AppConstants.logger("member has an emergency loan. dont create");
                FacesContext.getCurrentInstance().addMessage(
                        null,
                        new FacesMessage(FacesMessage.SEVERITY_WARN,
                                "Member has an active emergency loan", null));
                return;
            }
        }
        if (loan.getLoantype().getId() == 2 && selected.getActiveLoanNormal() != null) {
            if (selected.getActiveLoanNormal().getBalEst() == 0) {
                AppConstants.logger("member has a normal loan but fully paid");
                loan.setPedding(true);
            } else {
                AppConstants.logger("member has an normal loan. dont create");
                FacesContext.getCurrentInstance().addMessage(
                        null,
                        new FacesMessage(FacesMessage.SEVERITY_WARN,
                                "Member has an active normal loan", null));
                return;
            }
        }

        int random = new Random().nextInt(10000);
        loan.setRandomInt(random);
        loan.setGracePeriodCount(gracePeriod);
        getFacade().storeLoan(loan);
        loan = getFacade().getLoanByRandom(random);
        loan.setRandomInt(-1);
        getFacade().storeLoan(loan);
        SaccoLedgerEntries sle = new SaccoLedgerEntries();
        sle.setCr(0);
        sle.setDateWhen(new Date());
        sle.setDr((int) loan.getAmount());
        sle.setLedgerId(new SaccoLedgers(22));
        sle.setIsGeneral(false);
        sle.setParticulars("members loans to " + loan.getMemeberId().getSurname());
        getFacade().saveLedgerEntry(sle);
        sle = new SaccoLedgerEntries();
        sle.setCr((int) loan.getAmount());
        sle.setDateWhen(new Date());
        sle.setDr(0);
        sle.setIsGeneral(false);
        sle.setParticulars("members loans to " + loan.getMemeberId().getSurname());
        sle.setLedgerId(new SaccoLedgers(7));
        getFacade().saveLedgerEntry(sle);
        int interest = (int) (loan.getAmount() * loan.getInterestRate() / 12 * 0.01);
        LoanSchedules item = new LoanSchedules();
        item.setPrincipleAmount((float) loan.getAmount());
        item.setInterest(interest);
        item.setBalance((float) loan.getAmount());
        item.setLoanId(loan);
        item.setStatusCleared(new ScheduleStatus(2));
        item.setRandomInt(random);
        DateTime today = new DateTime();
        DateTime nextAfterAMonth = today.plusMonths(1);
        Date objectDate = nextAfterAMonth.toDate();
        item.setDueDate(objectDate);
        item.setIsForLastMonth(true);
        getFacade().storeLoanSchedule(item);
        item = getFacade().getLoanSchedule(random);
        item.setRandomInt(-1);
        getFacade().mergeLoanSchedule(item);
        LoanScheduleTransactions lst = new LoanScheduleTransactions();
        lst.setAmountDebit(interest);
        lst.setAmountCredit(0.0);
        lst.setDateCollected(new Date());
        lst.setDetails("interest");
        lst.setScheduleId(item);
        getFacade().storeTransaction(lst);
        if (negotiation > 0) {
            lst.setAmountDebit(loan.getAmount() * negotiation / 100);
            lst.setDetails("loannegotiation");
            getFacade().storeTransaction(lst);
            loan.setNegotiationFee((int) Math.ceil(loan.getAmount() * negotiation / 100));
            getFacade().storeLoan(loan);
//            sle = new SaccoLedgerEntries();
//            sle.setCr((int) Math.ceil(loan.getAmount() * negotiation / 100));
//            sle.setDateWhen(new Date());
//            sle.setDr(0);
//            sle.setLedgerId(new SaccoLedgers(44));
//            getFacade().saveLedgerEntry(sle);
//            sle = new SaccoLedgerEntries();
//            sle.setCr(0);
//            sle.setDateWhen(new Date());
//            sle.setDr((int) Math.ceil(loan.getAmount() * negotiation / 100));
//            sle.setLedgerId(new SaccoLedgers(6));
//            getFacade().saveLedgerEntry(sle);
        }
        if (loan.getMemeberId().getTotalDepositWithrawable() > 0) {
//            lst.setAmountCredit((double) loan.getMemeberId().getTotalDepositWithrawable());
//            lst.setAmountDebit(0.0);
//            lst.setDateCollected(new Date());
//            lst.setDetails("paid from deposits");
//            lst.setScheduleId(item);
//            getFacade().storeTransaction(lst);
            Collections c = new Collections();
            c.setAmount(loan.getMemeberId().getTotalDepositWithrawable());
            c.setDateCollected(new Date());
            c.setMemberId(loan.getMemeberId());
            c.setReceiptNo(11111);
            c.setCollectionstype(new CollectionTypes(loan.getLoantype().getId() + 1));
            getFacade().storeCollections(c);

            DepositCash depo = new DepositCash();
            depo.setAmount(-loan.getMemeberId().getTotalDepositWithrawable());
            depo.setDate(new Date());
            depo.setDescription("paid loans");
            depo.setIsWithdrawable(true);
            depo.setTransactedBy("jackie");
            depo.setMemberNo(loan.getMemeberId());
            getFacade().storeDeposit(depo);

            sle = new SaccoLedgerEntries();
            sle.setDateWhen(new Date());
            sle.setCr(depo.getAmount() * -1);
            sle.setDateWhen(new Date());
            sle.setDr(0);
            sle.setParticulars("Loan Recovery");
            sle.setIsGeneral(true);
            sle.setLedgerId(new SaccoLedgers(22));
            getFacade().saveLedgerEntry(sle);
            sle = new SaccoLedgerEntries();
            sle.setCr(0);
            sle.setIsGeneral(true);
            sle.setParticulars("Loan Recovery");
            sle.setDateWhen(new Date());
            sle.setDr(depo.getAmount() * -1);
            sle.setLedgerId(new SaccoLedgers(8));
            getFacade().saveLedgerEntry(sle);
        }
        if (listg != null && listg.size() > 0) {
            int amountGuaranteed = getTotalForAllGuarantor();
            int amountToDistribute = (int) (loan.getAmount() - amountGuaranteed);
            ArrayList<Guarantors> withAmountNotSpecified = new ArrayList<>();
            for (Guarantors g : listg) {
                if (g.getGuaranteedAmount() == 0) {
                    withAmountNotSpecified.add(g);
                }
            }
            int forEachMember;
            try {
                forEachMember = (int) Math.ceil(amountToDistribute / withAmountNotSpecified.size());
            } catch (ArithmeticException e) {
                forEachMember = 0;
            }
            for (Guarantors g : listg) {
                AppConstants.logger("amount:" + g.getGuaranteedAmount());
                if (g.getGuarantorId() != null) {
                    if (g.getGuaranteedAmount() == 0) {
                        g.setGuaranteedAmount(forEachMember);
                    }
                    g.setId(null);
                    g.setLoanId(loan);
                    getFacade().storeGuarator(g);
                }
            }
        }
        selected = getFacade().findSelected(selected);
        JsfUtil.addSuccessMessage("Loan Created");
    }

    public void createGuarantor() {
        if (listg != null && listg.size() > 0) {
            int amountGuaranteed = getTotalForAllGuarantor();
            int amountToDistribute = (int) (loan.getAmount() - amountGuaranteed);
            ArrayList<Guarantors> withAmountNotSpecified = new ArrayList<>();
            for (Guarantors g : listg) {
                if (g.getGuaranteedAmount() == 0) {
                    withAmountNotSpecified.add(g);
                }
            }
            int forEachMember;
            try {
                forEachMember = (int) Math.ceil(amountToDistribute / withAmountNotSpecified.size());
            } catch (ArithmeticException e) {
                forEachMember = 0;
            }
            for (Guarantors g : listg) {
                AppConstants.logger("amount:" + g.getGuaranteedAmount());
                if (g.getGuarantorId() != null) {
                    if (g.getGuaranteedAmount() == 0) {
                        getFacade().removeGuarantor(g);
                        break;
                    }
                    g.setLoanId(loan);
                    if (g.getId() == null) {
                        AppConstants.logger("stored");
                        getFacade().storeGuarator(g);
                    } else {
                        AppConstants.logger("Merged");
                        getFacade().mergeGuarantor(g);
                    }

                }
            }
        }
        selected = getFacade().findSelected(selected);
        loan = getFacade().findLoan(loan.getLoanId());
        FacesContext.getCurrentInstance().addMessage(
                null,
                new FacesMessage(FacesMessage.SEVERITY_INFO,
                        "Guarantors added", null));
    }

    public void payLoanFromDeposit() {
        if (loan != null) {
            if (depo.getAmount() > 0) {
                if (selected.getTotalDeposit() > depo.getAmount()) {
                    if (selected.getTotalDepositWithrawable() > 0) {
                        if (depo.getAmount() <= selected.getTotalDepositWithrawable()) {
                            Collections c = new Collections();
                            c.setAmount(depo.getAmount());
                            c.setDateCollected(new Date());
                            c.setMemberId(selected);
                            c.setRandomInt(-1);
                            c.setReceiptNo(1111);
                            int toDistribute = depo.getAmount();
                            if (loan.getLoantype().getId() == 1) {
                                c.setCollectionstype(new CollectionTypes(2));
                                int unpaidInterest = selected.getEstimatedUnPaidInterestEmergency();
                                int unpaidNegotiation = selected.getEstimatedUnPaidNegotiationEmergency();
                                if (unpaidInterest > 0) {
                                    if (toDistribute > unpaidInterest) {
                                        SaccoLedgerEntries sle = new SaccoLedgerEntries();
                                        sle.setParticulars("interest on loans");
                                        sle.setCr(unpaidInterest);
                                        sle.setDateWhen(new Date());
                                        sle.setDr(0);
                                        sle.setIsGeneral(false);
                                        sle.setStatusBilled(false);
                                        sle.setLedgerId(new SaccoLedgers(30));
                                        getFacade().saveLedgerEntry(sle);
                                        sle = new SaccoLedgerEntries();
                                        sle.setCr(0);
                                        sle.setIsGeneral(false);
                                        sle.setParticulars("interest on loans");
                                        sle.setDateWhen(new Date());
                                        sle.setStatusBilled(false);
                                        sle.setDr(unpaidInterest);
                                        sle.setLedgerId(new SaccoLedgers(8));
                                        getFacade().saveLedgerEntry(sle);
                                        toDistribute = toDistribute - unpaidInterest;
                                    } else {
                                        SaccoLedgerEntries sle = new SaccoLedgerEntries();
                                        sle.setParticulars("interest on loans");
                                        sle.setCr(toDistribute);
                                        sle.setDateWhen(new Date());
                                        sle.setDr(0);
                                        sle.setIsGeneral(false);
                                        sle.setStatusBilled(false);
                                        sle.setLedgerId(new SaccoLedgers(30));
                                        getFacade().saveLedgerEntry(sle);
                                        sle = new SaccoLedgerEntries();
                                        sle.setCr(0);
                                        sle.setParticulars("interest on loans");
                                        sle.setDateWhen(new Date());
                                        sle.setStatusBilled(false);
                                        sle.setIsGeneral(false);
                                        sle.setDr(toDistribute);
                                        sle.setLedgerId(new SaccoLedgers(8));
                                        getFacade().saveLedgerEntry(sle);
                                        toDistribute = 0;
                                    }

                                }
                                if (unpaidNegotiation > 0 && toDistribute > 0) {
                                    if (toDistribute > unpaidNegotiation) {
                                        SaccoLedgerEntries sle = new SaccoLedgerEntries();
                                        sle.setParticulars("loan negotiation");
                                        sle.setCr(unpaidInterest);
                                        sle.setDateWhen(new Date());
                                        sle.setDr(0);
                                        sle.setStatusBilled(false);
                                        sle.setLedgerId(new SaccoLedgers(41));
                                        getFacade().saveLedgerEntry(sle);
                                        sle = new SaccoLedgerEntries();
                                        sle.setCr(0);
                                        sle.setParticulars("loan negotiation");
                                        sle.setDateWhen(new Date());
                                        sle.setStatusBilled(false);
                                        sle.setDr(unpaidNegotiation);
                                        sle.setLedgerId(new SaccoLedgers(8));
                                        getFacade().saveLedgerEntry(sle);
                                        toDistribute = toDistribute - unpaidNegotiation;
                                    } else {
                                        SaccoLedgerEntries sle = new SaccoLedgerEntries();
                                        sle.setParticulars("loan negotiation");
                                        sle.setCr(toDistribute);
                                        sle.setDateWhen(new Date());
                                        sle.setDr(0);
                                        sle.setStatusBilled(false);
                                        sle.setLedgerId(new SaccoLedgers(41));
                                        getFacade().saveLedgerEntry(sle);
                                        sle = new SaccoLedgerEntries();
                                        sle.setCr(0);
                                        sle.setParticulars("loan negotiation");
                                        sle.setDateWhen(new Date());
                                        sle.setStatusBilled(false);
                                        sle.setDr(toDistribute);
                                        sle.setLedgerId(new SaccoLedgers(8));
                                        getFacade().saveLedgerEntry(sle);
                                        toDistribute = 0;
                                    }
                                }
                            } else if (loan.getLoantype().getId() == 2) {
                                c.setCollectionstype(new CollectionTypes(3));
                                int unpaidInterest = selected.getEstimatedUnPaidInterestNormal();
                                int unpaidNegotiation = selected.getEstimatedUnPaidNegotiationNormal();
                                if (unpaidInterest > 0) {
                                    if (toDistribute > unpaidInterest) {
                                        SaccoLedgerEntries sle = new SaccoLedgerEntries();
                                        sle.setParticulars("interest on loans");
                                        sle.setCr(unpaidInterest);
                                        sle.setDateWhen(new Date());
                                        sle.setDr(0);
                                        sle.setIsGeneral(false);
                                        sle.setStatusBilled(false);
                                        sle.setLedgerId(new SaccoLedgers(30));
                                        getFacade().saveLedgerEntry(sle);
                                        sle = new SaccoLedgerEntries();
                                        sle.setCr(0);
                                        sle.setParticulars("interest on loans");
                                        sle.setDateWhen(new Date());
                                        sle.setStatusBilled(false);
                                        sle.setIsGeneral(false);
                                        sle.setDr(unpaidInterest);
                                        sle.setLedgerId(new SaccoLedgers(8));
                                        getFacade().saveLedgerEntry(sle);
                                        toDistribute = toDistribute - unpaidInterest;
                                    } else {
                                        SaccoLedgerEntries sle = new SaccoLedgerEntries();
                                        sle.setParticulars("interest on loans");
                                        sle.setCr(toDistribute);
                                        sle.setDateWhen(new Date());
                                        sle.setDr(0);
                                        sle.setIsGeneral(false);
                                        sle.setStatusBilled(false);
                                        sle.setLedgerId(new SaccoLedgers(30));
                                        getFacade().saveLedgerEntry(sle);
                                        sle = new SaccoLedgerEntries();
                                        sle.setCr(0);
                                        sle.setParticulars("interest on loans");
                                        sle.setDateWhen(new Date());
                                        sle.setStatusBilled(false);
                                        sle.setDr(toDistribute);
                                        sle.setIsGeneral(false);
                                        sle.setLedgerId(new SaccoLedgers(8));
                                        getFacade().saveLedgerEntry(sle);
                                        toDistribute = 0;
                                    }

                                }
                                if (unpaidNegotiation > 0 && toDistribute > 0) {
                                    if (toDistribute > unpaidNegotiation) {
                                        SaccoLedgerEntries sle = new SaccoLedgerEntries();
                                        sle.setParticulars("loan negotiation");
                                        sle.setCr(unpaidInterest);
                                        sle.setDateWhen(new Date());
                                        sle.setDr(0);
                                        sle.setIsGeneral(false);
                                        sle.setStatusBilled(false);
                                        sle.setLedgerId(new SaccoLedgers(41));
                                        getFacade().saveLedgerEntry(sle);
                                        sle = new SaccoLedgerEntries();
                                        sle.setCr(0);
                                        sle.setIsGeneral(false);
                                        sle.setParticulars("loan negotiation");
                                        sle.setDateWhen(new Date());
                                        sle.setStatusBilled(false);
                                        sle.setDr(unpaidNegotiation);
                                        sle.setLedgerId(new SaccoLedgers(8));
                                        getFacade().saveLedgerEntry(sle);
                                        toDistribute = toDistribute - unpaidNegotiation;
                                    } else {
                                        SaccoLedgerEntries sle = new SaccoLedgerEntries();
                                        sle.setParticulars("interest on loans");
                                        sle.setCr(toDistribute);
                                        sle.setDateWhen(new Date());
                                        sle.setDr(0);
                                        sle.setIsGeneral(false);
                                        sle.setStatusBilled(false);
                                        sle.setLedgerId(new SaccoLedgers(41));
                                        getFacade().saveLedgerEntry(sle);
                                        sle = new SaccoLedgerEntries();
                                        sle.setCr(0);
                                        sle.setIsGeneral(false);
                                        sle.setParticulars("interest on loans");
                                        sle.setDateWhen(new Date());
                                        sle.setStatusBilled(false);
                                        sle.setDr(toDistribute);
                                        sle.setLedgerId(new SaccoLedgers(8));
                                        getFacade().saveLedgerEntry(sle);
                                        toDistribute = 0;
                                    }
                                }
                            }
                            //+++++
                            if (toDistribute > 0) {
                                SaccoLedgerEntries sle = new SaccoLedgerEntries();
                                sle.setCr(0);
                                sle.setIsGeneral(false);
                                sle.setDateWhen(new Date());
                                sle.setDr(toDistribute);
                                sle.setLedgerId(new SaccoLedgers(8));
                                sle.setParticulars("loan collections");
                                getFacade().saveLedgerEntry(sle);
                                sle = new SaccoLedgerEntries();
                                sle.setParticulars("loan collections");
                                sle.setCr(toDistribute);
                                sle.setDateWhen(new Date());
                                sle.setDr(0);
                                sle.setIsGeneral(false);
                                sle.setLedgerId(new SaccoLedgers(22));
                                getFacade().saveLedgerEntry(sle);
                            }
                            getFacade().storeCollections(c);
                            int toremain = depo.getAmount() - selected.getTotalDepositWithrawable();
                            depo.setAmount(selected.getTotalDepositWithrawable() * -1);
                            depo.setMemberNo(selected);
                            depo.setTransactedBy("");
                            depo.setDate(new Date());
                            depo.setDescription("paid loan");
                            depo.setIsWithdrawable(true);
                            getFacade().storeDeposit(depo);
                            depo = new DepositCash();
                            depo.setAmount(toremain * -1);
                            depo.setMemberNo(selected);
                            depo.setTransactedBy("");
                            depo.setDate(new Date());
                            depo.setDescription("paid loan");
                            depo.setIsWithdrawable(false);
                            getFacade().storeDeposit(depo);
                            depo = null;
                            loan = null;
                            selected = getFacade().findSelected(selected);
                            selected = getFacade().findSelected(selected);
                            FacesContext.getCurrentInstance().addMessage(
                                    null,
                                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                                            "loan paid", null));
                        } else {
                            Collections c = new Collections();
                            c.setAmount(depo.getAmount());
                            c.setDateCollected(new Date());
                            c.setMemberId(selected);
                            c.setRandomInt(-1);
                            c.setReceiptNo(1111);
                            int toDistribute = depo.getAmount();
                            if (loan.getLoantype().getId() == 1) {
                                c.setCollectionstype(new CollectionTypes(2));
                                int unpaidInterest = selected.getEstimatedUnPaidInterestEmergency();
                                int unpaidNegotiation = selected.getEstimatedUnPaidNegotiationEmergency();
                                if (unpaidInterest > 0) {
                                    if (toDistribute > unpaidInterest) {
                                        SaccoLedgerEntries sle = new SaccoLedgerEntries();
                                        sle.setParticulars("interest on loans");
                                        sle.setCr(unpaidInterest);
                                        sle.setDateWhen(new Date());
                                        sle.setDr(0);
                                        sle.setIsGeneral(false);
                                        sle.setStatusBilled(false);
                                        sle.setLedgerId(new SaccoLedgers(30));
                                        getFacade().saveLedgerEntry(sle);
                                        sle = new SaccoLedgerEntries();
                                        sle.setCr(0);
                                        sle.setIsGeneral(false);
                                        sle.setParticulars("interest on loans");
                                        sle.setDateWhen(new Date());
                                        sle.setStatusBilled(false);
                                        sle.setDr(unpaidInterest);
                                        sle.setLedgerId(new SaccoLedgers(8));
                                        getFacade().saveLedgerEntry(sle);
                                        toDistribute = toDistribute - unpaidInterest;
                                    } else {
                                        SaccoLedgerEntries sle = new SaccoLedgerEntries();
                                        sle.setParticulars("interest on loans");
                                        sle.setCr(toDistribute);
                                        sle.setDateWhen(new Date());
                                        sle.setDr(0);
                                        sle.setIsGeneral(false);
                                        sle.setStatusBilled(false);
                                        sle.setLedgerId(new SaccoLedgers(30));
                                        getFacade().saveLedgerEntry(sle);
                                        sle = new SaccoLedgerEntries();
                                        sle.setCr(0);
                                        sle.setParticulars("interest on loans");
                                        sle.setDateWhen(new Date());
                                        sle.setStatusBilled(false);
                                        sle.setIsGeneral(false);
                                        sle.setDr(toDistribute);
                                        sle.setLedgerId(new SaccoLedgers(8));
                                        getFacade().saveLedgerEntry(sle);
                                        toDistribute = 0;
                                    }

                                }
                                if (unpaidNegotiation > 0 && toDistribute > 0) {
                                    if (toDistribute > unpaidNegotiation) {
                                        SaccoLedgerEntries sle = new SaccoLedgerEntries();
                                        sle.setParticulars("loan negotiation");
                                        sle.setCr(unpaidInterest);
                                        sle.setDateWhen(new Date());
                                        sle.setDr(0);
                                        sle.setStatusBilled(false);
                                        sle.setLedgerId(new SaccoLedgers(41));
                                        getFacade().saveLedgerEntry(sle);
                                        sle = new SaccoLedgerEntries();
                                        sle.setCr(0);
                                        sle.setParticulars("loan negotiation");
                                        sle.setDateWhen(new Date());
                                        sle.setStatusBilled(false);
                                        sle.setDr(unpaidNegotiation);
                                        sle.setLedgerId(new SaccoLedgers(8));
                                        getFacade().saveLedgerEntry(sle);
                                        toDistribute = toDistribute - unpaidNegotiation;
                                    } else {
                                        SaccoLedgerEntries sle = new SaccoLedgerEntries();
                                        sle.setParticulars("loan negotiation");
                                        sle.setCr(toDistribute);
                                        sle.setDateWhen(new Date());
                                        sle.setDr(0);
                                        sle.setStatusBilled(false);
                                        sle.setLedgerId(new SaccoLedgers(41));
                                        getFacade().saveLedgerEntry(sle);
                                        sle = new SaccoLedgerEntries();
                                        sle.setCr(0);
                                        sle.setParticulars("loan negotiation");
                                        sle.setDateWhen(new Date());
                                        sle.setStatusBilled(false);
                                        sle.setDr(toDistribute);
                                        sle.setLedgerId(new SaccoLedgers(8));
                                        getFacade().saveLedgerEntry(sle);
                                        toDistribute = 0;
                                    }
                                }
                            } else if (loan.getLoantype().getId() == 2) {
                                c.setCollectionstype(new CollectionTypes(3));
                                int unpaidInterest = selected.getEstimatedUnPaidInterestNormal();
                                int unpaidNegotiation = selected.getEstimatedUnPaidNegotiationNormal();
                                if (unpaidInterest > 0) {
                                    if (toDistribute > unpaidInterest) {
                                        SaccoLedgerEntries sle = new SaccoLedgerEntries();
                                        sle.setParticulars("interest on loans");
                                        sle.setCr(unpaidInterest);
                                        sle.setDateWhen(new Date());
                                        sle.setDr(0);
                                        sle.setIsGeneral(false);
                                        sle.setStatusBilled(false);
                                        sle.setLedgerId(new SaccoLedgers(30));
                                        getFacade().saveLedgerEntry(sle);
                                        sle = new SaccoLedgerEntries();
                                        sle.setCr(0);
                                        sle.setParticulars("interest on loans");
                                        sle.setDateWhen(new Date());
                                        sle.setStatusBilled(false);
                                        sle.setIsGeneral(false);
                                        sle.setDr(unpaidInterest);
                                        sle.setLedgerId(new SaccoLedgers(8));
                                        getFacade().saveLedgerEntry(sle);
                                        toDistribute = toDistribute - unpaidInterest;
                                    } else {
                                        SaccoLedgerEntries sle = new SaccoLedgerEntries();
                                        sle.setParticulars("interest on loans");
                                        sle.setCr(toDistribute);
                                        sle.setDateWhen(new Date());
                                        sle.setDr(0);
                                        sle.setIsGeneral(false);
                                        sle.setStatusBilled(false);
                                        sle.setLedgerId(new SaccoLedgers(30));
                                        getFacade().saveLedgerEntry(sle);
                                        sle = new SaccoLedgerEntries();
                                        sle.setCr(0);
                                        sle.setParticulars("interest on loans");
                                        sle.setDateWhen(new Date());
                                        sle.setStatusBilled(false);
                                        sle.setDr(toDistribute);
                                        sle.setIsGeneral(false);
                                        sle.setLedgerId(new SaccoLedgers(8));
                                        getFacade().saveLedgerEntry(sle);
                                        toDistribute = 0;
                                    }

                                }
                                if (unpaidNegotiation > 0 && toDistribute > 0) {
                                    if (toDistribute > unpaidNegotiation) {
                                        SaccoLedgerEntries sle = new SaccoLedgerEntries();
                                        sle.setParticulars("loan negotiation");
                                        sle.setCr(unpaidInterest);
                                        sle.setDateWhen(new Date());
                                        sle.setDr(0);
                                        sle.setIsGeneral(false);
                                        sle.setStatusBilled(false);
                                        sle.setLedgerId(new SaccoLedgers(41));
                                        getFacade().saveLedgerEntry(sle);
                                        sle = new SaccoLedgerEntries();
                                        sle.setCr(0);
                                        sle.setIsGeneral(false);
                                        sle.setParticulars("loan negotiation");
                                        sle.setDateWhen(new Date());
                                        sle.setStatusBilled(false);
                                        sle.setDr(unpaidNegotiation);
                                        sle.setLedgerId(new SaccoLedgers(8));
                                        getFacade().saveLedgerEntry(sle);
                                        toDistribute = toDistribute - unpaidNegotiation;
                                    } else {
                                        SaccoLedgerEntries sle = new SaccoLedgerEntries();
                                        sle.setParticulars("interest on loans");
                                        sle.setCr(toDistribute);
                                        sle.setDateWhen(new Date());
                                        sle.setDr(0);
                                        sle.setIsGeneral(false);
                                        sle.setStatusBilled(false);
                                        sle.setLedgerId(new SaccoLedgers(41));
                                        getFacade().saveLedgerEntry(sle);
                                        sle = new SaccoLedgerEntries();
                                        sle.setCr(0);
                                        sle.setIsGeneral(false);
                                        sle.setParticulars("interest on loans");
                                        sle.setDateWhen(new Date());
                                        sle.setStatusBilled(false);
                                        sle.setDr(toDistribute);
                                        sle.setLedgerId(new SaccoLedgers(8));
                                        getFacade().saveLedgerEntry(sle);
                                        toDistribute = 0;
                                    }
                                }
                            }
                            //+++++
                            if (toDistribute > 0) {
                                SaccoLedgerEntries sle = new SaccoLedgerEntries();
                                sle.setCr(0);
                                sle.setIsGeneral(false);
                                sle.setDateWhen(new Date());
                                sle.setDr(toDistribute);
                                sle.setLedgerId(new SaccoLedgers(8));
                                sle.setParticulars("loan collections");
                                getFacade().saveLedgerEntry(sle);
                                sle = new SaccoLedgerEntries();
                                sle.setParticulars("loan collections");
                                sle.setCr(toDistribute);
                                sle.setDateWhen(new Date());
                                sle.setDr(0);
                                sle.setIsGeneral(false);
                                sle.setLedgerId(new SaccoLedgers(22));
                                getFacade().saveLedgerEntry(sle);
                            }
                            getFacade().storeCollections(c);
                            depo.setAmount(depo.getAmount() * -1);
                            depo.setMemberNo(selected);
                            depo.setTransactedBy("");
                            depo.setDate(new Date());
                            depo.setDescription("paid loan");
                            depo.setIsWithdrawable(true);
                            getFacade().storeDeposit(depo);
                            depo = null;
                            loan = null;
                            selected = getFacade().findSelected(selected);
                            selected = getFacade().findSelected(selected);
                            FacesContext.getCurrentInstance().addMessage(
                                    null,
                                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                                            "loan paid", null));
                        }
                    } else if (selected.getTotalDepositNonWithrawable() >= depo.getAmount()) {
                        Collections c = new Collections();
                        c.setAmount(depo.getAmount());
                        c.setDateCollected(new Date());
                        c.setMemberId(selected);
                        c.setRandomInt(-1);
                        c.setReceiptNo(1111);
                        int toDistribute = depo.getAmount();
                        if (loan.getLoantype().getId() == 1) {
                            c.setCollectionstype(new CollectionTypes(2));
                            int unpaidInterest = selected.getEstimatedUnPaidInterestEmergency();
                            int unpaidNegotiation = selected.getEstimatedUnPaidNegotiationEmergency();
                            if (unpaidInterest > 0) {
                                if (toDistribute > unpaidInterest) {
                                    SaccoLedgerEntries sle = new SaccoLedgerEntries();
                                    sle.setParticulars("interest on loans");
                                    sle.setCr(unpaidInterest);
                                    sle.setDateWhen(new Date());
                                    sle.setDr(0);
                                    sle.setIsGeneral(false);
                                    sle.setStatusBilled(false);
                                    sle.setLedgerId(new SaccoLedgers(30));
                                    getFacade().saveLedgerEntry(sle);
                                    sle = new SaccoLedgerEntries();
                                    sle.setCr(0);
                                    sle.setIsGeneral(false);
                                    sle.setParticulars("interest on loans");
                                    sle.setDateWhen(new Date());
                                    sle.setStatusBilled(false);
                                    sle.setDr(unpaidInterest);
                                    sle.setLedgerId(new SaccoLedgers(8));
                                    getFacade().saveLedgerEntry(sle);
                                    toDistribute = toDistribute - unpaidInterest;
                                } else {
                                    SaccoLedgerEntries sle = new SaccoLedgerEntries();
                                    sle.setParticulars("interest on loans");
                                    sle.setCr(toDistribute);
                                    sle.setDateWhen(new Date());
                                    sle.setDr(0);
                                    sle.setIsGeneral(false);
                                    sle.setStatusBilled(false);
                                    sle.setLedgerId(new SaccoLedgers(30));
                                    getFacade().saveLedgerEntry(sle);
                                    sle = new SaccoLedgerEntries();
                                    sle.setCr(0);
                                    sle.setParticulars("interest on loans");
                                    sle.setDateWhen(new Date());
                                    sle.setStatusBilled(false);
                                    sle.setIsGeneral(false);
                                    sle.setDr(toDistribute);
                                    sle.setLedgerId(new SaccoLedgers(8));
                                    getFacade().saveLedgerEntry(sle);
                                    toDistribute = 0;
                                }

                            }
                            if (unpaidNegotiation > 0 && toDistribute > 0) {
                                if (toDistribute > unpaidNegotiation) {
                                    SaccoLedgerEntries sle = new SaccoLedgerEntries();
                                    sle.setParticulars("loan negotiation");
                                    sle.setCr(unpaidInterest);
                                    sle.setDateWhen(new Date());
                                    sle.setDr(0);
                                    sle.setStatusBilled(false);
                                    sle.setLedgerId(new SaccoLedgers(41));
                                    getFacade().saveLedgerEntry(sle);
                                    sle = new SaccoLedgerEntries();
                                    sle.setCr(0);
                                    sle.setParticulars("loan negotiation");
                                    sle.setDateWhen(new Date());
                                    sle.setStatusBilled(false);
                                    sle.setDr(unpaidNegotiation);
                                    sle.setLedgerId(new SaccoLedgers(8));
                                    getFacade().saveLedgerEntry(sle);
                                    toDistribute = toDistribute - unpaidNegotiation;
                                } else {
                                    SaccoLedgerEntries sle = new SaccoLedgerEntries();
                                    sle.setParticulars("loan negotiation");
                                    sle.setCr(toDistribute);
                                    sle.setDateWhen(new Date());
                                    sle.setDr(0);
                                    sle.setStatusBilled(false);
                                    sle.setLedgerId(new SaccoLedgers(41));
                                    getFacade().saveLedgerEntry(sle);
                                    sle = new SaccoLedgerEntries();
                                    sle.setCr(0);
                                    sle.setParticulars("loan negotiation");
                                    sle.setDateWhen(new Date());
                                    sle.setStatusBilled(false);
                                    sle.setDr(toDistribute);
                                    sle.setLedgerId(new SaccoLedgers(8));
                                    getFacade().saveLedgerEntry(sle);
                                    toDistribute = 0;
                                }
                            }
                        } else if (loan.getLoantype().getId() == 2) {
                            c.setCollectionstype(new CollectionTypes(3));
                            int unpaidInterest = selected.getEstimatedUnPaidInterestNormal();
                            int unpaidNegotiation = selected.getEstimatedUnPaidNegotiationNormal();
                            if (unpaidInterest > 0) {
                                if (toDistribute > unpaidInterest) {
                                    SaccoLedgerEntries sle = new SaccoLedgerEntries();
                                    sle.setParticulars("interest on loans");
                                    sle.setCr(unpaidInterest);
                                    sle.setDateWhen(new Date());
                                    sle.setDr(0);
                                    sle.setIsGeneral(false);
                                    sle.setStatusBilled(false);
                                    sle.setLedgerId(new SaccoLedgers(30));
                                    getFacade().saveLedgerEntry(sle);
                                    sle = new SaccoLedgerEntries();
                                    sle.setCr(0);
                                    sle.setParticulars("interest on loans");
                                    sle.setDateWhen(new Date());
                                    sle.setStatusBilled(false);
                                    sle.setIsGeneral(false);
                                    sle.setDr(unpaidInterest);
                                    sle.setLedgerId(new SaccoLedgers(8));
                                    getFacade().saveLedgerEntry(sle);
                                    toDistribute = toDistribute - unpaidInterest;
                                } else {
                                    SaccoLedgerEntries sle = new SaccoLedgerEntries();
                                    sle.setParticulars("interest on loans");
                                    sle.setCr(toDistribute);
                                    sle.setDateWhen(new Date());
                                    sle.setDr(0);
                                    sle.setIsGeneral(false);
                                    sle.setStatusBilled(false);
                                    sle.setLedgerId(new SaccoLedgers(30));
                                    getFacade().saveLedgerEntry(sle);
                                    sle = new SaccoLedgerEntries();
                                    sle.setCr(0);
                                    sle.setParticulars("interest on loans");
                                    sle.setDateWhen(new Date());
                                    sle.setStatusBilled(false);
                                    sle.setDr(toDistribute);
                                    sle.setIsGeneral(false);
                                    sle.setLedgerId(new SaccoLedgers(8));
                                    getFacade().saveLedgerEntry(sle);
                                    toDistribute = 0;
                                }

                            }
                            if (unpaidNegotiation > 0 && toDistribute > 0) {
                                if (toDistribute > unpaidNegotiation) {
                                    SaccoLedgerEntries sle = new SaccoLedgerEntries();
                                    sle.setParticulars("loan negotiation");
                                    sle.setCr(unpaidInterest);
                                    sle.setDateWhen(new Date());
                                    sle.setDr(0);
                                    sle.setIsGeneral(false);
                                    sle.setStatusBilled(false);
                                    sle.setLedgerId(new SaccoLedgers(41));
                                    getFacade().saveLedgerEntry(sle);
                                    sle = new SaccoLedgerEntries();
                                    sle.setCr(0);
                                    sle.setIsGeneral(false);
                                    sle.setParticulars("loan negotiation");
                                    sle.setDateWhen(new Date());
                                    sle.setStatusBilled(false);
                                    sle.setDr(unpaidNegotiation);
                                    sle.setLedgerId(new SaccoLedgers(8));
                                    getFacade().saveLedgerEntry(sle);
                                    toDistribute = toDistribute - unpaidNegotiation;
                                } else {
                                    SaccoLedgerEntries sle = new SaccoLedgerEntries();
                                    sle.setParticulars("interest on loans");
                                    sle.setCr(toDistribute);
                                    sle.setDateWhen(new Date());
                                    sle.setDr(0);
                                    sle.setIsGeneral(false);
                                    sle.setStatusBilled(false);
                                    sle.setLedgerId(new SaccoLedgers(41));
                                    getFacade().saveLedgerEntry(sle);
                                    sle = new SaccoLedgerEntries();
                                    sle.setCr(0);
                                    sle.setIsGeneral(false);
                                    sle.setParticulars("interest on loans");
                                    sle.setDateWhen(new Date());
                                    sle.setStatusBilled(false);
                                    sle.setDr(toDistribute);
                                    sle.setLedgerId(new SaccoLedgers(8));
                                    getFacade().saveLedgerEntry(sle);
                                    toDistribute = 0;
                                }
                            }
                        }
                        //+++++
                        if (toDistribute > 0) {
                            SaccoLedgerEntries sle = new SaccoLedgerEntries();
                            sle.setCr(0);
                            sle.setIsGeneral(false);
                            sle.setDateWhen(new Date());
                            sle.setDr(toDistribute);
                            sle.setLedgerId(new SaccoLedgers(8));
                            sle.setParticulars("loan collections");
                            getFacade().saveLedgerEntry(sle);
                            sle = new SaccoLedgerEntries();
                            sle.setParticulars("loan collections");
                            sle.setCr(toDistribute);
                            sle.setDateWhen(new Date());
                            sle.setDr(0);
                            sle.setIsGeneral(false);
                            sle.setLedgerId(new SaccoLedgers(22));
                            getFacade().saveLedgerEntry(sle);
                        }
                        getFacade().storeCollections(c);
                        depo.setAmount(depo.getAmount() * -1);
                        depo.setMemberNo(selected);
                        depo.setTransactedBy("");
                        depo.setDate(new Date());
                        depo.setDescription("paid loan");
                        depo.setIsWithdrawable(false);
                        getFacade().storeDeposit(depo);
                        depo = null;
                        loan = null;
                        selected = getFacade().findSelected(selected);
                        selected = getFacade().findSelected(selected);
                        FacesContext.getCurrentInstance().addMessage(
                                null,
                                new FacesMessage(FacesMessage.SEVERITY_INFO,
                                        "loan paid", null));
                    }
                } else {
                    FacesContext.getCurrentInstance().addMessage(
                            null,
                            new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                    "Loan not paid. Limited account balance", null));
                }
            }
        }
    }

    public void deductFromGuarantors() {
        int noOfGuarantor = loan.getGuarantorsCollection().size();
        AppConstants.logger("guarantors" + noOfGuarantor);
        int guaranteedAmount = loan.getTotalGuaranteedAmount();
    }

    public void onRowEdit(RowEditEvent event) {
        Guarantors newg = (Guarantors) event.getObject();
        listg.get(newg.getId()).setGuaranteedAmount(newg.getGuaranteedAmount());
        listg.get(newg.getId()).setGuarantorId(newg.getGuarantorId());
    }

    public void onRowEditStatement(RowEditEvent event) {
        PaymentSummaryItems newg = (PaymentSummaryItems) event.getObject();
        getFacade().updateSummary(newg);
    }

    public void onRowCancel(RowEditEvent event) {
        AppConstants.logger("yeiuyuieyiru");
        FacesContext.getCurrentInstance().addMessage(
                null,
                new FacesMessage(FacesMessage.SEVERITY_FATAL,
                        "No record have been saved", null));
    }

    public void setNoOfGuarantors(int noOfGuarantors) {
        this.noOfGuarantors = noOfGuarantors;
    }

    public LoanScheduleTransactions getItemTransaction() {
        return itemtransaction;
    }

    public DepositCash getMoneyTransfer() {
        return moneyTransfer;
    }

    public void setMoneyTransfer(DepositCash moneyTransfer) {
        this.moneyTransfer = moneyTransfer;
    }

    public void saveDeposit() {
        if (moneyTransfer.getAmount() <= selected.getTotalDepositWithrawable()) {
            getFacade().storeDeposit(moneyTransfer);
            moneyTransfer.setAmount(moneyTransfer.getAmount() * -1);
            moneyTransfer.setDate(new Date());
            moneyTransfer.setDescription("Tranfered");
            moneyTransfer.setIsWithdrawable(true);
            moneyTransfer.setMemberNo(selected);
            moneyTransfer.setTransactedBy("jack");
            getFacade().storeDeposit(moneyTransfer);
            selected = getFacade().findSelected(selected);
            FacesContext.getCurrentInstance().addMessage(
                    null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Transfer successiful", null));
            return;
        }
        FacesContext.getCurrentInstance().addMessage(
                null,
                new FacesMessage(FacesMessage.SEVERITY_FATAL,
                        "Transfer failed, Insufficient funds", null));
    }
    private DepositCash moneyTransfer;

    public void prepareCreatedeposit() {
        moneyTransfer = new DepositCash();
        moneyTransfer.setAmount(getTotalWithDrawableIn() + getTotalWithDrawableOut());
        moneyTransfer.setDate(new Date());
        moneyTransfer.setDescription("Tranfered");
        moneyTransfer.setIsWithdrawable(false);
        moneyTransfer.setMemberNo(selected);
        moneyTransfer.setTransactedBy("jack");
    }

    public List<Collections> getCollectionsBetweenDates() {
        return getFacade().getCollectionsBetweenDates(selected, sdate, edate, type);
    }

    public List<Collections> getCollectionsBetweenDatesAll() {
        return getFacade().getCollectionsBetweenDates(selected, sdate, edate);
    }

    public int getTotalCollectionsBetweenDates() {
        int total = 0;
        for (Collections c : getCollectionsBetweenDates()) {
            total += c.getAmount();
        }
        return total;
    }

    public int getTotalCollectionsBetweenDatesAll() {
        int total = 0;
        for (Collections c : getCollectionsBetweenDatesAll()) {
            total += c.getAmount();
        }
        return total;
    }

    public void setItemTransaction(LoanScheduleTransactions itemtransaction) {
        this.itemtransaction = itemtransaction;
    }

    public Loan getLoan() {
        return loan;
    }

    public Date getSdate() {
        return sdate;
    }

    public void setSdate(Date sdate) {
        this.sdate = sdate;
    }

    public MemberBills prepareBill() {
        bill = new MemberBills();
        return bill;
    }

    public void billMember() {
        int rand = new Random().nextInt(99999999);
        bill = new MemberBills();
        bill.setMemberId(selected);
        bill.setAmount(billAmount);
        bill.setNarration(billNarration);
        bill.setDateIncured(whenBilled);
        bill.setRandomInt(rand);
        getFacade().persistBill(bill);
        bill = getFacade().getMemberBillByRandomInt(rand);
        bill.setRandomInt(-1);
        getFacade().mergeMemberBill(bill);
        BillTransactions bt = new BillTransactions();
        bt.setBillId(bill);
        bt.setAmountDebit(-bill.getAmount());
        bt.setDatePaid(new Date());
        bt.setReceiptNo("");
        bt.setNaration("billed");
        bt.setAmountCredit(0);
        getFacade().storeBillTransaction(bt);

        BillTransactions bt2 = new BillTransactions();
        bt2.setBillId(bill);
        bt2.setNaration(bill.getNarration());
        bt2.setAmountDebit(0);
        bt2.setDatePaid(new Date());
        bt2.setReceiptNo("");
        bt2.setAmountCredit(bill.getAmount());
        getFacade().storeBillTransaction(bt2);

        DepositCash depo = new DepositCash();
        depo.setIsWithdrawable(false);
        depo.setDescription("paid bills " + bill.getNarration());
        depo.setAmount(-bill.getAmount());
        depo.setDate(new Date());
        depo.setMemberNo(selected);
        depo.setTransactedBy("derer");
        getFacade().storeDeposit(depo);
        memberSelection.clear();
        JsfUtil.addSuccessMessage("Members accounts billed.");
    }

    public void billMembers() {
        for (Members m : memberSelection) {
            int rand = new Random().nextInt(99999999);
            bill = new MemberBills();
            bill.setMemberId(m);
            bill.setAmount(billAmount);
            bill.setNarration(billNarration);
            bill.setDateIncured(whenBilled);
            bill.setRandomInt(rand);
            getFacade().persistBill(bill);
            bill = getFacade().getMemberBillByRandomInt(rand);
            bill.setRandomInt(-1);
            getFacade().mergeMemberBill(bill);
            BillTransactions bt = new BillTransactions();
            bt.setBillId(bill);
            bt.setAmountDebit(-bill.getAmount());
            bt.setDatePaid(new Date());
            bt.setReceiptNo("");
            bt.setNaration("billed");
            bt.setAmountCredit(0);
            getFacade().storeBillTransaction(bt);

            BillTransactions bt2 = new BillTransactions();
            bt2.setBillId(bill);
            bt2.setNaration(bill.getNarration());
            bt2.setAmountDebit(0);
            bt2.setDatePaid(new Date());
            bt2.setReceiptNo("");
            bt2.setAmountCredit(bill.getAmount());
            getFacade().storeBillTransaction(bt2);

            DepositCash depo = new DepositCash();
            depo.setIsWithdrawable(false);
            depo.setDescription("paid bills " + bill.getNarration());
            depo.setAmount(-bill.getAmount());
            depo.setDate(new Date());
            depo.setMemberNo(m);
            depo.setTransactedBy("derer");
            getFacade().storeDeposit(depo);
        }
        memberSelection.clear();
        JsfUtil.addSuccessMessage("Members accounts billed.");
    }

    public Date getEdate() {
        return edate;
    }

    public void setEdate(Date edate) {
        this.edate = edate;
    }

    public void setLoan(Loan loan) {
        this.loan = loan;
    }

    private PaymentSummaryItems item;

    public void closeAllAcoounts() {
        /*
        this function runs in the background.
        algorithm.
        start
        create a payment record.
        create a empty list of PaymentSummaryItems. for each member, a PaymentSummaryItems will be created and added into the list
        loop through all members
        find if the mamber has paid any shares of registration fee ln 540-551
        find if the member has withdrawn for the sacco
        pay loan by collection of the loan type*/

        Runnable command = new Runnable() {
            @Override
            public void run() {
                p = new PaymentSummary();
                Date reportDate = new Date();
                reportDate.setMonth(reportDate.getMonth() - 1);
                reportDate.setDate(30);
                p.setDatePrepared(reportDate);
                Collection<PaymentSummaryItems> payitems = new ArrayList<>();
                message = "Started updating accounts";
                System.out.println("started updating accounts on " + new Date());
                int x = 0;
                for (Members m : getFacade().findAll()) {
                    x++;
                    if (!m.getMembershipStatus().equalsIgnoreCase("closed")) {
                        item = new PaymentSummaryItems();
                        List<BillTransactions> listbt = getFacade().getBillTransactionsBetweenDates(m);
                        int total = 0;
                        int totalreg = 0;
                        if (listbt != null) {
                            for (BillTransactions bt : listbt) {
                                if (bt.getAmountCredit() > 0 && bt.getNaration().equalsIgnoreCase("registration")) {
                                    totalreg += bt.getAmountCredit();
                                } else if (bt.getAmountCredit() > 0 && bt.getNaration().equalsIgnoreCase("shares")) {
                                    total += bt.getAmountCredit();
                                }
                            }
                        }
                        List<Withdrawal> listwith = getFacade().getWithDrawalBetweenDates(m);
                        int totalwithdrawal = 0;
                        int totalwithdrawalincome = 0;
                        if (listwith != null) {
                            for (Withdrawal with : listwith) {
                                if (with.getWithdrawalcharges() > 0) {
                                    totalwithdrawalincome += with.getWithdrawalcharges();
                                }
                                if (with.getAmount() > 0) {
                                    totalwithdrawal += with.getAmount();
                                }
                            }

                            System.out.println("Withdrawals:" + totalwithdrawal);
                            System.out.println("Withdrawals income:" + totalwithdrawalincome);
                        }
                        System.out.println("Synching account for " + m.getSurname());
                        int emergencyLoan = 0, normalLoan = 0;
                        System.out.println("finding total for collections");
                        for (Collections c : getFacade().getMemberUnbilledCollections(m)) {
                            if (c.getCollectionstype().getId() == 2) {
                                emergencyLoan += c.getAmount();
                            } else if (c.getCollectionstype().getId() == 3) {
                                normalLoan += c.getAmount();
                            }
                        }
                        System.out.println("finding total for collections finished with the following results");
                        System.out.println("Total for emergency:" + emergencyLoan);
                        System.out.println("Total for Normal:" + normalLoan);
                        System.out.println("finding if the member has an emergency loan");
                        Loan loan = getFacade().findMemberActiveEmergencyLoan(m);
                        if (loan.getMemeberId() != null) {
                            if (!loan.getStopBilling()) {
                                System.out.println("found that the member has an emergency loan");
                                System.out.println("called pay loan");
                                if (m.hasPendingEmergency()) {
                                    Loan l = getFacade().getPendingLoan(1, m);
                                    item.setLoanedemergency((int) ((int) Math.ceil(loan.getCurrentSchedule().getBalance() + l.getAmount())));
                                    item.setInterestChargedEmergency((int) ((int) Math.ceil(loan.getCurrentSchedule().getInterest() + l.getCurrentSchedule().getInterest())));
                                    int amountToPayCurrentLoan = (int) (loan.getCurrentSchedule().getBalance() + loan.getCurrentSchedule().getTotalDebits());
                                    System.out.println("Amount to paycurrent:" + amountToPayCurrentLoan);
                                    payLoan((int) (loan.getCurrentSchedule().getBalance() + loan.getCurrentSchedule().getTotalDebits()), 2, loan);
                                    int amountPaid = emergencyLoan - amountToPayCurrentLoan;
                                    System.out.println("Amount to paypeding:" + amountPaid);
                                    payLoan(amountPaid, 2, l);
                                } else {
                                    item.setLoanedemergency((int) loan.getCurrentSchedule().getBalance());
                                    item.setInterestChargedEmergency((int) loan.getCurrentSchedule().getInterest());
                                    payLoan(emergencyLoan, 2, loan);
                                }
                            } else {
                                item.setLoanedemergency((int) loan.getCurrentSchedule().getBalance());
                                item.setInterestChargedEmergency(0);
                                item.setEmergencyLoanInterestPaid(0);
                                item.setLoanBalanceEmergency(item.getLoanedemergency());
                            }
                        }
                        System.out.println("finding if the member has a normal loan");
                        loan = getFacade().findMemberActiveNormalLoan(m);
                        if (loan.getMemeberId() != null) {
                            if (!loan.getStopBilling()) {
                                if (m.hasPendingNormal()) {
                                    Loan l = getFacade().getPendingLoan(2, m);
                                    item.setLoaned((int) ((int) Math.ceil(loan.getCurrentSchedule().getBalance() + l.getAmount())));
                                    item.setInterestCharged((int) ((int) Math.ceil(loan.getCurrentSchedule().getInterest() + l.getCurrentSchedule().getInterest())));
                                    int amountToPayCurrentLoan = (int) (loan.getCurrentSchedule().getBalance() + loan.getCurrentSchedule().getTotalDebits());
                                    System.out.println("Amount to paycurrent:" + amountToPayCurrentLoan);
                                    payLoan((int) (loan.getCurrentSchedule().getBalance() + loan.getCurrentSchedule().getTotalDebits()), 3, loan);
                                    int amountPaid = normalLoan - amountToPayCurrentLoan;
                                    System.out.println("Amount to paypeding:" + amountPaid);
                                    payLoan(amountPaid, 3, l);
                                } else {
                                    item.setLoaned((int) loan.getCurrentSchedule().getBalance());
                                    item.setInterestCharged((int) loan.getCurrentSchedule().getInterest());
                                    payLoan(normalLoan, 3, loan);
                                }
                            } else {
                                item.setLoaned((int) loan.getCurrentSchedule().getBalance());
                                item.setInterestCharged(0);
                                item.setNormalLoanInterestPaid(0);
                                item.setLoanBalance(item.getLoaned());
                            }
                        }
                        item.setMemberName(m);
                        item.setPaidShares(total);
                        item.setSharesBalance(m.getSharesPaid());
                        item.setRegistration(totalreg);
                        item.setWithdrawnDepositsAmount(totalwithdrawal);
                        item.setDepositsTotal(m.getTotalDeposit());
                        item.setWithdrawal(totalwithdrawal);
                        item.setWithdrawalincome(totalwithdrawalincome);
                        for (DepositCash c : getFacade().getDepositsBetweenDates(m)) {
                            if (c.getAmount() > 0 || c.getDescription().equals("paid loan")) {
                                item.setDepositedAmount(item.getDepositedAmount() + c.getAmount());
                                m.setDeffaultCount(0);
                                getFacade().edit(m);
                            } else {
                                m.setDeffaultCount(m.getDeffaultCount() + 1);
                                getFacade().edit(m);
                            }
                        }
                        item.setPaymentSummaryId(p);
                        payitems.add(item);
                        if (m.getMembershipStatus().equalsIgnoreCase("Awaiting Close")) {
                            for (AccountTransitions acc : m.getAccountTransitionsCollection()) {
                                acc.setIsCurrent(false);
                                getFacade().editAccTransition(acc);
                            }
                            AccountTransitions acc = new AccountTransitions();
                            acc.setAccount(m);
                            acc.setComments("exited");
                            acc.setDateWhen(new Date());
                            acc.setIsCurrent(true);
                            acc.setStatus(new AccountStatus(3));
                            getFacade().StoreAccTransition(acc);
                        }
                    }
                    message = "Closed account for " + x + " members out of " + getFacade().findAll().size() + " members.";
                }
                System.out.println("finished closing accounts");
                p.setPaymentSummaryItemsCollection(payitems);
                getFacade().storePaymenySummary(p);
                getFacade().setAllBillTransactionsToBilled();
                getFacade().setAllDepositsBilled();
                getFacade().setAllWithdrawalsBilled();
                SaccoLedgerEntries sle;
                for (SaccoLedgers sl : getFacade().getAllSaccoLedger()) {
                    if (sl.getId() == 22) {
                        int totalUnBilled = sl.getTotalUnbilledDr();
                        System.out.println("totalUnBilled Loan issued" + totalUnBilled);
                        sle = new SaccoLedgerEntries();
                        sle.setParticulars("Total Loans Issued");
                        sle.setDr(totalUnBilled);
                        sle.setCr(0);
                        sle.setDateWhen(new Date());
                        sle.setLedgerId(sl);
                        sle.setStatusBilled(false);
                        sle.setIsGeneral(true);
                        getFacade().saveLedgerEntry(sle);
                        totalUnBilled = sl.getTotalUnbilled();
                        System.out.println("totalUnBilled Loan" + totalUnBilled);
                        sle = new SaccoLedgerEntries();
                        sle.setParticulars("Total Loans Paid");
                        sle.setCr(totalUnBilled);
                        sle.setDr(0);
                        sle.setDateWhen(new Date());
                        sle.setLedgerId(sl);
                        sle.setStatusBilled(false);
                        sle.setIsGeneral(true);
                        getFacade().saveLedgerEntry(sle);
                        getFacade().setAllLedgerEntriesToBilled(sl.getId());
                    }
                    if (sl.getId() == 18) {
                        int totalUnBilled = sl.getTotalUnbilled();
                        System.out.println("Total Entrance Fee Paid" + totalUnBilled);
                        sle = new SaccoLedgerEntries();
                        sle.setParticulars("Total Entrance Fee Paid");
                        sle.setCr(totalUnBilled);
                        sle.setDr(0);
                        sle.setDateWhen(new Date());
                        sle.setLedgerId(sl);
                        sle.setStatusBilled(false);
                        sle.setIsGeneral(true);
                        getFacade().saveLedgerEntry(sle);
                        getFacade().setAllLedgerEntriesToBilled(sl.getId());
                    }
                    if (sl.getId() == 8) {
                        int totalUnBilled = sl.getTotalUnbilled();
                        System.out.println("Total Deposited" + totalUnBilled);
                        sle = new SaccoLedgerEntries();
                        sle.setParticulars("Total Deposited");
                        sle.setCr(totalUnBilled);
                        sle.setDr(0);
                        sle.setDateWhen(new Date());
                        sle.setLedgerId(sl);
                        sle.setStatusBilled(false);
                        sle.setIsGeneral(true);
                        getFacade().saveLedgerEntry(sle);
                        getFacade().setAllLedgerEntriesToBilled(sl.getId());
                    }
                    if (sl.getId() == 1) {
                        int totalUnBilled = sl.getTotalUnbilled();
                        System.out.println("Total Shares Paid" + totalUnBilled);
                        sle = new SaccoLedgerEntries();
                        sle.setParticulars("Total Shares Paid");
                        sle.setCr(totalUnBilled);
                        sle.setDr(0);
                        sle.setDateWhen(new Date());
                        sle.setLedgerId(sl);
                        sle.setStatusBilled(false);
                        sle.setIsGeneral(true);
                        getFacade().saveLedgerEntry(sle);
                        getFacade().setAllLedgerEntriesToBilled(sl.getId());
                    }
                    if (sl.getId() == 6) {
                        int totalUnBilled = sl.getTotalUnbilledDr();
                        System.out.println("Total Cash Transacted" + totalUnBilled);
                        sle = new SaccoLedgerEntries();
                        sle.setParticulars("Total Cash Transacted");
                        sle.setCr(0);
                        sle.setDr(totalUnBilled);
                        sle.setDateWhen(new Date());
                        sle.setLedgerId(sl);
                        sle.setStatusBilled(false);
                        sle.setIsGeneral(true);
                        getFacade().saveLedgerEntry(sle);
                        getFacade().setAllLedgerEntriesToBilled(sl.getId());
                    }
                    if (sl.getId() == 30) {
                        int totalUnBilled = sl.getTotalUnbilled();
                        System.out.println("totalUnBilled interest" + totalUnBilled);
                        sle = new SaccoLedgerEntries();
                        sle.setParticulars("Total Interest Paid");
                        sle.setCr(totalUnBilled);
                        sle.setDr(0);
                        sle.setDateWhen(new Date());
                        sle.setLedgerId(sl);
                        sle.setStatusBilled(false);
                        sle.setIsGeneral(true);
                        getFacade().saveLedgerEntry(sle);
                        getFacade().setAllLedgerEntriesToBilled(sl.getId());
                    }
                    if (sl.getId() == 41) {
                        int totalUnBilled = sl.getTotalUnbilled();
                        System.out.println("Total Negotiation Paid" + totalUnBilled);
                        sle = new SaccoLedgerEntries();
                        sle.setParticulars("Total Negotiation Paid");
                        sle.setCr(totalUnBilled);
                        sle.setDr(0);
                        sle.setDateWhen(new Date());
                        sle.setLedgerId(sl);
                        sle.setStatusBilled(false);
                        sle.setIsGeneral(true);
                        getFacade().saveLedgerEntry(sle);
                        getFacade().setAllLedgerEntriesToBilled(sl.getId());
                    }
                    //System.out.println("bcd"+sl.getName()+sl.getCr()+""+sl.getDr());
                    if (sl.getIsCredit()) {
                        if (sl.getCr() > 0) {
                            sle = new SaccoLedgerEntries();
                            sle.setParticulars("b.c.d");
                            sle.setCr(0);
                            sle.setIsGeneral(true);
                            sle.setDr(sl.getCr());
                            sle.setDateWhen(new Date());
                            sle.setLedgerId(sl);
                            sle.setStatusBilled(false);
                            getFacade().saveLedgerEntry(sle);
                            sle = new SaccoLedgerEntries();
                            sle.setCr(sl.getCr());
                            sle.setDr(0);
                            sle.setParticulars("b.b.f");
                            sle.setDateWhen(new Date());
                            sle.setLedgerId(sl);
                            sle.setIsGeneral(true);
                            getFacade().saveLedgerEntry(sle);

                        } else {
                            sle = new SaccoLedgerEntries();
                            sle.setParticulars("b.c.d");
                            sle.setDr(0);
                            sle.setIsGeneral(true);
                            sle.setCr(sl.getCr());
                            sle.setDateWhen(new Date());
                            sle.setLedgerId(sl);
                            sle.setStatusBilled(false);
                            getFacade().saveLedgerEntry(sle);
                            sle = new SaccoLedgerEntries();
                            sle.setDr(sl.getCr());
                            sle.setCr(0);
                            sle.setIsGeneral(true);
                            sle.setParticulars("b.b.f");
                            sle.setDateWhen(new Date());
                            sle.setLedgerId(sl);
                            sle.setStatusBilled(false);
                            getFacade().saveLedgerEntry(sle);
                        }
                    } else if (sl.getDr() > 0) {
                        sle = new SaccoLedgerEntries();
                        sle.setParticulars("b.c.d");
                        sle.setDr(0);
                        sle.setIsGeneral(true);
                        sle.setCr(sl.getDr());
                        sle.setDateWhen(new Date());
                        sle.setLedgerId(sl);
                        sle.setStatusBilled(false);
                        getFacade().saveLedgerEntry(sle);
                        sle = new SaccoLedgerEntries();
                        sle.setDr(sl.getDr());
                        sle.setCr(0);
                        sle.setIsGeneral(true);
                        sle.setParticulars("b.b.f");
                        sle.setDateWhen(new Date());
                        sle.setLedgerId(sl);
                        sle.setStatusBilled(false);
                        getFacade().saveLedgerEntry(sle);

                    } else {
                        sle = new SaccoLedgerEntries();
                        sle.setParticulars("b.c.d");
                        sle.setDr(sl.getDr());
                        sle.setCr(0);
                        sle.setIsGeneral(true);
                        sle.setDateWhen(new Date());
                        sle.setLedgerId(sl);
                        sle.setStatusBilled(false);
                        getFacade().saveLedgerEntry(sle);
                        sle = new SaccoLedgerEntries();
                        sle.setCr(sl.getDr());
                        sle.setDr(0);
                        sle.setIsGeneral(true);
                        sle.setParticulars("b.b.f");
                        sle.setDateWhen(new Date());
                        sle.setLedgerId(sl);
                        sle.setStatusBilled(false);
                        getFacade().saveLedgerEntry(sle);
                    }
                }
                getFacade().setAllLoanNonPending();
                message = "Completed successifully";
            }
        };
        executor.execute(command);
    }

    public void onSelect() {
        loan = null;
    }

    public void payLoan(int amount, int type, Loan loan) {
        int loanId = loan.getLoanId();
        LoanScheduleTransactions itemtransactions = new LoanScheduleTransactions();
        itemtransactions.setScheduleId(loan.getCurrentSchedule());
        itemtransactions.setDetails("Collected");
        itemtransactions.setDateCollected(new Date());
        itemtransactions.setAmountCredit((double) amount);
        System.out.println("saving transaction" + itemtransactions.getAmountCredit());
        loan.getCurrentSchedule().getLoanScheduleTransactionsCollection().add(itemtransactions);
        getFacade().mergeLoanSchedule(loan.getCurrentSchedule());
        //loan = getFacade().findLoan(loanId);
        System.out.println("Listing transactions:");
        for (LoanScheduleTransactions lst : loan.getCurrentSchedule().getLoanScheduleTransactionsCollection()) {
            System.out.println(lst.toString());
        }
        System.out.println("loan amount:" + loan.getAmount() + "Loand Id:" + loan.getLoanId() + "loan last schedule bal:" + loan.getCurrentSchedule().getBalance() + "Interest Paid" + loan.getCurrentSchedule().getPaidInterest());
        if (type == 2) {
            System.out.println("Interest Paid emergency" + loan.getCurrentSchedule().getPaidInterest());
            System.out.println("Loan paid" + loan.getCurrentSchedule().getPayment());
            try {
                item.setEmergencyLoanInterestPaid(item.getEmergencyLoanInterestPaid() + loan.getCurrentSchedule().getPaidInterest());
                item.setEmergencyLoanPaid(item.getEmergencyLoanPaid() + loan.getCurrentSchedule().getPayment());
            } catch (NullPointerException e) {
            }
        } else {
            System.out.println("Interest Paid normal" + loan.getCurrentSchedule().getPaidInterest());
            System.out.println("Loan paid" + loan.getCurrentSchedule().getPayment());
            try {
                item.setNormalLoanInterestPaid(item.getNormalLoanInterestPaid() + loan.getCurrentSchedule().getPaidInterest());
                item.setNormalLoanPaid(item.getNormalLoanPaid() + loan.getCurrentSchedule().getPayment());
            } catch (NullPointerException e) {
            }
        }

        System.out.println("found loan");
        if (loan.getCurrentSchedule().getBalance() > 0) {
            System.out.println("loan balance>0: " + loan.getCurrentSchedule().getBalance() + "Net account" + loan.getCurrentSchedule().getNetAccount());
            System.out.println("calling close schedule");
            closeSchedule(type, loan);
            loan = getFacade().findLoan(loanId);
            loan.setRandomInt(-1);
            getFacade().storeLoan(loan);
        } else if (loan.getCurrentSchedule().getBalance() <= 0) {
            System.out.println("loan balance<0: " + loan.getCurrentSchedule().getBalance());
            loan.setLoanStatus(true);
            LoanSchedules ls = loan.getCurrentSchedule();
            ls.setBalance(0);
            getFacade().mergeLoanSchedule(ls);
            getFacade().storeLoan(loan);
        }
        Members m = getFacade().find(loan.getMemeberId().getId());
        getFacade().mergeCollection(m, type);
    }

    public void closeSchedule(int l, Loan loan) {
        LoanSchedules old = loan.getCurrentSchedule();
        LoanSchedules loanSchedule = new LoanSchedules();
        float remainingBalanceForTheLoan = (int) old.getBalance();
        System.out.println("remaining balance" + remainingBalanceForTheLoan);
        System.out.println("old.getNetAccount()" + old.getNetAccount());
        if (old.getNetAccount() >= 0) {
            loan.setDeffaultCount(0);
            loanSchedule.setBalance(remainingBalanceForTheLoan - old.getNetAccount());
            System.out.println("loanSchedule principle amount > 0" + loanSchedule.getBalance());
        }
        if (old.getUnpaidInterest() > 0) {
            loan.setDeffaultCount(loan.getDeffaultCount() + 1);
            loanSchedule.setBalance(remainingBalanceForTheLoan + old.getUnpaidInterest());
            System.out.println("loanSchedule principle amount < 0" + loanSchedule.getBalance());
            if (loan.getLoantype().getId() == 2) {
                item.setInterestBalance(old.getUnpaidInterest());
            } else {
                item.setInterestBalanceEmergency(old.getUnpaidInterest());
            }
        } else {
            loanSchedule.setBalance(remainingBalanceForTheLoan - old.getNetAccount());
        }
        loan.setPedding(false);
        if (loanSchedule.getBalance() > 0) {
            float interest;
            old.setPrincipleAmount(loanSchedule.getBalance());
            if (old.getLoanId().getLoantype().getId() == 1) {
                interest = (float) (0.01 * loanSchedule.getBalance() * emegencyinterestrate / 12);
            } else {
                interest = (float) (0.01 * loanSchedule.getBalance() * InterestRate / 12);
            }
            interest = (float) Math.ceil(interest);
            loanSchedule.setInterest(interest);
            int rand = new Random().nextInt(10000);
            loanSchedule.setRandomInt(rand);
            loanSchedule.setLoanId(loan);
            loanSchedule.setStatusCleared(getFacade().findScheduleStatus(2));
            DateTime today = new DateTime();
            DateTime nextAfterAMonth = today.plusMonths(1);
            Date objectDate = nextAfterAMonth.toDate();
            loanSchedule.setDueDate(objectDate);
            old.setStatusCleared(getFacade().findScheduleStatus(4));
            getFacade().mergeLoanSchedule(old);
            getFacade().storeLoanSchedule(loanSchedule);
            loanSchedule = getFacade().getLoanSchedule(rand);
            LoanScheduleTransactions lst = new LoanScheduleTransactions();
            lst.setAmountDebit(interest);
            lst.setAmountCredit(0.0);
            lst.setDateCollected(new Date());
            lst.setDetails("interest");
            lst.setScheduleId(loanSchedule);
            getFacade().storeTransaction(lst);
            loanSchedule.setRandomInt(-1);
            getFacade().mergeLoanSchedule(loanSchedule);
            if (loanSchedule.getLoanId().getLoantype().getId() == 2) {
                item.setLoanBalance((int) loanSchedule.getBalance());
            } else {
                item.setLoanBalanceEmergency((int) (loanSchedule.getBalance()));
            }
        } else if (loanSchedule.getBalance() == 0) {
            System.out.println("want to update loan++++++++++++++" + loan.getLoanId() + "Loan Status" + loan.getLoanStatus());
            loan.setLoanStatus(true);
            getFacade().mergeLoan(loan);
            old.setPrincipleAmount(loanSchedule.getBalance());
            loan = getFacade().findLoan(loan.getLoanId());
            System.out.println("loan status:" + loan.getLoanStatus());
        }
        if (old.getIsForLastMonth()) {
            System.out.println("Old loan status" + old.getLoanId().getLoanStatus());
            System.out.println("negotiation~~~~~~~~~~~~" + old.getNegotiationPaid());
            if (old.getNegotiationPaid() == old.getNegotiation()) {
                System.out.println("All Negotiation fee is paid");
                item.setNegotiation(item.getNegotiation() + old.getNegotiation());
            } else if (old.getNegotiationPaid() < old.getNegotiation()) {
                System.out.println("Negotiation partilly paid" + old.getNegotiationPaid());
                loanSchedule.setIsForLastMonth(true);
                getFacade().mergeLoanSchedule(loanSchedule);
                LoanScheduleTransactions lst = new LoanScheduleTransactions();
                lst.setAmountDebit(old.getNegotiation() - old.getNegotiationPaid());
                lst.setAmountCredit(0.0);
                lst.setDateCollected(new Date());
                lst.setDetails("loannegotiation");
                lst.setScheduleId(loanSchedule);
                getFacade().storeTransaction(lst);
                item.setNegotiation(old.getNegotiation());
            }
            old.setIsForLastMonth(false);
            getFacade().mergeLoanSchedule(old);
        }
    }

    public void createDepo(int amount, String desc, Members id) {
        depo = new DepositCash();
        depo.setAmount(amount);
        depo.setDescription(desc);
        depo.setMemberNo(id);
        depo.setTransactedBy("");
        depo.setDate(new Date());
        getFacade().storeDeposit(depo);
    }

    public int getTotalForAllGuarantor() {
        int total = 0;
        if (getGuarators() != null) {
            for (Guarantors g : getGuarators()) {
                total += g.getGuaranteedAmount();
            }
        }
        return total;
    }

    public void updateMember() {
        selected = getFacade().findSelected(selected);
        try {
            loan = getFacade().findLoan(loan.getLoanId());
        } catch (NullPointerException e) {

        }
    }

    public void performWithDrawal() {
        DateTime sDate = new DateTime();
        SaccoLedgerEntries sle = new SaccoLedgerEntries();
        Withdrawal with = new Withdrawal();
        with.setAmount((selected.getLoanDifferenceWithDeposits() - withdrawalcharges - (int) selected.getTotalRecoverableFromLoan()));
        with.setComments("Member left");
        with.setDateWhen(new Date());
        with.setMemberId(selected);
        with.setWithdrawalcharges(withdrawalcharges);
        getFacade().storeWithdrawal(with);
        createDepo(-withdrawalcharges, "withdrawal charges", selected);
        sle.setCr(0);
        sle.setParticulars("withdrawal from member");
        sle.setDateWhen(new Date());
        sle.setDr(selected.getLoanDifferenceWithDeposits() - withdrawalcharges - (int) selected.getTotalRecoverableFromLoan());
        sle.setLedgerId(new SaccoLedgers(43));
        getFacade().saveLedgerEntry(sle);
        sle = new SaccoLedgerEntries();
        sle.setCr(0);
        sle.setParticulars("withdrawal from member");
        sle.setDateWhen(new Date());
        sle.setDr(selected.getLoanDifferenceWithDeposits() - withdrawalcharges - (int) selected.getTotalRecoverableFromLoan());
        sle.setLedgerId(new SaccoLedgers(8));
        getFacade().saveLedgerEntry(sle);
        createDepo(-(selected.getLoanDifferenceWithDeposits() - withdrawalcharges - (int) selected.getTotalRecoverableFromLoan()), "withdrawal", selected);

//        with = new Withdrawal();
//        with.setAmount(selected.getLoanDifferenceWithDeposits() - withdrawalcharges - (int) selected.getTotalRecoverableFromLoan());
//        with.setComments("paidloan");
//        with.setDateWhen(new Date());
//        with.setMemberId(selected);
//        with.setWithdrawalcharges(0);
//        getFacade().storeWithdrawal(with);
//moving money from deposits to withdrawal income
        sle.setDr(0);
        sle.setParticulars("withdrawal charges");
        sle.setDateWhen(new Date());
        sle.setCr(withdrawalcharges);
        sle.setLedgerId(new SaccoLedgers(19));
        getFacade().saveLedgerEntry(sle);
        sle = new SaccoLedgerEntries();
        sle.setCr(0);
        sle.setParticulars("withdrawal charges");
        sle.setDateWhen(new Date());
        sle.setDr(withdrawalcharges);
        sle.setLedgerId(new SaccoLedgers(8));
        getFacade().saveLedgerEntry(sle);
        if (selected.getActiveLoanEmergency() != null && !selected.getActiveLoanEmergency().getLoanStatus()) {
            int toReduce = 0;
            if (selected.getActiveLoanEmergency().getMemeberId().getEstimatedUnPaidInterestEmergency() > 0) {
                toReduce += selected.getActiveLoanEmergency().getMemeberId().getEstimatedUnPaidInterestEmergency();
                sle.setCr(selected.getActiveLoanEmergency().getMemeberId().getEstimatedUnPaidInterestEmergency());
                sle.setDateWhen(new Date());
                sle.setDr(0);
                sle.setParticulars("interest on loan");
                sle.setLedgerId(new SaccoLedgers(30));
                getFacade().saveLedgerEntry(sle);
                sle = new SaccoLedgerEntries();
                sle.setCr(0);
                sle.setParticulars("interest on loan");
                sle.setDateWhen(new Date());
                sle.setDr(selected.getActiveLoanEmergency().getMemeberId().getEstimatedUnPaidInterestEmergency());
                sle.setLedgerId(new SaccoLedgers(8));
                getFacade().saveLedgerEntry(sle);
            }
            if (selected.getActiveLoanEmergency().getMemeberId().getEstimatedUnPaidNegotiationEmergency() > 0) {
                toReduce += selected.getActiveLoanEmergency().getMemeberId().getEstimatedUnPaidNegotiationEmergency();
                sle.setCr(selected.getActiveLoanEmergency().getMemeberId().getEstimatedUnPaidNegotiationEmergency());
                sle.setDateWhen(new Date());
                sle.setDr(0);
                sle.setParticulars("loan negotiation");
                sle.setLedgerId(new SaccoLedgers(41));
                getFacade().saveLedgerEntry(sle);
                sle = new SaccoLedgerEntries();
                sle.setCr(0);
                sle.setParticulars("loan negotiation");
                sle.setDateWhen(new Date());
                sle.setDr(selected.getActiveLoanEmergency().getMemeberId().getEstimatedUnPaidNegotiationEmergency());
                sle.setLedgerId(new SaccoLedgers(8));
                getFacade().saveLedgerEntry(sle);
            }
            Collections c = new Collections(selected.getActiveLoanEmergency().getBalEst());
            c.setCollectionstype(new CollectionTypes(2));
            c.setDateCollected(new Date());
            c.setMemberId(selected);
            c.setReceiptNo(1111);
            c.setStatusBilled(false);
            c.setAmount(selected.getActiveLoanEmergency().getBalEst());
            getFacade().storeCollections(c);
            c.setAmount(toReduce);
            sle.setCr(selected.getActiveLoanEmergency().getBalEst() - toReduce);
            sle.setDateWhen(new Date());
            sle.setDr(0);
            sle.setParticulars("loan collections");
            sle.setLedgerId(new SaccoLedgers(22));
            getFacade().saveLedgerEntry(sle);
            sle = new SaccoLedgerEntries();
            sle.setCr(0);
            sle.setParticulars("loan collections");
            sle.setDateWhen(new Date());
            sle.setDr(selected.getActiveLoanEmergency().getBalEst() - toReduce);
            sle.setLedgerId(new SaccoLedgers(8));
            getFacade().saveLedgerEntry(sle);
            createDepo(-selected.getActiveLoanEmergency().getBalEst(), "paidloan", selected);
            with = new Withdrawal();
            with.setAmount(selected.getActiveLoanEmergency().getBalEst());
            with.setComments("paidloan");
            with.setDateWhen(new Date());
            with.setMemberId(selected);
            with.setWithdrawalcharges(0);
            getFacade().storeWithdrawal(with);
        }
        if (selected.getActiveLoanNormal() != null && !selected.getActiveLoanNormal().getLoanStatus()) {
            int toReduce = 0;
            if (selected.getActiveLoanNormal().getMemeberId().getEstimatedUnPaidInterestNormal() > 0) {
                toReduce += selected.getActiveLoanNormal().getMemeberId().getEstimatedUnPaidInterestNormal();
                sle.setCr(selected.getActiveLoanNormal().getMemeberId().getEstimatedUnPaidInterestNormal());
                sle.setDateWhen(new Date());
                sle.setDr(0);
                sle.setParticulars("interest on loan");
                sle.setLedgerId(new SaccoLedgers(30));
                getFacade().saveLedgerEntry(sle);
                sle = new SaccoLedgerEntries();
                sle.setCr(0);
                sle.setParticulars("interest on loan");
                sle.setDateWhen(new Date());
                sle.setDr(selected.getActiveLoanNormal().getMemeberId().getEstimatedUnPaidInterestNormal());
                sle.setLedgerId(new SaccoLedgers(8));
                getFacade().saveLedgerEntry(sle);
            }
            if (selected.getActiveLoanNormal().getMemeberId().getEstimatedUnPaidNegotiationNormal() > 0) {
                toReduce += selected.getActiveLoanNormal().getMemeberId().getEstimatedUnPaidNegotiationNormal();
                sle.setCr(selected.getActiveLoanNormal().getMemeberId().getEstimatedUnPaidNegotiationNormal());
                sle.setDateWhen(new Date());
                sle.setDr(0);
                sle.setParticulars("loan negotiation");
                sle.setLedgerId(new SaccoLedgers(41));
                getFacade().saveLedgerEntry(sle);
                sle = new SaccoLedgerEntries();
                sle.setCr(0);
                sle.setParticulars("loan negotiation");
                sle.setDateWhen(new Date());
                sle.setDr(selected.getActiveLoanNormal().getMemeberId().getEstimatedUnPaidNegotiationNormal());
                sle.setLedgerId(new SaccoLedgers(8));
                getFacade().saveLedgerEntry(sle);
            }
            Collections c = new Collections(selected.getActiveLoanNormal().getBalEst());
            c.setCollectionstype(new CollectionTypes(3));
            c.setDateCollected(new Date());
            c.setMemberId(selected);
            c.setReceiptNo(1111);
            c.setStatusBilled(false);
            c.setAmount(selected.getActiveLoanNormal().getBalEst());
            getFacade().storeCollections(c);
            c.setAmount(toReduce);
            sle.setCr(selected.getActiveLoanNormal().getBalEst() - toReduce);
            sle.setDateWhen(new Date());
            sle.setDr(0);
            sle.setParticulars("loan collections");
            sle.setLedgerId(new SaccoLedgers(22));
            getFacade().saveLedgerEntry(sle);
            sle = new SaccoLedgerEntries();
            sle.setCr(0);
            sle.setParticulars("loan collections");
            sle.setDateWhen(new Date());
            sle.setDr(selected.getActiveLoanNormal().getBalEst() - toReduce);
            sle.setLedgerId(new SaccoLedgers(8));
            getFacade().saveLedgerEntry(sle);
            createDepo(-selected.getActiveLoanNormal().getBalEst(), "paidloan", selected);
            with = new Withdrawal();
            with.setAmount(selected.getActiveLoanNormal().getBalEst());
            with.setComments("paidloan");
            with.setDateWhen(new Date());
            with.setMemberId(selected);
            with.setWithdrawalcharges(0);
            getFacade().storeWithdrawal(with);
        }
        for (AccountTransitions acc : selected.getAccountTransitionsCollection()) {
            acc.setIsCurrent(false);
            getFacade().editAccTransition(acc);
        }
        AccountTransitions acc = new AccountTransitions();
        acc.setAccount(selected);
        acc.setComments("exited sacco");
        acc.setDateWhen(new Date());
        acc.setIsCurrent(true);
        acc.setStatus(new AccountStatus(4));
        getFacade().StoreAccTransition(acc);
        FacesContext.getCurrentInstance().addMessage(
                null,
                new FacesMessage(FacesMessage.SEVERITY_INFO,
                        "Member has been removed from the sacco", null));
        for (Collections c : getFacade().getMemberUnbilledCollections(selected)) {
            //c.setStatusBilled(true);
            getFacade().mergeCollection(c);
        }
        selected = getFacade().findSelected(selected);
    }

    public int getWithdrawalcharges() {
        return withdrawalcharges;
    }

    public void setWithdrawalcharges(int withdrawalcharges) {
        this.withdrawalcharges = withdrawalcharges;
    }

    public String getConfirmationMessage() {
        return "\nLast updated " + lastUpdated.toString();
    }

    public void createDeposit(int amount, Members id, int collectiontype) {
        AppConstants.logger("creating deposit for type" + collectiontype);
        SaccoLedgerEntries sle;
        AppConstants.logger("finding bills with arrears");
        if (id.getBillsTotalArrears() < 0) {
            AppConstants.logger("found bills with arrears");
            for (MemberBills mb : id.getMemberBillsCollection()) {
                if (amount > 0) {
                    if (mb.getNetAcc() < 0) {
                        if (amount == mb.getNetAcc() * -1 || amount < mb.getNetAcc() * -1) {
                            BillTransactions bt = new BillTransactions();
                            bt.setAmountCredit(amount);
                            bt.setDatePaid(new Date());
                            bt.setBillId(mb);
                            bt.setNaration(bt.getNaration());
                            bt.setReceiptNo("");
                            getFacade().storeBillTransaction(bt);
                            switch (bt.getNaration()) {
                                case "shares": {
                                    AppConstants.logger("setting cr for shares" + amount);
                                    sle = new SaccoLedgerEntries();
                                    sle.setCr(amount);
                                    sle.setDateWhen(new Date());
                                    sle.setDr(0);
                                    sle.setLedgerId(new SaccoLedgers(1));
                                    getFacade().saveLedgerEntry(sle);
                                    sle = new SaccoLedgerEntries();
                                    sle.setCr(0);
                                    sle.setDateWhen(new Date());
                                    sle.setDr(amount);
                                    sle.setLedgerId(new SaccoLedgers(22));
                                    getFacade().saveLedgerEntry(sle);
                                    break;
                                }
                                case "registration": {
                                    AppConstants.logger("setting cr for registration" + amount);
                                    sle = new SaccoLedgerEntries();
                                    sle.setCr(0);
                                    sle.setDateWhen(new Date());
                                    sle.setDr(amount);
                                    sle.setLedgerId(new SaccoLedgers(12));
                                    getFacade().saveLedgerEntry(sle);
                                    sle = new SaccoLedgerEntries();
                                    sle.setCr(0);
                                    sle.setDateWhen(new Date());
                                    sle.setDr(amount);
                                    sle.setLedgerId(new SaccoLedgers(22));
                                    getFacade().saveLedgerEntry(sle);
                                    break;
                                }
                            }
                            amount = 0;
                            break;
                        } else {
                            amount = amount - mb.getNetAcc() * -1;
                            BillTransactions bt = new BillTransactions();
                            bt.setAmountCredit(mb.getNetAcc() * -1);
                            bt.setDatePaid(new Date());
                            bt.setBillId(mb);
                            bt.setNaration(bt.getNaration());
                            bt.setReceiptNo("");
                            getFacade().storeBillTransaction(bt);
                            switch (bt.getNaration()) {
                                case "shares": {
                                    AppConstants.logger("setting cr for shares" + mb.getNetAcc() * -1);
                                    sle = new SaccoLedgerEntries();
                                    sle.setCr(mb.getNetAcc() * -1);
                                    sle.setDateWhen(new Date());
                                    sle.setDr(0);
                                    sle.setLedgerId(new SaccoLedgers(1));
                                    getFacade().saveLedgerEntry(sle);
                                    sle = new SaccoLedgerEntries();
                                    sle.setCr(0);
                                    sle.setDateWhen(new Date());
                                    sle.setDr(mb.getNetAcc() * -1);
                                    sle.setLedgerId(new SaccoLedgers(22));
                                    getFacade().saveLedgerEntry(sle);
                                    break;
                                }
                                case "registration": {
                                    AppConstants.logger("setting cr for registration" + mb.getNetAcc() * -1);
                                    sle = new SaccoLedgerEntries();
                                    sle.setCr(0);
                                    sle.setDateWhen(new Date());
                                    sle.setDr(mb.getNetAcc() * -1);
                                    sle.setLedgerId(new SaccoLedgers(12));
                                    getFacade().saveLedgerEntry(sle);
                                    sle = new SaccoLedgerEntries();
                                    sle.setCr(0);
                                    sle.setDateWhen(new Date());
                                    sle.setDr(mb.getNetAcc() * -1);
                                    sle.setLedgerId(new SaccoLedgers(22));
                                    getFacade().saveLedgerEntry(sle);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }

        AppConstants.logger("after paying bills arrears, amount remaining is" + amount);
        if (amount > 0) {
            AppConstants.logger("Depositing cash");
            DepositCash deposit = new DepositCash();
            deposit.setAmount(amount);
            deposit.setAmount(amount);
            deposit.setMemberNo(id);
            getFacade().storeDeposit(deposit);
            AppConstants.logger("Depositing cash done");
            AppConstants.logger("Updating legders");
            sle = new SaccoLedgerEntries();
            sle.setCr(amount);
            sle.setDateWhen(new Date());
            sle.setDr(0);
            sle.setLedgerId(new SaccoLedgers(8));
            getFacade().saveLedgerEntry(sle);
            sle = new SaccoLedgerEntries();
            sle.setCr(0);
            sle.setDateWhen(new Date());
            sle.setDr(amount);
            sle.setLedgerId(new SaccoLedgers(22));
            getFacade().saveLedgerEntry(sle);
            AppConstants.logger("Updating legders done");
        }
        AppConstants.logger("Updating collections to billed");
        for (Collections c : getFacade().getMemberUnbilledCollections(id)) {
            if (c.getCollectionstype().getId() == collectiontype) {
                c.setStatusBilled(true);
                getFacade().mergeCollection(c);
            }
        }
    }

    public Loan prepareCreateLoan() {
        loan = new Loan();
        loan.setMemeberId(selected);
        loan.setLoanStatus(false);
        listg = new ArrayList<>();
        return loan;
    }

    public MembersController() {
    }

    public Members getSelected() {
        return selected;
    }

    public void setSelected(Members selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    public void updateAllAccountStatus() {
        for (Members m : getItems()) {
            AccountTransitions accTran = new AccountTransitions();
            accTran.setAccount(m);
            accTran.setComments("account activated");
            accTran.setDateWhen(new Date());
            accTran.setStatus(new AccountStatus(1));
            accTran.setIsCurrent(true);
            getFacade().StoreAccTransition(accTran);
        }
    }

    protected void initializeEmbeddableKey() {
    }

    public DepositCash getDepo() {
        return depo;
    }

    public void setDepo(DepositCash depo) {
        this.depo = depo;
    }

    private MembersFacade getFacade() {
        return ejbFacade;
    }

    public Members prepareCreate() {
        selected = new Members();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        int randomInt = new Random().nextInt(100000);
        selected.setRandomInt(randomInt);
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("MembersCreated"));
        if (!JsfUtil.isValidationFailed()) {
            selected = getFacade().getByRandomInt(randomInt);
            if (registrationfee > 0) {
                int randomInt2 = new Random().nextInt(100000);
                MemberBills mb = new MemberBills();
                mb.setRandomInt(randomInt2);
                mb.setAmount(registrationfee);
                mb.setDateIncured(new Date());
                mb.setMemberId(selected);
                mb.setNarration("registration");
                getFacade().persistBill(mb);
                mb = getFacade().getMemberBillByRandomInt(randomInt2);
                BillTransactions bt = new BillTransactions();
                bt.setBillId(mb);
                bt.setAmountDebit(-registrationfee);
                bt.setDatePaid(new Date());
                bt.setNaration("registration");
                bt.setReceiptNo("");
                bt.setAmountCredit(0);
                getFacade().storeBillTransaction(bt);
                mb.setRandomInt(-1);
                getFacade().mergeMemberBill(mb);
            }
            if (entryfee > 0) {
                int randomInt2 = new Random().nextInt(100000);
                MemberBills mb = new MemberBills();
                mb.setRandomInt(randomInt2);
                mb.setAmount(entryfee);
                mb.setDateIncured(new Date());
                mb.setMemberId(selected);
                mb.setNarration("shares");
                getFacade().persistBill(mb);
                mb = getFacade().getMemberBillByRandomInt(randomInt2);
                BillTransactions bt = new BillTransactions();
                bt.setBillId(mb);
                bt.setAmountDebit(-entryfee);
                bt.setDatePaid(new Date());
                bt.setNaration("shares");
                bt.setReceiptNo("");
                bt.setAmountCredit(0);
                getFacade().storeBillTransaction(bt);
                mb.setRandomInt(-1);
                getFacade().mergeMemberBill(mb);
            }
            AccountTransitions accTran = new AccountTransitions();
            accTran.setAccount(selected);
            accTran.setComments("account activated");
            accTran.setDateWhen(new Date());
            accTran.setStatus(new AccountStatus(1));
            accTran.setIsCurrent(true);
            getFacade().StoreAccTransition(accTran);
            selected.setRandomInt(-1);
            getFacade().edit(selected);
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("MembersUpdated"));
    }

    public DepositCash prepareDeposit() {
        depo = new DepositCash();
        depo.setMemberNo(selected);
        return depo;
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("MembersDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<LoanTypes> getLoanTypes() {
        return getFacade().getLoanTypes();
    }

    public List<Members> getItems() {
        items = getFacade().getAllMembers();
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public Members getMembers(java.lang.Integer id) {
        return getFacade().find(id);
    }

    public List<Members> getItemsAvailableSelectMany() {
        return getFacade().getAllMembers();
    }

    public List<Members> getItemsAvailableSelectOne() {
        return getFacade().getAllMembers();
    }

    @FacesConverter(forClass = Members.class)
    public static class MembersControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            MembersController controller = (MembersController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "membersController");
            return controller.getMembers(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Members) {
                Members o = (Members) object;
                return getStringKey(o.getId());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Members.class.getName()});
                return null;
            }
        }
    }

    public String getTitle() {
        return "Sacco Members Payments Summary For The Month Of " + new SimpleDateFormat("MMMM-yyyy").format(new DateTime().minusMonths(1).toDate()).replace("-", " Year ");
    }

    public String getTitleMember() {
        try {
            return "Acc Statement for " + selected.getSurname() + " As at the Month Of " + new SimpleDateFormat("MMMM-yyyy").format(new DateTime().minusMonths(1).toDate()).replace("-", " Year ");
        } catch (NullPointerException e) {
        }
        return "Acc Statement";
    }

    public void store() {
        getFacade().storePaymenySummary(p);
    }

    public int getTotalShares() {
        int total = 0;
        if (getItems() != null) {
            for (Members m : getItems()) {
                total += m.getSharesPaid();
            }
        }
        return total;
    }

    public int getTotalReg() {
        int total = 0;
        if (getItems() != null) {
            for (Members m : getItems()) {
                total += m.getRegPaid();
            }
        }
        return total;
    }

    public int getTotalLsf() {
        int total = 0;
        if (getItems() != null) {
            for (Members m : getItems()) {
                total += m.getLoanDifferenceWithDeposits() - withdrawalcharges - m.getTotalRecoverableFromLoan();
            }
        }
        return total;
    }

    public int getTotalDepost() {
        int total = 0;
        if (getItems() != null) {
            for (Members m : getItems()) {
                total += m.getTotalDeposit();
            }
        }
        return total;
    }

    public int getTotalLoans() {
        int total = 0;
        if (getItems() != null) {
            for (Members m : getItems()) {
                total += m.getTotalUnpaidLoan();
            }
        }
        return total;
    }

    public int getTotalInterestEarned() {
        int total = 0;
        if (getItems() != null) {
            for (Members m : getItems()) {
                total += m.getTotalInterestPaidOnLoans();
            }
        }
        return total;
    }

    public int getTotalLoanRecovery() {
        int total = 0;
        if (getItems() != null) {
            for (Members m : getItems()) {
                total += m.getTotalLoanRecovery();
            }
        }
        return total;
    }

    public int getTotalWithDrawal() {
        int total = 0;
        if (getItems() != null) {
            for (Members m : getItems()) {
                total += m.getTotalWithdrawals();
            }
        }
        return total;
    }

    public int getTotalWithdrawalCharges() {
        int total = 0;
        if (getItems() != null) {
            for (Members m : getItems()) {
                total += m.getTotalWithdrawalsIncome();
            }
        }
        return total;
    }

    public int getRecoverFromMembers() {
        if (loan != null && selected != null) {
            if (loan.getBalEst() > selected.getTotalDeposit()) {
                return loan.getBalEst() - selected.getTotalDeposit();
            }
        }
        return 0;
    }

    public int getTotalForAllGuarantors() {
        int total = 0;
        if (getGuarators() != null);
        for (Guarantors g : getGuarators()) {
            total += g.getGuaranteedAmount();
        }
        return total;
    }

    public void deduct() {
        SaccoLedgerEntries sle = new SaccoLedgerEntries();
        sle.setCr(getRecoverFromMembers());
        sle.setDateWhen(new Date());
        sle.setDr(0);
        sle.setIsGeneral(true);
        sle.setParticulars("Loan Recovery From Guarantors");
        sle.setLedgerId(new SaccoLedgers(22));
        if (getRecoverFromMembers() > 0) {
            getFacade().saveLedgerEntry(sle);
        }
        sle = new SaccoLedgerEntries();
        sle.setCr(0);
        sle.setParticulars("Loan Recovery From Guarantors");
        sle.setIsGeneral(true);
        sle.setDateWhen(new Date());
        sle.setDr(getRecoverFromMembers());
        sle.setLedgerId(new SaccoLedgers(8));
        if (getRecoverFromMembers() > 0) {
            getFacade().saveLedgerEntry(sle);
        }
        Collections c = new Collections();
        if (getRecoverFromMembers() > 0 && !dummy.isEmpty()) {
            c.setAmount(loan.getBalEst());
        } else if (getRecoverFromMembers() > 0 && dummy.isEmpty()) {
            c.setAmount(selected.getTotalDeposit());
        } else if (getRecoverFromMembers() == 0) {
            c.setAmount(loan.getBalEst());
        }
        c.setCollectionstype(new CollectionTypes(loan.getLoantype().getId() + 1));
        c.setDateCollected(new Date());
        c.setMemberId(selected);
        c.setReceiptNo(11111);
        c.setStatusBilled(false);
        getFacade().storeCollections(c);
        getFacade().storeLoan(loan);
        DepositCash deposit = new DepositCash();
        if (selected.getTotalDeposit() > loan.getBalEst()) {
            deposit.setAmount(-(loan.getBalEst()));
        } else {
            deposit.setAmount(-(selected.getTotalDeposit()));
        }
        deposit.setDescription("loan recovery");
        deposit.setDate(new Date());
        deposit.setTransactedBy("");
        deposit.setTransactedBy("");
        deposit.setMemberNo(selected);
        if (deposit.getAmount() * -1 > 0) {
            getFacade().storeDeposit(deposit);
            sle = new SaccoLedgerEntries();
            sle.setDateWhen(new Date());
            sle.setCr(deposit.getAmount() * -1);
            sle.setDateWhen(new Date());
            sle.setDr(0);
            sle.setParticulars("Loan Recovery");
            sle.setIsGeneral(true);
            sle.setLedgerId(new SaccoLedgers(22));
            getFacade().saveLedgerEntry(sle);
            sle = new SaccoLedgerEntries();
            sle.setCr(0);
            sle.setIsGeneral(true);
            sle.setParticulars("Loan Recovery");
            sle.setDateWhen(new Date());
            sle.setDr(deposit.getAmount() * -1);
            sle.setLedgerId(new SaccoLedgers(8));
            getFacade().saveLedgerEntry(sle);
        }
        if (!dummy.isEmpty()) {
            for (Guarantors g : dummy) {
                AppConstants.logger("Recovered after" + g.getRecoveredAmount());
                deposit.setMemberNo(g.getGuarantorId());
                deposit.setAmount(-(int) g.getRecoveredAmount());
                getFacade().storeDeposit(deposit);
                LoanRecovery lr = new LoanRecovery();
                lr.setAmount((int) g.getRecoveredAmount());
                lr.setDateWhen(new Date());
                lr.setLoanId(g.getLoanId());
                lr.setMemberId(g.getGuarantorId());
                getFacade().storeLoanRecovery(lr);
            }
        }

        FacesContext.getCurrentInstance().addMessage(
                null,
                new FacesMessage(FacesMessage.SEVERITY_INFO,
                        "Loan amount has been recovered from guarantors", null));
        loan = null;
        selected = getFacade().findSelected(selected);

    }
    ArrayList<Guarantors> dummy;

    public ArrayList<Guarantors> getGuaratorsRecovery() {
        dummy = new ArrayList<>();
        if (loan != null && getRecoverFromMembers() > 0) {
            if (loan.getGuarantorsCollection() != null) {
                for (Guarantors g : loan.getGuarantorsCollection()) {
                    g.setRecoveredAmount((float) (Math.ceil((float) getRecoverFromMembers() * g.getRatio())));
                    AppConstants.logger("Recovered " + g.getRecoveredAmount());
                    dummy.add(g);
                }
            }
        }
        return dummy;
    }

    public List<Collections> getLoanCollections() {
        if (getLoan().getLoantype().getId() == 1) {
            return getFacade().getCollections(2, getLoan());
        } else {
            return getFacade().getCollections(3, getLoan());
        }
    }

    public void onPrintCollections() throws JRException, IOException {
        JasperPrint jasperPrint;
        AppConstants.logger("calling print");
        ArrayList<Collections> finallist = new ArrayList<>();
        int x = 1;
        for (Collections c : getCollectionsBetweenDates()) {
            c.setId(x);
            x++;
            finallist.add(c);
        }
        JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(finallist);
        FacesContext facesContext = FacesContext.getCurrentInstance();
        String reportPath = facesContext.getExternalContext().getRealPath("/report/collections.jasper");
        Map params = new HashMap();
        params.put("reportsdate", "MEMBERS COLLECTIONS FOR " + selected.getSurname() + " MEMBER NO:" + selected.getMemberNo() + " BETWEEN " + new SimpleDateFormat("dd-MM-yyyy").format(sdate) + " TO " + new SimpleDateFormat("dd-MM-yyyy").format(edate) + " RECEIPT TYPE:" + this.type.getType().toUpperCase());
        params.put("total", getTotalCollectionsBetweenDates());
        AppConstants.logger("params" + params.toString());
        jasperPrint = JasperFillManager.fillReport(reportPath, params, beanCollectionDataSource);
        HttpServletResponse httpServletResponse = (HttpServletResponse) facesContext.getExternalContext().getResponse();
        httpServletResponse.addHeader("Content-disposition", "attachment; filename=" + selected.getTrimmedSurname() + ".pdf");
        try (ServletOutputStream servletOutputStream = httpServletResponse.getOutputStream()) {
            JasperExportManager.exportReportToPdfStream(jasperPrint, servletOutputStream);
            servletOutputStream.flush();
        }
        FacesContext.getCurrentInstance().responseComplete();
    }

    public void onPrintCollectionsAll() throws IOException, JRException {
        JasperPrint jasperPrint;
        AppConstants.logger("calling print");
        ArrayList<Collections> finallist = new ArrayList<>();
        int x = 1;
        for (Collections c : getCollectionsBetweenDatesAll()) {
            c.setId(x);
            x++;
            finallist.add(c);
        }
        JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(finallist);
        FacesContext facesContext = FacesContext.getCurrentInstance();
        String reportPath = facesContext.getExternalContext().getRealPath("/report/CollectionWithTypejrxml.jasper");
        Map params = new HashMap();
        params.put("reportsdate", "MEMBERS COLLECTIONS FOR " + selected.getSurname() + " MEMBER NO:" + selected.getMemberNo() + " BETWEEN " + new SimpleDateFormat("dd-MM-yyyy").format(sdate) + " TO " + new SimpleDateFormat("dd-MM-yyyy").format(edate) + " ALL RECEIPT TYPES");
        params.put("total", getTotalCollectionsBetweenDatesAll());
        AppConstants.logger("params" + params.toString());
        jasperPrint = JasperFillManager.fillReport(reportPath, params, beanCollectionDataSource);
        HttpServletResponse httpServletResponse = (HttpServletResponse) facesContext.getExternalContext().getResponse();
        httpServletResponse.addHeader("Content-disposition", "attachment; filename=" + selected.getTrimmedSurname() + ".pdf");
        try (ServletOutputStream servletOutputStream = httpServletResponse.getOutputStream()) {
            JasperExportManager.exportReportToPdfStream(jasperPrint, servletOutputStream);
            servletOutputStream.flush();
        }
        FacesContext.getCurrentInstance().responseComplete();
    }

    public void onPrint() throws IOException, JRException {
        JasperPrint jasperPrint;
        AppConstants.logger("calling print");
        ArrayList<PaymentSummaryItems> finallist = new ArrayList<>();
        int x = 1;
        for (PaymentSummaryItems pt : selected.getPaymentSummaryItemsCollection()) {
            pt.setId(x);
            x++;
            finallist.add(pt);
        }
        JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(finallist);
        FacesContext facesContext = FacesContext.getCurrentInstance();
        String reportPath = facesContext.getExternalContext().getRealPath("/admin/report/statement.jasper");
        Map params = new HashMap();
        params.put("reportsdate", "MEMBER NAME:" + selected.getSurname() + ".  MEMBER NO: " + selected.getMemberNo() + ". SOCIETY: T.K.N SACCO.   STATEMENT AS AT: " + finallist.get(finallist.size() - 1).getDatePrepared() + ".");
        AppConstants.logger("params" + params.toString());
        jasperPrint = JasperFillManager.fillReport(reportPath, params, beanCollectionDataSource);
        HttpServletResponse httpServletResponse = (HttpServletResponse) facesContext.getExternalContext().getResponse();
        httpServletResponse.addHeader("Content-disposition", "attachment; filename=" + selected.getTrimmedSurname() + ".pdf");
        try (ServletOutputStream servletOutputStream = httpServletResponse.getOutputStream()) {
            JasperExportManager.exportReportToPdfStream(jasperPrint, servletOutputStream);
            servletOutputStream.flush();
        }
        FacesContext.getCurrentInstance().responseComplete();
    }

    public void createWithDrawal() {
        depo.setAmount(depo.getAmount() * -1);
        depo.setMemberNo(selected);
        depo.setTransactedBy("");
        depo.setDate(new Date());
        getFacade().storeDeposit(depo);
        FacesContext.getCurrentInstance().addMessage(
                null,
                new FacesMessage(FacesMessage.SEVERITY_INFO,
                        "Withdrawal successiful", null));
        depo = null;
        selected = getFacade().findSelected(selected);
    }

    public void stopBillingLoan() {
        loan.setStopBilling(true);
        getFacade().storeLoan(loan);
        JsfUtil.addSuccessMessage("Loan interest billing stoped");
        updateMember();
    }

    public void startBillingLoan() {
        loan.setStopBilling(false);
        getFacade().storeLoan(loan);
        JsfUtil.addSuccessMessage("Loan interest billing restarted");
        updateMember();
    }

    public void loanCleared() {
        loan.setLoanStatus(true);
        getFacade().mergeLoan(loan);
        JsfUtil.addSuccessMessage("Loan cleared");
    }

    public int getTotalNegotiationEarned() {
        int total = 0;
        if (getItems() != null) {
            for (Members m : getItems()) {
                total += m.getTotalNegotiationPaidOnLoans();
            }
        }
        return total;
    }

    public void onPrintReport() throws IOException, JRException {
        JasperPrint jasperPrint;
        AppConstants.logger("calling print");
        ArrayList<Members> finallist = new ArrayList<>();
        int x = 1;
        for (Members m : getItems()) {
            m.setId(x);
            x++;
            finallist.add(m);
        }
        JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(finallist);
        FacesContext facesContext = FacesContext.getCurrentInstance();
        String reportPath = facesContext.getExternalContext().getRealPath("/admin/report/membersAccStatusReport.jasper");
        Map params = new HashMap();

        params.put("totalpaidShares", getTotalShares());
        params.put("totaldepositedAmount", getTotalDepost());
        params.put("totalLoanUnPaid", getTotalLoans());
        params.put("totalLoanInterestPaid", getTotalInterestEarned());
        params.put("totalRecoveredForLoan", getTotalLoanRecovery());
        params.put("totalwithdrawal", getTotalWithDrawal());
        params.put("totalwithdrawalincome", getTotalWithdrawalCharges());
        params.put("totalnegotiation", getTotalNegotiationEarned());
        params.put("totalregistration", getTotalReg());
        params.put("reportsdate", new Date().toString());
        jasperPrint = JasperFillManager.fillReport(reportPath, params, beanCollectionDataSource);
        HttpServletResponse httpServletResponse = (HttpServletResponse) facesContext.getExternalContext().getResponse();
        httpServletResponse.addHeader("Content-disposition", "attachment; filename=" + "all" + ".pdf");
        try (ServletOutputStream servletOutputStream = httpServletResponse.getOutputStream()) {
            JasperExportManager.exportReportToPdfStream(jasperPrint, servletOutputStream);
            servletOutputStream.flush();
        }
        FacesContext.getCurrentInstance().responseComplete();
    }

    public Loan getActiveLoanForMember() {
        return getFacade().findMemberActiveNormalLoan(selected);
    }

    public LoanSchedules getCurrentSchedule() {
        return getFacade().getScheduleCurrent(selected);
    }
}
