package controllers;

import entities.ExpensesPaymentMethod;
import controllers.util.JsfUtil;
import controllers.util.JsfUtil.PersistAction;
import facades.ExpensesPaymenMethodFacade;

import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@Named("expensesPaymentMethodController")
@SessionScoped
public class ExpensesPaymentMethodController implements Serializable {

    @EJB
    private facades.ExpensesPaymenMethodFacade ejbFacade;
    private List<ExpensesPaymentMethod> items = null;
    private ExpensesPaymentMethod selected;

    public ExpensesPaymentMethodController() {
    }

    public ExpensesPaymentMethod getSelected() {
        return selected;
    }

    public void setSelected(ExpensesPaymentMethod selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private ExpensesPaymenMethodFacade getFacade() {
        return ejbFacade;
    }

    public ExpensesPaymentMethod prepareCreate() {
        selected = new ExpensesPaymentMethod();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("ExpensesPaymentMethodCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("ExpensesPaymentMethodUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("ExpensesPaymentMethodDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<ExpensesPaymentMethod> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public ExpensesPaymentMethod getExpensesPaymentMethod(java.lang.Integer id) {
        return getFacade().find(id);
    }

    public List<ExpensesPaymentMethod> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<ExpensesPaymentMethod> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = ExpensesPaymentMethod.class)
    public static class ExpensesPaymentMethodControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            ExpensesPaymentMethodController controller = (ExpensesPaymentMethodController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "expensesPaymentMethodController");
            return controller.getExpensesPaymentMethod(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof ExpensesPaymentMethod) {
                ExpensesPaymentMethod o = (ExpensesPaymentMethod) object;
                return getStringKey(o.getId());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), ExpensesPaymentMethod.class.getName()});
                return null;
            }
        }

    }

}
