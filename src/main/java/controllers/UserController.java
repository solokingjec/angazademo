/**
 * Copyright (c) 2014 Oracle and/or its affiliates. All rights reserved.
 *
 * You may not modify, use, reproduce, or distribute this software except in
 * compliance with  the terms of the License at:
 * http://java.net/projects/javaeetutorial/pages/BerkeleyLicense
 */
package controllers;

import controllers.util.AppConstants;
import controllers.util.JsfUtil;
import controllers.util.LoggedIn;
import entities.AdminProfile;
import entities.Clerk;
import entities.Groups;
import entities.Users;
import facades.UserBean;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Produces;
import javax.faces.application.FacesMessage;
import javax.faces.application.NavigationHandler;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.apache.shiro.subject.Subject;
import org.primefaces.context.RequestContext;

/**
 *
 * @author markito
 */
@Named(value = "userController")
@SessionScoped
public class UserController implements Serializable {

    private static final String BUNDLE = "bundles.Bundle";
    private String selectedUserName = "";
    private static final long serialVersionUID = -8851462237612818158L;

    Users selected;
    Users user;
    @EJB
    private UserBean ejbFacade;
    private String username;
    private String password;
    private Clerk clerk;
    private String userOldPassword, userNewPassWord, userNewPassWordConfirm, userNewUserName;
    private boolean isShowNewPassWordEntry = false;
    private AdminProfile profile;

    public String getUserNewUserName() {
        return userNewUserName;
    }
    private int attempts = 0;

    public void setUserNewUserName(String userNewUserName) {
        this.userNewUserName = userNewUserName;
    }

    public int getAttempts() {
        return attempts;
    }

    public void setAttempts(int attempts) {
        this.attempts = attempts;
    }

    public void onSelect() {
        selectedUserName = selected.getUsername();
        AppConstants.logger(selectedUserName);
    }

    public void changePassword() {
        AppConstants.logger("password change");
        if (userNewPassWord.equals(userNewPassWordConfirm)) {
            Users users = getAuthenticatedUser();
            users.setPassword(JsfUtil.hashGenerator(userNewPassWord));
            ejbFacade.updateUserPassword(users);
            JsfUtil.addSuccessMessage("Password change success.");
            isShowNewPassWordEntry = false;
            userOldPassword = "";
        } else {
            JsfUtil.addErrorMessage("Password confirmation does not match.");
        }

    }

    public void reset() {
        userNewPassWord = "";
        userNewPassWordConfirm = "";
        userOldPassword = "";
        isShowNewPassWordEntry = false;
    }

    public String getUserNewPassWordConfirm() {
        return userNewPassWordConfirm;
    }

    public void setUserNewPassWordConfirm(String userNewPassWordConfirm) {
        this.userNewPassWordConfirm = userNewPassWordConfirm;
    }

    public boolean isIsShowNewPassWordEntry() {
        return isShowNewPassWordEntry;
    }

    public void setIsShowNewPassWordEntry(boolean isShowNewPassWordEntry) {
        this.isShowNewPassWordEntry = isShowNewPassWordEntry;
    }

    public String getUserOldPassword() {
        return userOldPassword;
    }

    public void setUserOldPassword(String userOldPassword) {
        this.userOldPassword = userOldPassword;
    }

    public void compare() {
        String oldPassHash = this.getAuthenticatedUser().getPassword();
        AppConstants.logger("oldhash" + oldPassHash);
        AppConstants.logger("old pass entered" + userOldPassword);
        AppConstants.logger("old pass entered hash=" + JsfUtil.hashGenerator(userOldPassword));
        if (oldPassHash.equals(JsfUtil.hashGenerator(userOldPassword))) {
            AppConstants.logger("password match");
            isShowNewPassWordEntry = true;
            JsfUtil.addErrorMessage("Enter new password. We encourage alphanumeric phrase and 10+ characters long.");
        } else {
            JsfUtil.addErrorMessage("Password does not match. Contact admin for new password.");
            isShowNewPassWordEntry = false;
        }

    }

    public void editUserAdminProfile() {
        getEjbFacade().updateProfile(profile);
        JsfUtil.addSuccessMessage("Profile Updated");
    }

    public void compareProfile() {
        String oldPassHash = this.getAuthenticatedUser().getPassword();
        AppConstants.logger("oldhash" + oldPassHash);
        AppConstants.logger("old pass entered" + userOldPassword);
        AppConstants.logger("old pass entered hash=" + JsfUtil.hashGenerator(userOldPassword));
        if (oldPassHash.equals(JsfUtil.hashGenerator(userOldPassword))) {
            AppConstants.logger("password match");
            isShowNewPassWordEntry = true;
        } else {
            JsfUtil.addErrorMessage("Invalid Password. You must enter a valid password to change the details for this account");
            isShowNewPassWordEntry = false;
        }

    }

    public void compareUname() {
        String oldPassHash = this.getAuthenticatedUser().getPassword();
        AppConstants.logger("oldhash" + oldPassHash);
        AppConstants.logger("old pass entered" + userOldPassword);
        AppConstants.logger("old pass entered hash=" + JsfUtil.hashGenerator(userOldPassword));
        if (oldPassHash.equals(JsfUtil.hashGenerator(userOldPassword))) {
            AppConstants.logger("password match");
            isShowNewPassWordEntry = true;
            JsfUtil.addErrorMessage("Please enter new username. The system will log you out for the changes to be applied.");
        } else {
            JsfUtil.addErrorMessage("Invalid Password. You must enter a valid password to change the details for this account");
            isShowNewPassWordEntry = false;
        }

    }

    public String getUserNewPassWord() {
        return userNewPassWord;
    }

    public void setUserNewPassWord(String userNewPassWord) {
        this.userNewPassWord = userNewPassWord;
    }

    /**
     * Creates a new instance of Login
     */
    public UserController() {
    }

    public AdminProfile getProfile() {
        return profile;
    }

    public void setProfile(AdminProfile profile) {
        this.profile = profile;
    }

    public void prepareCreateAdmin() {
        selected = new Users();
        profile = new AdminProfile();
    }

    public void prepareCreate() {
        selected = new Users();
        clerk = new Clerk();
    }

    public Clerk getClerk() {
        return clerk;
    }

    public void setClerk(Clerk clerk) {
        this.clerk = clerk;
    }

    public Users getSelected() {
        return selected;
    }

    public void setSelected(Users selected) {
        this.selected = selected;
    }

    public void create() {
        String passWordHash = controllers.util.JsfUtil.hashGenerator(selected.getPassword());
        selected.setPassword(passWordHash);
        getEjbFacade().create(selected);
        selected = getEjbFacade().getUserByUsername(selected.getUsername(), selected.getPassword());
        Groups psg = new Groups();
        psg.setGroupname("parcelclerk");
        psg.setUsername(selected);
        getEjbFacade().persistPersonGroup(psg);
        clerk.setUsername(selected);
        clerk.setStatusActive(true);
        getEjbFacade().storeClerk(clerk);
        JsfUtil.addSuccessMessage("Parcel Clerk Added");

    }

    public void createAdmin() {
        String passWordHash = controllers.util.JsfUtil.hashGenerator(selected.getPassword());
        selected.setPassword(passWordHash);
        getEjbFacade().create(selected);
        selected = getEjbFacade().getUserByUsername(selected.getUsername(), selected.getPassword());
        Groups psg = new Groups();
        psg.setGroupname("staff");
        psg.setUsername(selected);
        getEjbFacade().persistPersonGroup(psg);
        profile.setUsername(selected);
        profile.setStatusActive(true);
        getEjbFacade().persitAdminProfile(profile);
        JsfUtil.addSuccessMessage("Staff Added");

    }

    public void editStageClerk() {
        getEjbFacade().updateClerkDetail(clerk);
        JsfUtil.addSuccessMessage("Clerk Details Updated");
    }

    public void prepareCreateStageClerk() {
        clerk = new Clerk();
    }

    public void createStageClerk() {
        getEjbFacade().storeClerk(clerk);
        JsfUtil.addSuccessMessage("Clerk Added");

    }

    public String getTemplate() {
        if (isAdmin()) {
            return "/admin/template/main.xhtml";
        }
        return "/staff/template/main.xhtml";
    }

    public String getDashBoardPath() {
        if (isAdmin()) {
            return "admin";
        }
        return "staff";
    }

    public List<Users> getAllPersons() {
        return getEjbFacade().getAllPersons(getAuthenticatedUser().getUsername());
    }

    /**
     * Login method based on <code>HttpServletRequest</code> and security realm
     *
     * @return
     */
    public String login() {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
        String result;

        try {
            request.login(this.getUsername(), this.getPassword());
            this.user = ejbFacade.getUserByUsername(getUsername());
            this.getAuthenticatedUser();

            if (isAdmin()) {
                result = "/admin/dashboard?faces-redirect=true";
            } else if (isPercelClerk()) {
                //result = "/users/reports/deliveriesByCar";
                result = "/parcel/parcelhome?faces-redirect=true";
            } else {
                logout();
                JsfUtil.addErrorMessage("You are not authorised to access this system. Contact Admin");
                result = "index";
            }

        } catch (ServletException ex) {
            //Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
            if (ex.getMessage().contains("request") || ex.getMessage().contains("already") || ex.getMessage().contains("authenticated")) {
                AppConstants.logger("already loged in");
                if (isAdmin()) {
                    result = "/admin/dashboard?faces-redirect=true";
                } else {
                    //result = "/users/reports/deliveriesByCar";
                    result = "/parcel/parcelhome?faces-redirect=true";
                }
            } else {
                RequestContext requestContext = RequestContext.getCurrentInstance();
                JsfUtil.addErrorMessage("Login Failed!!!!!!.Please retry");
                requestContext.update("login:growl");
                result = "index";
            }
        }

        return result;
    }

    public String logout() {
        AppConstants.logger("logout");
        // Notice the redirect syntax. The forward slash means start at
        // the root of the web application.
        String destination = "/index?faces-redirect=true";

        // FacesContext provides access to other container managed objects,
        // such as the HttpServletRequest object, which is needed to perform
        // the logout operation.
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request
                = (HttpServletRequest) context.getExternalContext().getRequest();

        try {
            // added May 12, 2014
            HttpSession session = request.getSession();
            session.invalidate();

            // this does not invalidate the session but does null out the user Principle
            request.logout();
        } catch (ServletException e) {
            //log.log(Level.SEVERE, "Failed to logout user!", e);
            destination = "/index?faces-redirect=true";
        }

        return destination; // go to destination
    }

    /**
     * @return the ejbFacade
     */
    public UserBean getEjbFacade() {
        return ejbFacade;
    }

    public @Produces
    @LoggedIn
    Users getAuthenticatedUser() {
        return user;
    }

    public boolean isLogged() {
        return (getUser() != null);
    }

    public boolean isAdmin() {
        for (Groups g : user.getGroupsCollection()) {
            if (g.getGroupname().equals("admin")) {
                return true;
            }
        }
        return false;
    }

    public boolean isStaff() {
        for (Groups g : user.getGroupsCollection()) {
            if (g.getGroupname().equals("staff")) {
                return true;
            }
        }
        return false;
    }

    public boolean isActive() {
        for (AdminProfile g : user.getAdminProfileCollection()) {
            if (g.getStatusActive()) {
                return true;
            }
        }
        return false;
    }

    public boolean isPercelClerk() {
        for (Groups g : user.getGroupsCollection()) {
            if (g.getGroupname().equals("parcelclerk") && user.getClerkDetails().getStatusActive()) {
                return true;
            }
        }
        return false;
    }

    public String goAdmin() {
        if (isAdmin()) {
            return "/admin/index";
        } else {
            return "index";
        }
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the user
     */
    public Users getUser() {
        return user;
    }

    public void editUserPassword() {
        selected.setPassword(JsfUtil.hashGenerator("mypassword"));
        getEjbFacade().updateUserPassword(selected);
        JsfUtil.addSuccessMessage("Password changed");
    }

    public void editUser() {
        getEjbFacade().edit(selected);
        getEjbFacade().updateClerkDetail(selected.getClerkDetails());
        if (selectedUserName.equalsIgnoreCase(selected.getUsername())) {
            JsfUtil.addSuccessMessage("User Details Updated");
            return;
        }
        selected = getEjbFacade().getUserByUsername(selected.getUsername());
        Groups g = getEjbFacade().findGroup(selectedUserName);
        Clerk p = getEjbFacade().findClerk(selectedUserName);
        g.setUsername(selected);
        p.setUsername(selected);
        getEjbFacade().updateProfileClerk(p);
        getEjbFacade().upDateGroup(g);
        JsfUtil.addSuccessMessage("User Details Updated");
        getEjbFacade().deleteUser(selectedUserName);
        selectedUserName = selected.getUsername();
    }

    public void prepareChangeUserName() {
        selectedUserName = getAuthenticatedUser().getUsername();
        selected = getAuthenticatedUser();
        profile = getAuthenticatedUser().getAdminProfile();
    }

    public void editUserAdmin() {
        getEjbFacade().edit(selected);
        getEjbFacade().updateProfile(selected.getAdminProfile());
        if (selectedUserName.equalsIgnoreCase(selected.getUsername())) {
            JsfUtil.addSuccessMessage("User Details Updated");
            return;
        }
        selected = getEjbFacade().getUserByUsername(selected.getUsername());
        Groups g = getEjbFacade().findGroup(selectedUserName);
        AdminProfile p = getEjbFacade().findAdminUserName(selectedUserName);
        g.setUsername(selected);
        p.setUsername(selected);
        getEjbFacade().updateProfile(p);
        getEjbFacade().upDateGroup(g);
        JsfUtil.addSuccessMessage("User Details Updated");
        getEjbFacade().deleteUser(selectedUserName);
        selectedUserName = selected.getUsername();
    }

    public void editUserAdminUserName() {
        getEjbFacade().edit(selected);
        getEjbFacade().updateProfile(selected.getAdminProfile());
        if (selectedUserName.equalsIgnoreCase(selected.getUsername())) {
            JsfUtil.addSuccessMessage("User Details Updated");
            return;
        }
        selected = getEjbFacade().getUserByUsername(selected.getUsername());
        Groups g = getEjbFacade().findGroup(selectedUserName);
        AdminProfile p = getEjbFacade().findAdminUserName(selectedUserName);
        g.setUsername(selected);
        p.setUsername(selected);
        getEjbFacade().updateProfile(p);
        getEjbFacade().upDateGroup(g);
        JsfUtil.addSuccessMessage("User Details Updated");
        getEjbFacade().deleteUser(selectedUserName);
        selectedUserName = selected.getUsername();
        logoutUser();
    }

    private boolean rememberMe;

    public boolean isRememberMe() {
        return rememberMe;
    }

    public void setRememberMe(boolean rememberMe) {
        this.rememberMe = rememberMe;
    }

    public void logoutUser() {
        SecurityUtils.getSubject().logout();
        NavigationHandler nh = FacesContext.getCurrentInstance().getApplication().getNavigationHandler();
        nh.handleNavigation(FacesContext.getCurrentInstance(), null, "/login?faces-redirect=true");
    }

    public void loginUser() {
        try {
            Subject currentUser = SecurityUtils.getSubject();
            UsernamePasswordToken token = new UsernamePasswordToken(username, new Sha256Hash(password).toHex());
            token.setRememberMe(rememberMe);
            currentUser.login(token);
            attempts = 0;

        } catch (UnknownAccountException uae) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Login failed!", "Your username wrong"));

        } catch (IncorrectCredentialsException ice) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Login failed!", "Password is incorrect"));

            attempts++;
            AppConstants.logger("attemptsa-" + attempts);
            if (attempts > 5) {
                JsfUtil.addErrorMessage("Too many attemps. Your IP address has been reported for investigation");
            }
        } catch (LockedAccountException lae) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Login failed!", "This username is locked"));

        } catch (AuthenticationException aex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Login failed!", aex.toString()));
        }

    }

    public void authorizedUserControl() {
        if (null != SecurityUtils.getSubject().getPrincipal()) {
            this.user = ejbFacade.getUserByUsername(getUsername());
            this.getAuthenticatedUser();
            String result = "";
            if (isAdmin()) {
                result = "/admin/members/List?faces-redirect=true";
            } else if (isStaff() & isActive()) {
                AppConstants.logger("clerk");
                //result = "/users/reports/deliveriesByCar";
                result = "/staff/dashboard?faces-redirect=true";
            } else if (isPercelClerk()) {
                AppConstants.logger("clerk");
                //result = "/users/reports/deliveriesByCar";
                result = "/parcel/parcelhome?faces-redirect=true";
            } else {
                logout();
                JsfUtil.addErrorMessage("You are not authorised to access this system. Contact Admin");
                result = "login";
            }
            NavigationHandler nh = FacesContext.getCurrentInstance().getApplication().getNavigationHandler();
            nh.handleNavigation(FacesContext.getCurrentInstance(), null, result);
            String renderKitId = FacesContext.getCurrentInstance().getViewRoot().getRenderKitId();
            AppConstants.logger(renderKitId);
            if (renderKitId.equalsIgnoreCase("PRIMEFACES_MOBILE")) {
                FacesContext.getCurrentInstance().getApplication().getNaviga‌​tionHandler().handleNavigation(FacesContext.getCurrentInstance(), "MOBILE", "/index.htm");
                AppConstants.logger("Mobile");
            }
        }
    }
}
