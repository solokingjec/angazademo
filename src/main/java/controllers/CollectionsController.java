package controllers;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import controllers.util.AppConstants;
import controllers.util.JsfUtil;
import controllers.util.JsfUtil.PersistAction;
import entities.BillTransactions;
import entities.CollectionTypes;
import entities.Collections;
import entities.DepositCash;
import entities.LoanTypes;
import entities.MemberBills;
import entities.SaccoLedgerEntries;
import entities.SaccoLedgers;
import facades.CollectionsFacade;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Named;
import javax.servlet.http.Part;
import org.joda.time.DateTime;
import org.primefaces.event.CellEditEvent;
import org.primefaces.event.RowEditEvent;

@Named("collectionsController")
@SessionScoped
public class CollectionsController implements Serializable {

    @EJB
    private facades.CollectionsFacade ejbFacade;
    private List<Collections> items = null;
    private List<Collections> dummy = new ArrayList<>();
    private Collections selected;
    private Date dummyDate;
    private LoanTypes savings = new LoanTypes();
    private Date DateEntry = new Date();
    private CollectionTypes cl = new CollectionTypes();
    private int dummyCount;
    private Part filePart;
    private facades.MembersFacade ejbFacadeMemberFacade;

    @PostConstruct
    public void loadType() {
        cl = getFacade().findCollectionType(1);
    }

    public CollectionTypes getCl() {
        return cl;
    }

    public facades.MembersFacade getMembersFacade() {
        return ejbFacadeMemberFacade;
    }

    public List<Collections> getDummy() {
        if (dummyCount > 0) {
            if (dummyCount == dummy.size()) {
                for (Collections c : dummy) {
                    c.setCollectionstype(cl);
                    c.setDateCollected(getDummyDate());
                }
                return dummy;
            }
            dummy = new ArrayList<>();
            for (int x = 1; x <= dummyCount; x++) {
                Collections c = new Collections();
                c.setId(x - 1);
                if (cl.getId() == 1) {
                    //c.setAmount(200);
                }
                c.setCollectionstype(cl);
                c.setDateCollected(getDummyDate());
                AppConstants.logger(c.toString());
                dummy.add(c);
            }
        }
        return dummy;
    }

    public Date getDummyDate() {
        return dummyDate;
    }

    public void setDummyDate(Date dummyDate) {
        this.dummyDate = dummyDate;
    }

    public int getDummyCount() {
        return dummyCount;
    }

    public void setDummyCount(int dummyCount) {
        this.dummyCount = dummyCount;
    }

    public void setDummy(List<Collections> dummy) {
        this.dummy = dummy;
    }

    public void setCl(CollectionTypes cl) {
        this.cl = cl;
    }

    public Date getDateEntry() {
        return DateEntry;
    }

    public void setDateEntry(Date DateEntry) {
        this.DateEntry = DateEntry;
    }

    public LoanTypes getSavings() {
        return savings;
    }

    public void setSavings(LoanTypes savings) {
        this.savings = savings;
    }

    public CollectionsController() {
    }

    public Collections getSelected() {
        return selected;
    }

    public List<Collections> getItemsByDate() {
        DateTime dt = new DateTime(DateEntry);
        return getFacade().findItemsByDate(dt.toDate());

    }

    public List<Collections> getItemsByDateAndType() {
        DateTime dt = new DateTime(DateEntry);
        return getFacade().findItemsByDate(dt.toDate(), cl);
    }

    public int getTotal() {
        int total = 0;
        if (getItemsByDate() != null) {
            for (Collections c : getItemsByDate()) {
                total += c.getAmount();
            }
        }
        return total;
    }

    public int getTotalDateAndType() {
        int total = 0;
        if (getItemsByDate() != null) {
            for (Collections c : getItemsByDateAndType()) {
                total += c.getAmount();
            }
        }
        return total;
    }

    public void setSelected(Collections selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private CollectionsFacade getFacade() {
        return ejbFacade;
    }

    public Collections prepareCreate() {
        selected = new Collections();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        int random = new Random().nextInt(10000000);//this will be used to identify which object we have just persisted
        selected.setRandomInt(random);
        selected.setCollectionstype(cl);
        selected.setDateCollected(DateEntry);
        int remainingafter = 0;
        //if collection is savings and withdrawable and the amount is not 0,the status should be billed.
        if ((selected.getCollectionstype().getId() == 1 || selected.getCollectionstype().getId() == 4) && selected.getAmount() > 0) {
            selected.setStatusBilled(true);
        }
        System.out.println("receipt type" + selected.getCollectionstype().getId());
        //if its for emergency loan and the loan balance is 0, we make it a deposit
        if (selected.getCollectionstype().getId() == 2) {
            if (selected.getMemberId().getActiveLoanEmergency() != null) {
                if (selected.getMemberId().getActiveLoanEmergency().getBalEst() <= 0) {
                    //when the receipt is for emergency loan and the member has a normal loan no emergency
                    if (!selected.getMemberId().hasPendingEmergency()) {
                        selected.setStatusBilled(true);
                        selected.setCollectionstype(new CollectionTypes(4));
                    } else if (selected.getMemberId().hasPendingEmergency()) {
                        AppConstants.logger("amount received:" + selected.getAmount());

                        //member has a pending loan
                        //calculate amount paid but not billed
                        int emergencyCollectionsTotal = 0;
                        for (Collections col : getFacade().getMemberUnbilledCollections(selected.getMemberId())) {
                            if (col.getCollectionstype().getId() == 2) {
                                emergencyCollectionsTotal += col.getAmount();
                            }
                        }
                        AppConstants.logger("total amount paid for emergency loan:" + emergencyCollectionsTotal);
                        int amountForOldLoan = (int) (selected.getMemberId().getActiveLoanEmergency().getCurrentSchedule().getTotalDebits() + selected.getMemberId().getActiveLoanEmergency().getCurrentSchedule().getBalance());
                        AppConstants.logger("Amount to clear the active loan" + amountForOldLoan);
                        int excess = emergencyCollectionsTotal - amountForOldLoan;
                        AppConstants.logger("amount already paid extra for the new loan is:" + excess);
                        //test whether the amount is exceeding the pending loan bal
                        int balForNewLoan = selected.getMemberId().getBalanceForPendingLoanEmergency();
                        AppConstants.logger("new loan bal" + balForNewLoan);
                        int amountRemainingForthenewLoan = balForNewLoan - excess;
                        AppConstants.logger("outstanding loan bal:" + amountRemainingForthenewLoan);
                        if (amountRemainingForthenewLoan < selected.getAmount()) {
                            remainingafter = selected.getAmount() - amountRemainingForthenewLoan;
                            selected.setAmount(amountRemainingForthenewLoan);
                            AppConstants.logger("amount sent to withdrawable:" + remainingafter);
                        }
                        int finalbal = selected.getAmount() - amountRemainingForthenewLoan;
                        AppConstants.logger("final outstanding=:" + finalbal);
                    }
                } else if (selected.getMemberId().getActiveLoanEmergency().getBalEst() <= selected.getAmount()) {
                    remainingafter = selected.getAmount() - selected.getMemberId().getActiveLoanEmergency().getBalEst();
                    selected.setAmount(selected.getAmount() - remainingafter);
                }

            } else if (selected.getMemberId().getActiveLoanNormal() != null) {
                if (selected.getMemberId().getActiveLoanNormal().getBalEst() > 0) {
                    selected.setCollectionstype(new CollectionTypes(3));
                    remainingafter = selected.getAmount() - selected.getMemberId().getActiveLoanNormal().getBalEst();
                    selected.setAmount(selected.getMemberId().getActiveLoanNormal().getBalEst());
                } else if (selected.getMemberId().hasPendingNormal()) {
                    selected.setCollectionstype(new CollectionTypes(3));
                }
            } else {
                selected.setStatusBilled(true);
                selected.setCollectionstype(new CollectionTypes(4));
            }
        }
        //if its for normal loan and the loan balance is 0, we make it a deposit
        if (selected.getCollectionstype().getId() == 3) {
            if (selected.getMemberId().getActiveLoanNormal() != null) {
                if (selected.getMemberId().getActiveLoanNormal().getBalEst() <= 0) {
                    if (!selected.getMemberId().hasPendingNormal()) {
                        selected.setStatusBilled(true);
                        selected.setCollectionstype(new CollectionTypes(4));
                    } else if (selected.getMemberId().hasPendingNormal()) {
                        AppConstants.logger("amount received:" + selected.getAmount());

                        //member has a pending loan
                        //calculate amount paid but not billed
                        int normalCollectionsTotal = 0;
                        for (Collections col : getFacade().getMemberUnbilledCollections(selected.getMemberId())) {
                            if (col.getCollectionstype().getId() == 3) {
                                normalCollectionsTotal += col.getAmount();
                            }
                        }
                        AppConstants.logger("total amount paid for normal loan:" + normalCollectionsTotal);
                        int amountForOldLoan = (int) (selected.getMemberId().getActiveLoanNormal().getCurrentSchedule().getTotalDebits() + selected.getMemberId().getActiveLoanNormal().getCurrentSchedule().getBalance());
                        AppConstants.logger("Amount to clear the active loan" + amountForOldLoan);
                        int excess = normalCollectionsTotal - amountForOldLoan;
                        AppConstants.logger("amount already paid extra for the new loan is:" + excess);
                        //test whether the amount is exceeding the pending loan bal
                        int balForNewLoan = selected.getMemberId().getBalanceForPendingLoanNormal();
                        AppConstants.logger("new loan bal" + balForNewLoan);
                        int amountRemainingForthenewLoan = balForNewLoan - excess;
                        AppConstants.logger("outstanding loan bal:" + amountRemainingForthenewLoan);
                        if (amountRemainingForthenewLoan < selected.getAmount()) {
                            remainingafter = selected.getAmount() - amountRemainingForthenewLoan;
                            selected.setAmount(amountRemainingForthenewLoan);
                            AppConstants.logger("amount sent to withdrawable:" + remainingafter);
                        }
                        int finalbal = selected.getAmount() - amountRemainingForthenewLoan;
                        AppConstants.logger("final outstanding=:" + finalbal);
                    }
                } else if (selected.getMemberId().getActiveLoanNormal().getBalEst() <= selected.getAmount() && (!selected.getMemberId().hasPendingNormal())) {
                    remainingafter = selected.getAmount() - selected.getMemberId().getActiveLoanNormal().getBalEst();
                    selected.setAmount(selected.getMemberId().getActiveLoanNormal().getBalEst());
                }
            } else if (selected.getMemberId().getActiveLoanEmergency() != null) {
                if (selected.getMemberId().getActiveLoanEmergency().getBalEst() > 0) {
                    selected.setCollectionstype(new CollectionTypes(2));
                    remainingafter = selected.getAmount() - selected.getMemberId().getActiveLoanEmergency().getBalEst();
                    selected.setAmount(selected.getMemberId().getActiveLoanEmergency().getBalEst());
                } else if (selected.getMemberId().hasPendingEmergency()) {
                    selected.setCollectionstype(new CollectionTypes(2));
                }
            } else {
                selected.setStatusBilled(true);
                selected.setCollectionstype(new CollectionTypes(4));
            }
        }
        //if the member has a normal loan and the normal loan balance is less or equal to the amount,the remaining amount after paying loan
        //is computed by subtracting the loan bal from the amount paid.
        //e.g suppose the member has a loan bal of 5000 and he has paid 6000. we will store a loan repayment
        //of 5000, and then pay 1000 as a deposit

        //we save the collection
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("CollectionsCreated"));
        if (!JsfUtil.isValidationFailed()) {
            selected = getFacade().getByRandom(selected.getRandomInt());
            selected.setRandomInt(-1);
            getFacade().edit(selected);
            //if the collection is for savings or withdrawable
            if ((selected.getCollectionstype().getId() == 1 || selected.getCollectionstype().getId() == 4) && selected.getAmount() > 0) {
                int remaining = selected.getAmount();
                if (selected.getMemberId().getBillsTotalArrears() < 0) {
                    for (MemberBills mb : selected.getMemberId().getMemberBillsCollection()) {
                        if (remaining > 0) {
                            if (mb.getNetAcc() < 0) {
                                if (remaining == mb.getNetAcc() * -1 || remaining < mb.getNetAcc() * -1) {
                                    BillTransactions bt = new BillTransactions();
                                    bt.setAmountCredit(remaining);
                                    bt.setDatePaid(selected.getDateCollected());
                                    bt.setBillId(mb);
                                    bt.setNaration(mb.getNarration());
                                    bt.setReceiptNo(Integer.toString(selected.getReceiptNo()));
                                    getFacade().storeBillTransaction(bt);
                                    switch (bt.getNaration()) {
                                        case "shares": {
                                            AppConstants.logger("setting cr for shares" + remaining);
                                            SaccoLedgerEntries sle = new SaccoLedgerEntries();
                                            sle.setCr(remaining);
                                            sle.setDateWhen(selected.getDateCollected());
                                            sle.setDr(0);
                                            sle.setParticulars("Shares payment");
                                            sle.setLedgerId(new SaccoLedgers(1));
                                            getFacade().saveLedgerEntry(sle);
                                            sle = new SaccoLedgerEntries();
                                            sle.setCr(0);
                                            sle.setParticulars("Shares payment");
                                            sle.setDateWhen(selected.getDateCollected());
                                            sle.setDr(remaining);
                                            sle.setLedgerId(new SaccoLedgers(6));
                                            getFacade().saveLedgerEntry(sle);
                                            break;
                                        }
                                        case "registration": {
                                            AppConstants.logger("setting cr for registration" + remaining);
                                            SaccoLedgerEntries sle = new SaccoLedgerEntries();
                                            sle.setCr(remaining);
                                            sle.setDateWhen(selected.getDateCollected());
                                            sle.setDr(0);
                                            sle.setParticulars("Registration payment");
                                            sle.setLedgerId(new SaccoLedgers(18));
                                            getFacade().saveLedgerEntry(sle);
                                            sle = new SaccoLedgerEntries();
                                            sle.setCr(0);
                                            sle.setDateWhen(selected.getDateCollected());
                                            sle.setDr(remaining);
                                            sle.setParticulars("Registration payment");
                                            sle.setLedgerId(new SaccoLedgers(6));
                                            getFacade().saveLedgerEntry(sle);
                                            break;
                                        }
                                    }
                                    remaining = 0;
                                    break;
                                } else {
                                    remaining = remaining - mb.getNetAcc() * -1;
                                    BillTransactions bt = new BillTransactions();
                                    bt.setAmountCredit(mb.getNetAcc() * -1);
                                    bt.setDatePaid(selected.getDateCollected());
                                    bt.setBillId(mb);
                                    bt.setNaration(mb.getNarration());
                                    bt.setReceiptNo(Integer.toString(selected.getReceiptNo()));
                                    getFacade().storeBillTransaction(bt);
                                    switch (bt.getNaration()) {
                                        case "shares": {
                                            AppConstants.logger("setting cr for shares" + mb.getNetAcc() * -1);
                                            SaccoLedgerEntries sle = new SaccoLedgerEntries();
                                            sle.setCr(mb.getNetAcc() * -1);
                                            sle.setDateWhen(selected.getDateCollected());
                                            sle.setDr(0);
                                            sle.setParticulars("Shares payment");
                                            sle.setLedgerId(new SaccoLedgers(1));
                                            getFacade().saveLedgerEntry(sle);
                                            AppConstants.logger("setting dr for cash" + mb.getNetAcc() * -1);
                                            sle = new SaccoLedgerEntries();
                                            sle.setCr(0);
                                            sle.setParticulars("Shares payment");
                                            sle.setDateWhen(selected.getDateCollected());
                                            sle.setDr(mb.getNetAcc() * -1);
                                            sle.setLedgerId(new SaccoLedgers(6));
                                            getFacade().saveLedgerEntry(sle);
                                            break;
                                        }
                                        case "registration": {
                                            AppConstants.logger("setting cr for registration" + mb.getNetAcc() * -1);
                                            SaccoLedgerEntries sle = new SaccoLedgerEntries();
                                            sle.setCr(mb.getNetAcc() * -1);
                                            sle.setDateWhen(selected.getDateCollected());
                                            sle.setDr(0);
                                            sle.setParticulars("Registration payment");
                                            sle.setLedgerId(new SaccoLedgers(18));
                                            getFacade().saveLedgerEntry(sle);
                                            AppConstants.logger("setting dr for cash" + mb.getNetAcc() * -1);
                                            sle = new SaccoLedgerEntries();
                                            sle.setCr(0);
                                            sle.setParticulars("Registration payment");
                                            sle.setDateWhen(selected.getDateCollected());
                                            sle.setDr(mb.getNetAcc() * -1);
                                            sle.setLedgerId(new SaccoLedgers(6));
                                            getFacade().saveLedgerEntry(sle);
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if (remaining > 0) {
                    AppConstants.logger("storing deposit");
                    DepositCash depo = new DepositCash();
                    if (selected.getCollectionstype().getId() == 4) {
                        depo.setIsWithdrawable(true);
                        depo.setDescription("deposit withdrawable");
                    } else {
                        depo.setDescription("deposit");
                    }
                    selected.setRandomInt(-1);
                    getFacade().edit(selected);
                    depo.setAmount(remaining);
                    depo.setDate(selected.getDateCollected());
                    depo.setMemberNo(selected.getMemberId());
                    depo.setTransactedBy("derer");
                    getFacade().persistDeposit(depo);
                    SaccoLedgerEntries sle = new SaccoLedgerEntries();
                    sle.setCr(remaining);
                    sle.setDateWhen(selected.getDateCollected());
                    sle.setDr(0);
                    sle.setParticulars("members deposit");
                    sle.setLedgerId(new SaccoLedgers(8));
                    getFacade().saveLedgerEntry(sle);
                    sle = new SaccoLedgerEntries();
                    sle.setCr(0);
                    sle.setParticulars("members deposit");
                    sle.setDateWhen(selected.getDateCollected());
                    sle.setDr(remaining);
                    sle.setLedgerId(new SaccoLedgers(6));
                    getFacade().saveLedgerEntry(sle);
                }
            } else if (selected.getCollectionstype().getId() != 1 && selected.getCollectionstype().getId() != 4 && selected.getAmount() > 0) {
                int toDistribute = selected.getAmount();
                if (selected.getCollectionstype().getId() == 2) {
                    int unpaidInterest = selected.getMemberId().getEstimatedUnPaidInterestEmergency();
                    int unpaidNegotiation = selected.getMemberId().getEstimatedUnPaidNegotiationEmergency();
                    if (unpaidInterest > 0) {
                        if (toDistribute > unpaidInterest) {
                            SaccoLedgerEntries sle = new SaccoLedgerEntries();
                            sle.setParticulars("interest on loans");
                            sle.setCr(unpaidInterest);
                            sle.setDateWhen(new Date());
                            sle.setDr(0);
                            sle.setIsGeneral(false);
                            sle.setStatusBilled(false);
                            sle.setLedgerId(new SaccoLedgers(30));
                            getFacade().saveLedgerEntry(sle);
                            sle = new SaccoLedgerEntries();
                            sle.setCr(0);
                            sle.setIsGeneral(false);
                            sle.setParticulars("interest on loans");
                            sle.setDateWhen(new Date());
                            sle.setStatusBilled(false);
                            sle.setDr(unpaidInterest);
                            sle.setLedgerId(new SaccoLedgers(6));
                            getFacade().saveLedgerEntry(sle);
                            toDistribute = toDistribute - unpaidInterest;
                        } else {
                            SaccoLedgerEntries sle = new SaccoLedgerEntries();
                            sle.setParticulars("interest on loans");
                            sle.setCr(toDistribute);
                            sle.setDateWhen(new Date());
                            sle.setDr(0);
                            sle.setIsGeneral(false);
                            sle.setStatusBilled(false);
                            sle.setLedgerId(new SaccoLedgers(30));
                            getFacade().saveLedgerEntry(sle);
                            sle = new SaccoLedgerEntries();
                            sle.setCr(0);
                            sle.setParticulars("interest on loans");
                            sle.setDateWhen(new Date());
                            sle.setStatusBilled(false);
                            sle.setIsGeneral(false);
                            sle.setDr(toDistribute);
                            sle.setLedgerId(new SaccoLedgers(6));
                            getFacade().saveLedgerEntry(sle);
                            toDistribute = 0;
                        }

                    }
                    if (unpaidNegotiation > 0 && toDistribute > 0) {
                        if (toDistribute > unpaidNegotiation) {
                            SaccoLedgerEntries sle = new SaccoLedgerEntries();
                            sle.setParticulars("loan negotiation");
                            sle.setCr(unpaidInterest);
                            sle.setDateWhen(new Date());
                            sle.setDr(0);
                            sle.setStatusBilled(false);
                            sle.setLedgerId(new SaccoLedgers(41));
                            getFacade().saveLedgerEntry(sle);
                            sle = new SaccoLedgerEntries();
                            sle.setCr(0);
                            sle.setParticulars("loan negotiation");
                            sle.setDateWhen(new Date());
                            sle.setStatusBilled(false);
                            sle.setDr(unpaidNegotiation);
                            sle.setLedgerId(new SaccoLedgers(6));
                            getFacade().saveLedgerEntry(sle);
                            toDistribute = toDistribute - unpaidNegotiation;
                        } else {
                            SaccoLedgerEntries sle = new SaccoLedgerEntries();
                            sle.setParticulars("loan negotiation");
                            sle.setCr(toDistribute);
                            sle.setDateWhen(new Date());
                            sle.setDr(0);
                            sle.setStatusBilled(false);
                            sle.setLedgerId(new SaccoLedgers(41));
                            getFacade().saveLedgerEntry(sle);
                            sle = new SaccoLedgerEntries();
                            sle.setCr(0);
                            sle.setParticulars("loan negotiation");
                            sle.setDateWhen(new Date());
                            sle.setStatusBilled(false);
                            sle.setDr(toDistribute);
                            sle.setLedgerId(new SaccoLedgers(6));
                            getFacade().saveLedgerEntry(sle);
                            toDistribute = 0;
                        }
                    }
                } else if (selected.getCollectionstype().getId() == 3) {
                    int unpaidInterest = selected.getMemberId().getEstimatedUnPaidInterestNormal();
                    int unpaidNegotiation = selected.getMemberId().getEstimatedUnPaidNegotiationNormal();
                    if (unpaidInterest > 0) {
                        if (toDistribute > unpaidInterest) {
                            SaccoLedgerEntries sle = new SaccoLedgerEntries();
                            sle.setParticulars("interest on loans");
                            sle.setCr(unpaidInterest);
                            sle.setDateWhen(new Date());
                            sle.setDr(0);
                            sle.setIsGeneral(false);
                            sle.setStatusBilled(false);
                            sle.setLedgerId(new SaccoLedgers(30));
                            getFacade().saveLedgerEntry(sle);
                            sle = new SaccoLedgerEntries();
                            sle.setCr(0);
                            sle.setParticulars("interest on loans");
                            sle.setDateWhen(new Date());
                            sle.setStatusBilled(false);
                            sle.setIsGeneral(false);
                            sle.setDr(unpaidInterest);
                            sle.setLedgerId(new SaccoLedgers(6));
                            getFacade().saveLedgerEntry(sle);
                            toDistribute = toDistribute - unpaidInterest;
                        } else {
                            SaccoLedgerEntries sle = new SaccoLedgerEntries();
                            sle.setParticulars("interest on loans");
                            sle.setCr(toDistribute);
                            sle.setDateWhen(new Date());
                            sle.setDr(0);
                            sle.setIsGeneral(false);
                            sle.setStatusBilled(false);
                            sle.setLedgerId(new SaccoLedgers(30));
                            getFacade().saveLedgerEntry(sle);
                            sle = new SaccoLedgerEntries();
                            sle.setCr(0);
                            sle.setParticulars("interest on loans");
                            sle.setDateWhen(new Date());
                            sle.setStatusBilled(false);
                            sle.setDr(toDistribute);
                            sle.setIsGeneral(false);
                            sle.setLedgerId(new SaccoLedgers(6));
                            getFacade().saveLedgerEntry(sle);
                            toDistribute = 0;
                        }

                    }
                    if (unpaidNegotiation > 0 && toDistribute > 0) {
                        if (toDistribute > unpaidNegotiation) {
                            SaccoLedgerEntries sle = new SaccoLedgerEntries();
                            sle.setParticulars("loan negotiation");
                            sle.setCr(unpaidInterest);
                            sle.setDateWhen(new Date());
                            sle.setDr(0);
                            sle.setIsGeneral(false);
                            sle.setStatusBilled(false);
                            sle.setLedgerId(new SaccoLedgers(41));
                            getFacade().saveLedgerEntry(sle);
                            sle = new SaccoLedgerEntries();
                            sle.setCr(0);
                            sle.setIsGeneral(false);
                            sle.setParticulars("loan negotiation");
                            sle.setDateWhen(new Date());
                            sle.setStatusBilled(false);
                            sle.setDr(unpaidNegotiation);
                            sle.setLedgerId(new SaccoLedgers(6));
                            getFacade().saveLedgerEntry(sle);
                            toDistribute = toDistribute - unpaidNegotiation;
                        } else {
                            SaccoLedgerEntries sle = new SaccoLedgerEntries();
                            sle.setParticulars("interest on loans");
                            sle.setCr(toDistribute);
                            sle.setDateWhen(new Date());
                            sle.setDr(0);
                            sle.setIsGeneral(false);
                            sle.setStatusBilled(false);
                            sle.setLedgerId(new SaccoLedgers(41));
                            getFacade().saveLedgerEntry(sle);
                            sle = new SaccoLedgerEntries();
                            sle.setCr(0);
                            sle.setIsGeneral(false);
                            sle.setParticulars("interest on loans");
                            sle.setDateWhen(new Date());
                            sle.setStatusBilled(false);
                            sle.setDr(toDistribute);
                            sle.setLedgerId(new SaccoLedgers(6));
                            getFacade().saveLedgerEntry(sle);
                            toDistribute = 0;
                        }
                    }
                }
                //+++++
                if (toDistribute > 0) {
                    SaccoLedgerEntries sle = new SaccoLedgerEntries();
                    sle.setCr(0);
                    sle.setIsGeneral(false);
                    sle.setDateWhen(selected.getDateCollected());
                    sle.setDr(toDistribute);
                    sle.setLedgerId(new SaccoLedgers(6));
                    sle.setParticulars("loan collections");
                    getFacade().saveLedgerEntry(sle);
                    sle = new SaccoLedgerEntries();
                    sle.setParticulars("loan collections");
                    sle.setCr(toDistribute);
                    sle.setDateWhen(selected.getDateCollected());
                    sle.setDr(0);
                    sle.setIsGeneral(false);
                    sle.setLedgerId(new SaccoLedgers(22));
                    getFacade().saveLedgerEntry(sle);
                }
            }
            if (remainingafter > 0) {
                selected.setCollectionstype(new CollectionTypes(4));
                selected.setId(null);
                selected.setAmount(remainingafter);
                selected.setRandomInt(-1);
                getFacade().create(selected);
                DepositCash depo = new DepositCash();
                depo.setDescription("deposit");
                depo.setDescription("withdrawable");
                depo.setAmount(remainingafter);
                depo.setDate(selected.getDateCollected());
                depo.setIsWithdrawable(true);
                depo.setMemberNo(selected.getMemberId());
                depo.setTransactedBy("derer");
                getFacade().persistDeposit(depo);
                SaccoLedgerEntries sle = new SaccoLedgerEntries();
                sle.setCr(remainingafter);
                sle.setDateWhen(selected.getDateCollected());
                sle.setDr(0);
                sle.setIsGeneral(false);
                sle.setParticulars("members deposit");
                sle.setLedgerId(new SaccoLedgers(8));
                getFacade().saveLedgerEntry(sle);
                sle = new SaccoLedgerEntries();
                sle.setCr(0);
                sle.setIsGeneral(false);
                sle.setParticulars("members deposit");
                sle.setDateWhen(selected.getDateCollected());
                sle.setDr(remainingafter);
                sle.setLedgerId(new SaccoLedgers(6));
                getFacade().saveLedgerEntry(sle);
            }
            items = null;    // Invalidate list of items to trigger re-query.
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("CollectionsCreated"));
        }
    }

    public void create(Collections c) {
        c.setId(null);
        int random = new Random().nextInt(10000000);
        c.setRandomInt(random);
        //c.setCollectionstype(cl);
        //c.setDateCollected(DateEntry);
        AppConstants.logger("creating a copy of collection");
        int remainingafter = 0;
        if ((c.getCollectionstype().getId() == 1 || c.getCollectionstype().getId() == 4) && c.getAmount() > 0) {
            c.setStatusBilled(true);
        }
        //if its for emergency loan and the loan balance is 0, we make it a deposit
        if (c.getCollectionstype().getId() == 2) {
            if (c.getMemberId().getActiveLoanEmergency() != null) {
                if (c.getMemberId().getActiveLoanEmergency().getBalEst() <= 0) {
                    //when the receipt is for emergency loan and the member has a normal loan no emergency
                    if (!c.getMemberId().hasPendingEmergency()) {
                        c.setStatusBilled(true);
                        c.setCollectionstype(new CollectionTypes(4));
                    } else if (c.getMemberId().hasPendingEmergency()) {
                        AppConstants.logger("amount received:" + c.getAmount());

                        //member has a pending loan
                        //calculate amount paid but not billed
                        int emergencyCollectionsTotal = 0;
                        for (Collections col : getFacade().getMemberUnbilledCollections(c.getMemberId())) {
                            if (col.getCollectionstype().getId() == 2) {
                                emergencyCollectionsTotal += col.getAmount();
                            }
                        }
                        AppConstants.logger("total amount paid for emergency loan:" + emergencyCollectionsTotal);
                        int amountForOldLoan = (int) (c.getMemberId().getActiveLoanEmergency().getCurrentSchedule().getTotalDebits() + c.getMemberId().getActiveLoanEmergency().getCurrentSchedule().getBalance());
                        AppConstants.logger("Amount to clear the active loan" + amountForOldLoan);
                        int excess = emergencyCollectionsTotal - amountForOldLoan;
                        AppConstants.logger("amount already paid extra for the new loan is:" + excess);
                        //test whether the amount is exceeding the pending loan bal
                        int balForNewLoan = c.getMemberId().getBalanceForPendingLoanEmergency();
                        AppConstants.logger("new loan bal" + balForNewLoan);
                        int amountRemainingForthenewLoan = balForNewLoan - excess;
                        AppConstants.logger("outstanding loan bal:" + amountRemainingForthenewLoan);
                        if (amountRemainingForthenewLoan < c.getAmount()) {
                            remainingafter = c.getAmount() - amountRemainingForthenewLoan;
                            c.setAmount(amountRemainingForthenewLoan);
                            AppConstants.logger("amount sent to withdrawable:" + remainingafter);
                        }
                        int finalbal = c.getAmount() - amountRemainingForthenewLoan;
                        AppConstants.logger("final outstanding=:" + finalbal);
                    }
                } else if (c.getMemberId().getActiveLoanEmergency().getBalEst() <= c.getAmount()) {
                    remainingafter = c.getAmount() - c.getMemberId().getActiveLoanEmergency().getBalEst();
                    c.setAmount(c.getMemberId().getActiveLoanEmergency().getBalEst());
                }

            } else if (c.getMemberId().getActiveLoanNormal() != null) {
                if (c.getMemberId().getActiveLoanNormal().getBalEst() > 0) {
                    if (c.getMemberId().getActiveLoanNormal().getBalEst() <= c.getAmount()) {
                        remainingafter = c.getAmount() - c.getMemberId().getActiveLoanNormal().getBalEst();
                        c.setAmount(c.getMemberId().getActiveLoanNormal().getBalEst());
                    }
                    c.setCollectionstype(new CollectionTypes(3));
                } else if (c.getMemberId().hasPendingNormal()) {
                    c.setCollectionstype(new CollectionTypes(3));
                }
            } else {
                c.setStatusBilled(true);
                c.setCollectionstype(new CollectionTypes(4));
            }
        }
        //if its for normal loan and the loan balance is 0, we make it a deposit
        if (c.getCollectionstype().getId() == 3) {
            if (c.getMemberId().getActiveLoanNormal() != null) {
                if (c.getMemberId().getActiveLoanNormal().getBalEst() <= 0) {
                    if (!c.getMemberId().hasPendingNormal()) {
                        c.setStatusBilled(true);
                        c.setCollectionstype(new CollectionTypes(4));
                    } else if (c.getMemberId().hasPendingNormal()) {
                        AppConstants.logger("amount received:" + c.getAmount());

                        //member has a pending loan
                        //calculate amount paid but not billed
                        int normalCollectionsTotal = 0;
                        for (Collections col : getFacade().getMemberUnbilledCollections(c.getMemberId())) {
                            if (col.getCollectionstype().getId() == 3) {
                                normalCollectionsTotal += col.getAmount();
                            }
                        }
                        AppConstants.logger("total amount paid for normal loan:" + normalCollectionsTotal);
                        int amountForOldLoan = (int) (c.getMemberId().getActiveLoanNormal().getCurrentSchedule().getTotalDebits() + c.getMemberId().getActiveLoanNormal().getCurrentSchedule().getBalance());
                        AppConstants.logger("Amount to clear the active loan" + amountForOldLoan);
                        int excess = normalCollectionsTotal - amountForOldLoan;
                        AppConstants.logger("amount already paid extra for the new loan is:" + excess);
                        //test whether the amount is exceeding the pending loan bal
                        int balForNewLoan = c.getMemberId().getBalanceForPendingLoanNormal();
                        AppConstants.logger("new loan bal" + balForNewLoan);
                        int amountRemainingForthenewLoan = balForNewLoan - excess;
                        AppConstants.logger("outstanding loan bal:" + amountRemainingForthenewLoan);
                        if (amountRemainingForthenewLoan < c.getAmount()) {
                            remainingafter = c.getAmount() - amountRemainingForthenewLoan;
                            c.setAmount(amountRemainingForthenewLoan);
                            AppConstants.logger("amount sent to withdrawable:" + remainingafter);
                        }
                        int finalbal = c.getAmount() - amountRemainingForthenewLoan;
                        AppConstants.logger("final outstanding=:" + finalbal);
                    }
                } else if (c.getMemberId().getActiveLoanNormal().getBalEst() <= c.getAmount() && (!c.getMemberId().hasPendingNormal())) {
                    remainingafter = c.getAmount() - c.getMemberId().getActiveLoanNormal().getBalEst();
                    c.setAmount(c.getMemberId().getActiveLoanNormal().getBalEst());
                }
            } else if (c.getMemberId().getActiveLoanEmergency() != null) {
                if (c.getMemberId().getActiveLoanEmergency().getBalEst() > 0) {
                    if (c.getMemberId().getActiveLoanEmergency().getBalEst() <= c.getAmount()) {
                        remainingafter = c.getAmount() - c.getMemberId().getActiveLoanEmergency().getBalEst();
                        c.setAmount(c.getMemberId().getActiveLoanEmergency().getBalEst());
                    }
                    c.setCollectionstype(new CollectionTypes(2));
                } else if (c.getMemberId().hasPendingEmergency()) {
                    c.setCollectionstype(new CollectionTypes(2));
                }
            } else {
                c.setStatusBilled(true);
                c.setCollectionstype(new CollectionTypes(4));
            }
        }
        AppConstants.logger(c.getCollectionstype().getType());
        selected = c;
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("CollectionsCreated"));
        selected = null;
        c = getFacade().getByRandom(c.getRandomInt());
        c.setRandomInt(-1);
        getFacade().edit(c);
        if (!JsfUtil.isValidationFailed()) {
            if ((c.getCollectionstype().getId() == 1 || c.getCollectionstype().getId() == 4) && c.getAmount() > 0) {
                int remaining = c.getAmount();
                if (c.getMemberId().getBillsTotalArrears() < 0) {
                    for (MemberBills mb : c.getMemberId().getMemberBillsCollection()) {
                        if (remaining > 0) {
                            if (mb.getNetAcc() < 0) {
                                if (remaining == mb.getNetAcc() * -1 || remaining < mb.getNetAcc() * -1) {
                                    BillTransactions bt = new BillTransactions();
                                    bt.setAmountCredit(remaining);
                                    bt.setDatePaid(c.getDateCollected());
                                    bt.setBillId(mb);
                                    bt.setNaration(mb.getNarration());
                                    bt.setReceiptNo(Integer.toString(c.getReceiptNo()));
                                    getFacade().storeBillTransaction(bt);
                                    switch (bt.getNaration()) {
                                        case "shares": {
                                            AppConstants.logger("setting cr for shares" + remaining);
                                            SaccoLedgerEntries sle = new SaccoLedgerEntries();
                                            sle.setCr(remaining);
                                            sle.setDateWhen(c.getDateCollected());
                                            sle.setDr(0);
                                            sle.setParticulars("Shares payment");
                                            sle.setLedgerId(new SaccoLedgers(1));
                                            getFacade().saveLedgerEntry(sle);
                                            sle = new SaccoLedgerEntries();
                                            sle.setCr(0);
                                            sle.setParticulars("Shares payment");
                                            sle.setDateWhen(c.getDateCollected());
                                            sle.setDr(remaining);
                                            sle.setLedgerId(new SaccoLedgers(6));
                                            getFacade().saveLedgerEntry(sle);
                                            break;
                                        }
                                        case "registration": {
                                            AppConstants.logger("setting cr for registration" + remaining);
                                            SaccoLedgerEntries sle = new SaccoLedgerEntries();
                                            sle.setCr(remaining);
                                            sle.setDateWhen(c.getDateCollected());
                                            sle.setDr(0);
                                            sle.setParticulars("Registration payment");
                                            sle.setLedgerId(new SaccoLedgers(18));
                                            getFacade().saveLedgerEntry(sle);
                                            sle = new SaccoLedgerEntries();
                                            sle.setCr(0);
                                            sle.setDateWhen(c.getDateCollected());
                                            sle.setDr(remaining);
                                            sle.setParticulars("Registration payment");
                                            sle.setLedgerId(new SaccoLedgers(6));
                                            getFacade().saveLedgerEntry(sle);
                                            break;
                                        }
                                    }
                                    remaining = 0;
                                    break;
                                } else {
                                    remaining = remaining - mb.getNetAcc() * -1;
                                    BillTransactions bt = new BillTransactions();
                                    bt.setAmountCredit(mb.getNetAcc() * -1);
                                    bt.setDatePaid(c.getDateCollected());
                                    bt.setBillId(mb);
                                    bt.setNaration(mb.getNarration());
                                    bt.setReceiptNo(Integer.toString(c.getReceiptNo()));
                                    getFacade().storeBillTransaction(bt);
                                    switch (bt.getNaration()) {
                                        case "shares": {
                                            AppConstants.logger("setting cr for shares" + mb.getNetAcc() * -1);
                                            SaccoLedgerEntries sle = new SaccoLedgerEntries();
                                            sle.setCr(mb.getNetAcc() * -1);
                                            sle.setDateWhen(c.getDateCollected());
                                            sle.setDr(0);
                                            sle.setParticulars("Shares payment");
                                            sle.setLedgerId(new SaccoLedgers(1));
                                            getFacade().saveLedgerEntry(sle);
                                            AppConstants.logger("setting dr for cash" + mb.getNetAcc() * -1);
                                            sle = new SaccoLedgerEntries();
                                            sle.setCr(0);
                                            sle.setParticulars("Shares payment");
                                            sle.setDateWhen(c.getDateCollected());
                                            sle.setDr(mb.getNetAcc() * -1);
                                            sle.setLedgerId(new SaccoLedgers(6));
                                            getFacade().saveLedgerEntry(sle);
                                            break;
                                        }
                                        case "registration": {
                                            AppConstants.logger("setting cr for registration" + mb.getNetAcc() * -1);
                                            SaccoLedgerEntries sle = new SaccoLedgerEntries();
                                            sle.setCr(mb.getNetAcc() * -1);
                                            sle.setDateWhen(c.getDateCollected());
                                            sle.setDr(0);
                                            sle.setParticulars("Registration payment");
                                            sle.setLedgerId(new SaccoLedgers(18));
                                            getFacade().saveLedgerEntry(sle);
                                            AppConstants.logger("setting dr for cash" + mb.getNetAcc() * -1);
                                            sle = new SaccoLedgerEntries();
                                            sle.setCr(0);
                                            sle.setParticulars("Registration payment");
                                            sle.setDateWhen(c.getDateCollected());
                                            sle.setDr(mb.getNetAcc() * -1);
                                            sle.setLedgerId(new SaccoLedgers(6));
                                            getFacade().saveLedgerEntry(sle);
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if (remaining > 0) {
                    AppConstants.logger("storing deposit");
                    DepositCash depo = new DepositCash();
                    depo.setDescription("deposit");
                    if (c.getCollectionstype().getId() == 4) {
                        depo.setIsWithdrawable(true);
                        depo.setDescription("withdrawable");
                    }
                    c.setRandomInt(-1);
                    getFacade().edit(c);
                    depo.setAmount(remaining);
                    depo.setDate(c.getDateCollected());
                    depo.setMemberNo(c.getMemberId());
                    depo.setTransactedBy("derer");
                    getFacade().persistDeposit(depo);
                    SaccoLedgerEntries sle = new SaccoLedgerEntries();
                    sle.setCr(remaining);
                    sle.setDateWhen(c.getDateCollected());
                    sle.setDr(0);
                    sle.setParticulars("members deposit");
                    sle.setLedgerId(new SaccoLedgers(8));
                    getFacade().saveLedgerEntry(sle);
                    sle = new SaccoLedgerEntries();
                    sle.setCr(0);
                    sle.setParticulars("members deposit");
                    sle.setDateWhen(c.getDateCollected());
                    sle.setDr(remaining);
                    sle.setLedgerId(new SaccoLedgers(6));
                    getFacade().saveLedgerEntry(sle);
                }
            } else if (c.getCollectionstype().getId() != 1 && c.getCollectionstype().getId() != 4 && c.getAmount() > 0) {
                int toDistribute = c.getAmount();
                if (c.getCollectionstype().getId() == 2) {
                    int unpaidInterest = c.getMemberId().getEstimatedUnPaidInterestEmergency();
                    int unpaidNegotiation = c.getMemberId().getEstimatedUnPaidNegotiationEmergency();
                    if (unpaidInterest > 0) {
                        if (toDistribute > unpaidInterest) {
                            SaccoLedgerEntries sle = new SaccoLedgerEntries();
                            sle.setParticulars("interest on loans");
                            sle.setCr(unpaidInterest);
                            sle.setDateWhen(new Date());
                            sle.setDr(0);
                            sle.setIsGeneral(false);
                            sle.setStatusBilled(false);
                            sle.setLedgerId(new SaccoLedgers(30));
                            getFacade().saveLedgerEntry(sle);
                            sle = new SaccoLedgerEntries();
                            sle.setCr(0);
                            sle.setIsGeneral(false);
                            sle.setParticulars("interest on loans");
                            sle.setDateWhen(new Date());
                            sle.setStatusBilled(false);
                            sle.setDr(unpaidInterest);
                            sle.setLedgerId(new SaccoLedgers(6));
                            getFacade().saveLedgerEntry(sle);
                            toDistribute = toDistribute - unpaidInterest;
                        } else {
                            SaccoLedgerEntries sle = new SaccoLedgerEntries();
                            sle.setParticulars("interest on loans");
                            sle.setCr(toDistribute);
                            sle.setDateWhen(new Date());
                            sle.setDr(0);
                            sle.setIsGeneral(false);
                            sle.setStatusBilled(false);
                            sle.setLedgerId(new SaccoLedgers(30));
                            getFacade().saveLedgerEntry(sle);
                            sle = new SaccoLedgerEntries();
                            sle.setCr(0);
                            sle.setParticulars("interest on loans");
                            sle.setDateWhen(new Date());
                            sle.setStatusBilled(false);
                            sle.setIsGeneral(false);
                            sle.setDr(toDistribute);
                            sle.setLedgerId(new SaccoLedgers(6));
                            getFacade().saveLedgerEntry(sle);
                            toDistribute = 0;
                        }

                    }
                    if (unpaidNegotiation > 0 && toDistribute > 0) {
                        if (toDistribute > unpaidNegotiation) {
                            SaccoLedgerEntries sle = new SaccoLedgerEntries();
                            sle.setParticulars("loan negotiation");
                            sle.setCr(unpaidInterest);
                            sle.setDateWhen(new Date());
                            sle.setDr(0);
                            sle.setStatusBilled(false);
                            sle.setLedgerId(new SaccoLedgers(41));
                            getFacade().saveLedgerEntry(sle);
                            sle = new SaccoLedgerEntries();
                            sle.setCr(0);
                            sle.setParticulars("loan negotiation");
                            sle.setDateWhen(new Date());
                            sle.setStatusBilled(false);
                            sle.setDr(unpaidNegotiation);
                            sle.setLedgerId(new SaccoLedgers(6));
                            getFacade().saveLedgerEntry(sle);
                            toDistribute = toDistribute - unpaidNegotiation;
                        } else {
                            SaccoLedgerEntries sle = new SaccoLedgerEntries();
                            sle.setParticulars("loan negotiation");
                            sle.setCr(toDistribute);
                            sle.setDateWhen(new Date());
                            sle.setDr(0);
                            sle.setStatusBilled(false);
                            sle.setLedgerId(new SaccoLedgers(41));
                            getFacade().saveLedgerEntry(sle);
                            sle = new SaccoLedgerEntries();
                            sle.setCr(0);
                            sle.setParticulars("loan negotiation");
                            sle.setDateWhen(new Date());
                            sle.setStatusBilled(false);
                            sle.setDr(toDistribute);
                            sle.setLedgerId(new SaccoLedgers(6));
                            getFacade().saveLedgerEntry(sle);
                            toDistribute = 0;
                        }
                    }
                } else if (c.getCollectionstype().getId() == 3) {
                    int unpaidInterest = c.getMemberId().getEstimatedUnPaidInterestNormal();
                    int unpaidNegotiation = c.getMemberId().getEstimatedUnPaidNegotiationNormal();
                    if (unpaidInterest > 0) {
                        if (toDistribute > unpaidInterest) {
                            SaccoLedgerEntries sle = new SaccoLedgerEntries();
                            sle.setParticulars("interest on loans");
                            sle.setCr(unpaidInterest);
                            sle.setDateWhen(new Date());
                            sle.setDr(0);
                            sle.setIsGeneral(false);
                            sle.setStatusBilled(false);
                            sle.setLedgerId(new SaccoLedgers(30));
                            getFacade().saveLedgerEntry(sle);
                            sle = new SaccoLedgerEntries();
                            sle.setCr(0);
                            sle.setParticulars("interest on loans");
                            sle.setDateWhen(new Date());
                            sle.setStatusBilled(false);
                            sle.setIsGeneral(false);
                            sle.setDr(unpaidInterest);
                            sle.setLedgerId(new SaccoLedgers(6));
                            getFacade().saveLedgerEntry(sle);
                            toDistribute = toDistribute - unpaidInterest;
                        } else {
                            SaccoLedgerEntries sle = new SaccoLedgerEntries();
                            sle.setParticulars("interest on loans");
                            sle.setCr(toDistribute);
                            sle.setDateWhen(new Date());
                            sle.setDr(0);
                            sle.setIsGeneral(false);
                            sle.setStatusBilled(false);
                            sle.setLedgerId(new SaccoLedgers(30));
                            getFacade().saveLedgerEntry(sle);
                            sle = new SaccoLedgerEntries();
                            sle.setCr(0);
                            sle.setParticulars("interest on loans");
                            sle.setDateWhen(new Date());
                            sle.setStatusBilled(false);
                            sle.setDr(toDistribute);
                            sle.setIsGeneral(false);
                            sle.setLedgerId(new SaccoLedgers(6));
                            getFacade().saveLedgerEntry(sle);
                            toDistribute = 0;
                        }

                    }
                    if (unpaidNegotiation > 0 && toDistribute > 0) {
                        if (toDistribute > unpaidNegotiation) {
                            SaccoLedgerEntries sle = new SaccoLedgerEntries();
                            sle.setParticulars("loan negotiation");
                            sle.setCr(unpaidInterest);
                            sle.setDateWhen(new Date());
                            sle.setDr(0);
                            sle.setIsGeneral(false);
                            sle.setStatusBilled(false);
                            sle.setLedgerId(new SaccoLedgers(41));
                            getFacade().saveLedgerEntry(sle);
                            sle = new SaccoLedgerEntries();
                            sle.setCr(0);
                            sle.setIsGeneral(false);
                            sle.setParticulars("loan negotiation");
                            sle.setDateWhen(new Date());
                            sle.setStatusBilled(false);
                            sle.setDr(unpaidNegotiation);
                            sle.setLedgerId(new SaccoLedgers(6));
                            getFacade().saveLedgerEntry(sle);
                            toDistribute = toDistribute - unpaidNegotiation;
                        } else {
                            SaccoLedgerEntries sle = new SaccoLedgerEntries();
                            sle.setParticulars("interest on loans");
                            sle.setCr(toDistribute);
                            sle.setDateWhen(new Date());
                            sle.setDr(0);
                            sle.setIsGeneral(false);
                            sle.setStatusBilled(false);
                            sle.setLedgerId(new SaccoLedgers(41));
                            getFacade().saveLedgerEntry(sle);
                            sle = new SaccoLedgerEntries();
                            sle.setCr(0);
                            sle.setIsGeneral(false);
                            sle.setParticulars("interest on loans");
                            sle.setDateWhen(new Date());
                            sle.setStatusBilled(false);
                            sle.setDr(toDistribute);
                            sle.setLedgerId(new SaccoLedgers(6));
                            getFacade().saveLedgerEntry(sle);
                            toDistribute = 0;
                        }
                    }
                }
                //+++++
                if (toDistribute > 0) {
                    SaccoLedgerEntries sle = new SaccoLedgerEntries();
                    sle.setCr(0);
                    sle.setIsGeneral(false);
                    sle.setDateWhen(c.getDateCollected());
                    sle.setDr(toDistribute);
                    sle.setLedgerId(new SaccoLedgers(6));
                    sle.setParticulars("loan collections");
                    getFacade().saveLedgerEntry(sle);
                    sle = new SaccoLedgerEntries();
                    sle.setParticulars("loan collections");
                    sle.setCr(toDistribute);
                    sle.setDateWhen(c.getDateCollected());
                    sle.setDr(0);
                    sle.setIsGeneral(false);
                    sle.setLedgerId(new SaccoLedgers(22));
                    getFacade().saveLedgerEntry(sle);
                }
            }
            if (remainingafter > 0) {
                if (c.getMemberId().getPriorityAccount().getId() == 1) {
                    c.setCollectionstype(new CollectionTypes(1));
                } else {
                    c.setCollectionstype(new CollectionTypes(4));
                }
                c.setId(null);
                c.setAmount(remainingafter);
                c.setRandomInt(-1);
                getFacade().create(c);
                DepositCash depo = new DepositCash();
                if (c.getCollectionstype().getId() == 4) {
                    depo.setIsWithdrawable(true);
                }
                depo.setAmount(remainingafter);
                depo.setDate(c.getDateCollected());
                depo.setDescription("depositwithdrwble");
                depo.setMemberNo(c.getMemberId());
                depo.setTransactedBy("derer");
                depo.setIsWithdrawable(true);
                getFacade().persistDeposit(depo);
                SaccoLedgerEntries sle = new SaccoLedgerEntries();
                sle.setCr(remainingafter);
                sle.setDateWhen(c.getDateCollected());
                sle.setDr(0);
                sle.setIsGeneral(false);
                sle.setParticulars("members deposit");
                sle.setLedgerId(new SaccoLedgers(8));
                getFacade().saveLedgerEntry(sle);
                sle = new SaccoLedgerEntries();
                sle.setCr(0);
                sle.setIsGeneral(false);
                sle.setParticulars("members deposit");
                sle.setDateWhen(c.getDateCollected());
                sle.setDr(remainingafter);
                sle.setLedgerId(new SaccoLedgers(6));
                getFacade().saveLedgerEntry(sle);
            }
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("CollectionsUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("CollectionsDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<Collections> getItems() {
        if (items == null) {
            items = getFacade().findAllItems();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    if (selected.getAmount() > 0) {
                        getFacade().edit(selected);
                    }
                } else {
                    getFacade().remove(selected);
                }
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public Collections getCollections(java.lang.Integer id) {
        return getFacade().find(id);
    }

    public List<Collections> getItemsAvailableSelectMany() {
        return getFacade().findAllItems();
    }

    public List<Collections> getItemsAvailableSelectOne() {
        return getFacade().findAllItems();

    }

    @FacesConverter(forClass = Collections.class)
    public static class CollectionsControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            CollectionsController controller = (CollectionsController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "collectionsController");
            return controller.getCollections(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Collections) {
                Collections o = (Collections) object;
                return getStringKey(o.getId());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Collections.class.getName()});
                return null;
            }
        }
    }

    public void onRowEdit(CellEditEvent event) {
        Object oldValue = event.getOldValue();
        Object newValue = event.getNewValue();

        if (newValue != null && !newValue.equals(oldValue)) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Cell Changed", "Old: " + oldValue + ", New:" + newValue);
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

    public void onRowCancel(RowEditEvent event) {
        AppConstants.logger("yeiuyuieyiru");
        FacesContext.getCurrentInstance().addMessage(
                null,
                new FacesMessage(FacesMessage.SEVERITY_FATAL,
                        "No record have been saved", null));
    }

    public int getDummyTotal() {
        int total = 0;
        if (dummy != null) {
            for (Collections c : dummy) {
                total += c.getAmount();
                AppConstants.logger("Total" + total);
            }
        }
        return total;
    }

    public int getDummyTotalSavings() {
        int total = 0;
        if (dummy != null) {
            for (Collections c : dummy) {
                try {
                    if (c.getCollectionstype().getId() == 1 || c.getCollectionstype().getId() == 4) {
                        total += c.getAmount();
                    }
                } catch (NullPointerException e) {

                }
            }
        }
        return total;
    }

    public int getDummyTotalEmergency() {
        int total = 0;
        if (dummy != null) {
            for (Collections c : dummy) {
                try {
                    if (c.getCollectionstype().getId() == 2) {
                        total += c.getAmount();
                    }
                } catch (NullPointerException e) {

                }
            }
        }
        return total;
    }

    public int getDummyTotalNormal() {
        int total = 0;
        if (dummy != null) {
            for (Collections c : dummy) {
                try {
                    if (c.getCollectionstype().getId() == 3) {
                        total += c.getAmount();
                    }
                } catch (NullPointerException e) {

                }
            }
        }
        return total;
    }

    public void test() {
        boolean isSkipped = false;
        String message;
        FacesMessage.Severity s;
        if (dummy != null) {
            for (Collections c : dummy) {
                try {
                    AppConstants.logger("Amount:" + c.getAmount() + "Member:" + c.getMemberId().getSurname() + "Collection Type:" + c.getCollectionstype().getType() + "Date:" + c.getDateCollected());
                    if (c.getAmount() != 0) {
                        create(c);
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                    isSkipped = true;
                }
            }
            dummy.clear();
            dummyCount = 0;
            if (isSkipped) {
                message = "Some receipts have not been saved";
                s = FacesMessage.SEVERITY_FATAL;
                showMessage(message, s);
            } else {
                message = "All Receipts have been saved";
                s = FacesMessage.SEVERITY_INFO;
                showMessage(message, s);
            }
        }
    }

    public void showMessage(String message, FacesMessage.Severity s) {
        FacesContext.getCurrentInstance().addMessage(
                null,
                new FacesMessage(s,
                        message, null));
    }

    private static final List<String> EXTENSIONS_ALLOWED = new ArrayList<>();

    static {
        // json only
        EXTENSIONS_ALLOWED.add(".json");
    }

    private String getFileName(Part part) {
        String partHeader = part.getHeader("content-disposition");
        //logger.log(Level.INFO, "Part Header = {0}", partHeader);
        for (String cd : part.getHeader("content-disposition").split(";")) {
            if (cd.trim().startsWith("filename")) {
                return cd.substring(cd.indexOf('=') + 1).trim().replace("\"", "");
            }
        }
        return null;

    }

    /**
     * @return the filePart
     */
    public Part getFilePart() {
        return filePart;
    }

    /**
     * @param filePart the filePart to set
     */
    public void setFilePart(Part filePart) {
        this.filePart = filePart;
    }

    public void upload() {
        //logger.info(getFilePart().getName());
        final int PKG_SIZE = 1024;
        StringBuilder buffer = new StringBuilder(PKG_SIZE * 10);
        try {
            InputStream is = getFilePart().getInputStream();
            int read;
            byte[] b = new byte[PKG_SIZE];
            while ((read = is.read(b)) != -1) {
                String str = new String(b, 0, read);
                buffer.append(str);
            }
            //creating Gson instance to convert JSON array to Java array
            Gson converter = new Gson();

            AppConstants.logger("ready to convert");
            Type type = new TypeToken<List<Collection>>() {
            }.getType();
            List<Collection> list = converter.fromJson(buffer.toString(), type);
            dummy = new ArrayList<>();
            for (Collection listitem : list) {
                Collections c = new Collections();
                c.setAmount(listitem.getAmount());
                c.setCollectionstype(getFacade().findCollectionType(listitem.collectionstype));
                c.setMemberId(getFacade().findMember(listitem.getMemberId()));
                c.setReceiptNo(listitem.getReceiptNo());
                try {
                    c.setDateCollected(new SimpleDateFormat("yyyy-MM-dd").parse(listitem.getDatewhen().substring(0, 10)));

                } catch (ParseException ex) {
                    Logger.getLogger(CollectionsController.class
                            .getName()).log(Level.SEVERE, null, ex);
                }
                dummy.add(c);
                AppConstants.logger(listitem.toString());
            }
            //logger.log(Level.INFO, "Length : {0}", b.length);
            String fileName = getFileName(getFilePart());
            //logger.log(Level.INFO, "File name : {0}", fileName);

            // generate *unique* filename
            final String extension = fileName.substring(fileName.length() - 5);

            if (!EXTENSIONS_ALLOWED.contains(extension)) {
                //logger.severe("User tried to upload file that's not a json. Upload canceled.");
                JsfUtil.addErrorMessage(new Exception("Error trying to upload file"), "Error trying to upload file");
            }
        } catch (IOException | JsonSyntaxException ex) {
            System.err.println("error occured" + ex.getMessage());

        }

    }

    class Collection {

        private int receiptNo, amount, collectionstype, memberId;
        private String datewhen;

        public String getDatewhen() {
            return datewhen;
        }

        public void setDatewhen(String datewhen) {
            this.datewhen = datewhen;
        }

        public int getReceiptNo() {
            return receiptNo;
        }

        public void setReceiptNo(int receiptNo) {
            this.receiptNo = receiptNo;
        }

        public int getAmount() {
            return amount;
        }

        public void setAmount(int amount) {
            this.amount = amount;
        }

        public int getCollectionstype() {
            return collectionstype;
        }

        public void setCollectionstype(int collectionstype) {
            this.collectionstype = collectionstype;
        }

        public int getMemberId() {
            return memberId;
        }

        public void setMemberId(int memberId) {
            this.memberId = memberId;
        }

        @Override
        public String toString() {
            return "Collection{" + "receiptNo=" + receiptNo + ", amount=" + amount + ", collectionstype=" + collectionstype + ", memberId=" + memberId + '}';
        }

    }

}
