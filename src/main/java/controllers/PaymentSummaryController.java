package controllers;

import controllers.util.AppConstants;
import entities.PaymentSummary;
import controllers.util.JsfUtil;
import controllers.util.JsfUtil.PersistAction;
import entities.PaymentSummaryItems;
import facades.PaymentSummaryFacade;
import java.io.IOException;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Named("paymentSummaryController")
@SessionScoped
public class PaymentSummaryController implements Serializable {

    @EJB
    private facades.PaymentSummaryFacade ejbFacade;
    private List<PaymentSummary> items = null;
    private PaymentSummary selected;

    public PaymentSummaryController() {
    }

    public PaymentSummary getSelected() {
        return selected;
    }

    public void setSelected(PaymentSummary selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private PaymentSummaryFacade getFacade() {
        return ejbFacade;
    }

    public PaymentSummary prepareCreate() {
        selected = new PaymentSummary();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("PaymentSummaryCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("PaymentSummaryUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("PaymentSummaryDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<PaymentSummary> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public PaymentSummary getPaymentSummary(java.lang.Integer id) {
        return getFacade().find(id);
    }

    public List<PaymentSummary> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<PaymentSummary> getItemsAvailableSelectOne() {
        return getFacade().findAllOrderedById();
    }

    @FacesConverter(forClass = PaymentSummary.class)
    public static class PaymentSummaryControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            PaymentSummaryController controller = (PaymentSummaryController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "paymentSummaryController");
            return controller.getPaymentSummary(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof PaymentSummary) {
                PaymentSummary o = (PaymentSummary) object;
                return getStringKey(o.getId());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), PaymentSummary.class.getName()});
                return null;
            }
        }

    }
public void onPrintMonthlyReport() throws IOException, JRException {
        JasperPrint jasperPrint;
        AppConstants.logger("calling print");
        ArrayList<PaymentSummaryItems> finallist = new ArrayList<>();
        int x = 1;
        for (PaymentSummaryItems pt : selected.getPaymentSummaryItemsCollection()) {
            pt.setId(x);
            x++;
            finallist.add(pt);
        }
        JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(finallist);
        FacesContext facesContext = FacesContext.getCurrentInstance();
        String reportPath = facesContext.getExternalContext().getRealPath("/admin/report/monthlyreport.jasper");
        Map params = new HashMap();

        params.put("totalpaidShares", selected.getTotalShares());
	params.put("totaldepositedAmount", selected.getTotalDeposits());
	params.put("totalnormalLoanPaid", selected.getTotalLoanNormal());
	params.put("totalnormalLoanInterestPaid", selected.getTotalLoanNormalInterest());
	params.put("totalemergencyLoanPaid", selected.getTotalEmergency());
	params.put("totalemergencyLoanInterestPaid", selected.getTotalEmergencyInterest());
	params.put("totalwithdrawal", selected.getTotalWithDrawal());
	params.put("totalwithdrawalincome", selected.getTotalWithDrawalIncome());
	params.put("totalnegotiation", selected.getTotalNegotiationPaid());
	params.put("totalregistration", selected.getTotalRegistrationPaid());
        params.put("reportsdate", selected.getDatePreparedStrMonth());
        jasperPrint = JasperFillManager.fillReport(reportPath, params, beanCollectionDataSource);
        HttpServletResponse httpServletResponse = (HttpServletResponse) facesContext.getExternalContext().getResponse();
        httpServletResponse.addHeader("Content-disposition", "attachment; filename=" + "all" + ".pdf");
        try (ServletOutputStream servletOutputStream = httpServletResponse.getOutputStream()) {
            JasperExportManager.exportReportToPdfStream(jasperPrint, servletOutputStream);
            servletOutputStream.flush();
        }
        FacesContext.getCurrentInstance().responseComplete();
    }
}
