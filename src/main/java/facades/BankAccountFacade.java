
package facades;

import entities.BankAccount;
import entities.BankAccountTransaction;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class BankAccountFacade extends AbstractFacade<BankAccount> {

    @PersistenceContext(unitName = "SaccoManagement")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public BankAccountFacade() {
        super(BankAccount.class);
    }

    public BankAccount findByRandom(int rand) {
        return (BankAccount) getEntityManager().createQuery("SELECT b FROM BankAccount b WHERE b.randomInt =:r")
                .setParameter("r", rand).getSingleResult();
    }

    public void createTransaction(BankAccountTransaction bankAccountTransaction) {
        getEntityManager().persist(bankAccountTransaction);
    }

    public List<BankAccountTransaction> findTransactions(BankAccount account) {
        return getEntityManager().createQuery("SELECT b FROM BankAccountTransaction b WHERE b.account =:b")
                .setParameter("b", account).getResultList();
    }

}
