/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facades;

import entities.DebtorsAccountLedgersEntries;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author francis
 */
@Stateless
public class DebtorsAccountLedgersEntriesFacade extends AbstractFacade<DebtorsAccountLedgersEntries> {
    @PersistenceContext(unitName = "SaccoManagement")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public DebtorsAccountLedgersEntriesFacade() {
        super(DebtorsAccountLedgersEntries.class);
    }
    
}
