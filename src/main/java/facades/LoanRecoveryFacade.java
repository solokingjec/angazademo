
package facades;

import entities.LoanRecovery;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class LoanRecoveryFacade extends AbstractFacade<LoanRecovery> {

    @PersistenceContext(unitName = "SaccoManagement")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public LoanRecoveryFacade() {
        super(LoanRecovery.class);
    }

}
