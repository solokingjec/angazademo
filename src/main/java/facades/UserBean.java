/**
 * Copyright (c) 2014 Oracle and/or its affiliates. All rights reserved.
 *
 * You may not modify, use, reproduce, or distribute this software except in
 * compliance with  the terms of the License at:
 * http://java.net/projects/javaeetutorial/pages/BerkeleyLicense
 */
package facades;

import entities.AdminProfile;
import entities.Clerk;
import entities.Users;
import entities.Groups;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author markito
 */
@Stateless
public class UserBean extends AbstractFacade<Users> {

    @PersistenceContext(unitName = "SaccoManagement")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    /**
     * Create a new user verifying if the user already exists TODO: Create
     * custom exceptions ?
     *
     * @param user
     * @return
     */
    public boolean createUser(Users user) {
        // check if user exists
        if (getUserByUsername(user.getUsername()) == null) {
            super.create(user);
            return true;
        } else {
            return false;
        }
    }

    public List<Users> getAllPersons(String isolated) {
        return getEntityManager().createQuery("SELECT g  FROM Users g WHERE g.username !=:isolated").setParameter("isolated", isolated).getResultList();
    }

    public Users getUserByUsername(String uname) {
        Query createNamedQuery = getEntityManager().createNamedQuery("Users.findByUsername");

        createNamedQuery.setParameter("username", uname);

        if (createNamedQuery.getResultList().size() > 0) {
            return (Users) createNamedQuery.getSingleResult();
        } else {
            return null;
        }
    }

    public UserBean() {
        super(Users.class);
    }

    public void persistPersonGroup(Groups psg) {
        getEntityManager().persist(psg);
    }

    public Users getUserByUsername(String username, String password) {
        Query createNamedQuery = getEntityManager().createQuery("SELECT u FROM Users u WHERE u.username =:username AND u.password =:password").setParameter("username", username).setParameter("password", password);
        if (createNamedQuery.getResultList().size() > 0) {
            return (Users) createNamedQuery.getSingleResult();
        } else {
            return null;
        }
    }

    public void storeClerk(Clerk clerk) {
        getEntityManager().persist(clerk);
    }

    public void updateUserPassword(Users user) {
        getEntityManager().merge(user);
    }
    public void updateClerkDetail(Clerk c){
        getEntityManager().merge(c);
    }

    public void persitAdminProfile(AdminProfile profile) {
        getEntityManager().persist(profile);
    }

    public void updateProfile(AdminProfile adminProfile) {
        getEntityManager().merge(adminProfile);
    }

    public Groups findGroup(String selectedUserName) {
        return (Groups) getEntityManager().createQuery("SELECT g FROM Groups g WHERE g.username.username =:username").setParameter("username", selectedUserName).getSingleResult();
    }

    public AdminProfile findAdminUserName(String selectedUserName) {
        return (AdminProfile) getEntityManager().createQuery("SELECT g FROM AdminProfile g WHERE g.username.username =:username").setParameter("username", selectedUserName).getSingleResult();
    }

    public void upDateGroup(Groups g) {
        getEntityManager().merge(g);
    }

    public void deleteUser(String selectedUserName) {
        getEntityManager().createQuery("DELETE FROM Users AS s WHERE s.username =:selectedUserName").setParameter("selectedUserName", selectedUserName).executeUpdate();
    }

    public Clerk findClerk(String selectedUserName) {
        return (Clerk) getEntityManager().createQuery("SELECT g FROM Clerk g WHERE g.username.username =:username").setParameter("username", selectedUserName).getSingleResult();
    }

    public void updateProfileClerk(Clerk p) {
        getEntityManager().merge(p);
    }
}
