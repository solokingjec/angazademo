/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facades;

import entities.Bankings;
import entities.SaccoLedgerEntries;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author francis
 */
@Stateless
public class BankingsFacade extends AbstractFacade<Bankings> {
    @PersistenceContext(unitName = "SaccoManagement")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public BankingsFacade() {
        super(Bankings.class);
    }

    public void saveLedgerEntry(SaccoLedgerEntries sle) {
        getEntityManager().persist(sle);
    }
    
}
