/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facades;

import entities.Loan;
import entities.LoanScheduleTransactions;
import entities.LoanSchedules;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author francis
 */
@Stateless
public class LoanFacade extends AbstractFacade<Loan> {
    @PersistenceContext(unitName = "SaccoManagement")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public LoanFacade() {
        super(Loan.class);
    }

    public LoanSchedules getLoanSchedule(int random) {
        return (LoanSchedules) getEntityManager().createQuery("SELECT l FROM LoanSchedules l WHERE l.randomInt=:rand").setParameter("rand", random).getSingleResult();
    }

    public void mergeLoanSchedule(LoanSchedules item) {
        getEntityManager().merge(item);
    }

    public void storeTransaction(LoanScheduleTransactions lst) {
        getEntityManager().persist(lst);
    }

    public void storeLoanSchedule(LoanSchedules item) {
        getEntityManager().persist(item);
    }

    public List<Loan> findAllDefaulted() {
        return getEntityManager().createQuery("SELECT l FROM Loan l WHERE l.deffaultCount > 2 AND l.loanStatus = FALSE").getResultList();
    }
    
}
