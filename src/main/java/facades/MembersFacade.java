/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facades;

import controllers.util.AppConstants;
import entities.AccountTransitions;
import entities.BillTransactions;
import entities.CollectionTypes;
import entities.Collections;
import entities.DepositCash;
import entities.Guarantors;
import entities.Loan;
import entities.LoanRecovery;
import entities.LoanScheduleTransactions;
import entities.LoanSchedules;
import entities.LoanTypes;
import entities.MemberBills;
import entities.Members;
import entities.PaymentSummary;
import entities.PaymentSummaryItems;
import entities.SaccoLedgerEntries;
import entities.SaccoLedgers;
import entities.ScheduleStatus;
import entities.SystemSettings;
import entities.Withdrawal;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

/**
 *
 * @author francis
 */
@Stateless
public class MembersFacade extends AbstractFacade<Members> {

    @PersistenceContext(unitName = "SaccoManagement")
    private EntityManager em;
    public Iterable<Loan> getAllLoans;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public MembersFacade() {
        super(Members.class);
    }

    public SystemSettings getSettings(int i) {
        return (SystemSettings) getEntityManager().createNamedQuery("SystemSettings.findById").setParameter("id", i).getSingleResult();
    }

    public Members getByRandomInt(int rand) {
        return (Members) getEntityManager().createQuery("SELECT m FROM Members m WHERE m.randomInt=:rand").setParameter("rand", rand).getSingleResult();
    }

    public void persistBill(MemberBills mb) {
        getEntityManager().persist(mb);
    }

    public MemberBills getMemberBillByRandomInt(int randomInt2) {
        return (MemberBills) getEntityManager().createQuery("SELECT m FROM MemberBills m WHERE m.randomInt=:rand").setParameter("rand", randomInt2).getSingleResult();
    }

    public void storeBillTransaction(BillTransactions bt) {
        getEntityManager().persist(bt);
    }

    public void mergeMemberBill(MemberBills mb) {
        getEntityManager().merge(mb);
    }

    public void storeDeposit(DepositCash depo) {
        getEntityManager().persist(depo);
    }

    public void storeLoan(Loan loan) {
        AppConstants.logger("Store loan callled" + loan.getLoanStatus());
        getEntityManager().merge(loan);
        flustEt();
    }

    public Members findSelected(Members selected) {
        return (Members) getEntityManager().createNamedQuery("Members.findById").setParameter("id", selected.getId()).getSingleResult();
    }

    public Loan getLoanByRandom(int random) {
        return (Loan) getEntityManager().createQuery("SELECT l FROM Loan l WHERE l.randomInt=:rand").setParameter("rand", random).getSingleResult();
    }

    public void storeLoanSchedule(LoanSchedules item) {
        getEntityManager().persist(item);
    }

    public List<BillTransactions> getBillTransactionsBetweenDates(Members m) {
        return getEntityManager().createQuery("SELECT b FROM BillTransactions b WHERE b.billId.memberId=:selected AND b.statusBilled=:billed").setParameter("billed", false).setParameter("selected", m).getResultList();
    }

    public LoanSchedules getLoanSchedule(int random) {
        return (LoanSchedules) getEntityManager().createQuery("SELECT l FROM LoanSchedules l WHERE l.randomInt=:rand").setParameter("rand", random).getSingleResult();
    }

    public void storeTransaction(LoanScheduleTransactions lst) {
        getEntityManager().persist(lst);
        getEntityManager().flush();
        AppConstants.logger("Saved Transaction++++++++++++++" + lst.toString());
    }

    public void mergeLoanSchedule(LoanSchedules item) {
        AppConstants.logger("loan status:::mergeLoanSchedule" + item.getLoanId().getLoanStatus());
        getEntityManager().merge(item);
    }

    public Loan findLoan(Integer loanId) {
        return (Loan) getEntityManager().createQuery("SELECT l FROM Loan l WHERE l.loanId=:id").setParameter("id", loanId).getSingleResult();
    }

    public ScheduleStatus findScheduleStatus(int i) {
        return (ScheduleStatus) getEntityManager().createQuery("SELECT s FROM ScheduleStatus s WHERE s.idscheduleStatus=:id").setParameter("id", i).getSingleResult();
    }

    public List<LoanTypes> getLoanTypes() {
        return getEntityManager().createNamedQuery("LoanTypes.findAll").getResultList();
    }

    public Iterable<Collections> getMemberUnbilledCollections(Members m, Date sdate, Date edate) {
        return getEntityManager().createQuery("SELECT c FROM Collections c WHERE c.statusBilled=:billed AND c.memberId=:id AND c.dateCollected BETWEEN :sdate AND :edate").setParameter("edate", edate).setParameter("sdate", sdate).setParameter("billed", false).setParameter("id", m).getResultList();
    }

    public Iterable<Collections> getMemberUnbilledCollections(Members m) {
        getEntityManager().flush();
        return getEntityManager().createQuery("SELECT c FROM Collections c WHERE c.statusBilled=:billed AND c.memberId=:id").setParameter("billed", false).setParameter("id", m).getResultList();
    }

    public Loan findMemberActiveNormalLoan(Members m) {
        try {
            return (Loan) getEntityManager().createQuery("SELECT l FROM Loan l WHERE l.memeberId=:id AND l.loanStatus = FALSE AND l.loantype.id=:type AND l.pedding= FALSE").setParameter("id", m).setParameter("type", 2).getSingleResult();
        } catch (NoResultException e) {
            return new Loan();
        }
    }

    public Loan findMemberActiveEmergencyLoan(Members m) {
        try {
            return (Loan) getEntityManager().createQuery("SELECT l FROM Loan l WHERE l.memeberId=:id AND l.loanStatus = FALSE AND l.loantype.id=:type AND l.pedding = FALSE").setParameter("id", m).setParameter("type", 1).getSingleResult();
        } catch (NoResultException e) {
            return new Loan();
        }
    }

    public void mergeCollection(Collections c) {
        getEntityManager().merge(c);
    }

    public List<Collections> getCollectionsBetweenDates(Members selected, Date sdate, Date edate, CollectionTypes type) {
        return getEntityManager().createQuery("SELECT c FROM Collections c WHERE c.memberId=:selected AND c.dateCollected BETWEEN :sdate AND :edate AND c.collectionstype =:type").setParameter("type", type).setParameter("edate", edate).setParameter("sdate", sdate).setParameter("selected", selected).getResultList();
    }

    public void storeGuarator(Guarantors g) {
        getEntityManager().persist(g);
    }

    public void StoreAccTransition(AccountTransitions accTran) {
        getEntityManager().persist(accTran);
    }

    public Iterable<DepositCash> getDepositsBetweenDates(Members m) {
        return getEntityManager().createQuery("SELECT d FROM DepositCash d WHERE d.memberNo=:selected AND d.statusBilled=:billed").setParameter("billed", false).setParameter("selected", m).getResultList();
    }

    public void storePaymenySummary(PaymentSummary p) {
        getEntityManager().persist(p);
    }

    public void saveLedgerEntry(SaccoLedgerEntries sle) {
        getEntityManager().persist(sle);
        getEntityManager().flush();
    }

    public void storeWithdrawal(Withdrawal with) {
        getEntityManager().persist(with);
    }

    public void editAccTransition(AccountTransitions acc) {
        getEntityManager().merge(acc);
    }

    public List<BillTransactions> getBillTransactionsBetweenDates(Members m, Date sdate, Date edate) {
        return getEntityManager().createQuery("SELECT b FROM BillTransactions b WHERE b.billId.memberId=:selected AND b.datePaid BETWEEN :sdate AND :edate AND b.naration=:naration").setParameter("naration", "shares").setParameter("edate", edate).setParameter("sdate", sdate).setParameter("selected", m).getResultList();
    }

    public List<Withdrawal> getWithDrawalBetweenDates(Members m, Date sdate, Date edate) {
        return getEntityManager().createQuery("SELECT w FROM Withdrawal w WHERE w.memberId=:selected AND w.dateWhen BETWEEN :sdate AND :edate").setParameter("edate", edate).setParameter("sdate", sdate).setParameter("selected", m).getResultList();
    }

    public List<Withdrawal> getWithDrawalBetweenDates(Members m) {
        return getEntityManager().createQuery("SELECT w FROM Withdrawal w WHERE w.memberId=:selected AND w.statusBilled=:billed").setParameter("billed", false).setParameter("selected", m).getResultList();
    }

    public void storeCollections(Collections c) {
        getEntityManager().persist(c);
    }

    public Iterable<SaccoLedgers> getAllSaccoLedger() {
        return getEntityManager().createNamedQuery("SaccoLedgers.findAll").getResultList();
    }

    public void setAllLedgerEntriesToBilled(int id) {
        getEntityManager().createQuery("UPDATE SaccoLedgerEntries s SET s.statusBilled = true WHERE s.ledgerId.id = ?2 AND s.isGeneral = ?3").setParameter(3, false).setParameter(2, id).executeUpdate();
    }

    public void setAllBillTransactionsToBilled() {
        getEntityManager().createQuery("UPDATE BillTransactions b SET b.statusBilled = true").executeUpdate();
    }

    public void setAllDepositsBilled() {
        getEntityManager().createQuery("UPDATE DepositCash d SET d.statusBilled = true").executeUpdate();
    }

    public void setAllWithdrawalsBilled() {
        getEntityManager().createQuery("UPDATE Withdrawal w SET w.statusBilled = true").executeUpdate();
    }

    public List<Members> getAllMembers() {
        return getEntityManager().createNamedQuery("Members.findAll").getResultList();
    }

    public List<Collections> getCollections(int i, Loan loan) {
        return getEntityManager().createQuery("SELECT c FROM Collections c WHERE c.collectionstype.id = :collectionstype AND c.memberId = :memberId AND c.statusBilled =:statusBilled  ORDER BY c.id DESC").setParameter("statusBilled", false).setParameter("memberId", loan.getMemeberId()).setParameter("collectionstype", i).getResultList();
    }

    public void mergeCollection(Members memeberId, int type) {
        getEntityManager().createQuery("UPDATE Collections c SET c.statusBilled = TRUE WHERE c.memberId =:memberId AND c.collectionstype.id =:type").setParameter("memberId", memeberId).setParameter("type", type).executeUpdate();
    }

    public void mergeGuarantor(Guarantors g) {
        getEntityManager().merge(g);
    }

    public List<Collections> getCollectionsBetweenDates(Members selected, Date sdate, Date edate) {
        return getEntityManager().createQuery("SELECT c FROM Collections c WHERE c.memberId=:selected AND c.dateCollected BETWEEN :sdate AND :edate").setParameter("edate", edate).setParameter("sdate", sdate).setParameter("selected", selected).getResultList();
    }

    public PaymentSummaryItems getMembersCurrentItem(Members memeberId) {
        return (PaymentSummaryItems) getEntityManager().createQuery("SELECT p FROM PaymentSummaryItems p WHERE p.memberName =:m ORDER BY p.id DESC").setParameter("m", memeberId).setMaxResults(1).getSingleResult();
    }

    public void mergeSummaryItems(PaymentSummaryItems itemsummary) {
        getEntityManager().merge(itemsummary);
    }

    public Loan getPendingLoan(int i, Members m) {
        try {
            return (Loan) getEntityManager().createQuery("SELECT l FROM Loan l WHERE l.memeberId=:id AND l.loanStatus = FALSE AND l.loantype.id=:type AND l.pedding = TRUE").setParameter("id", m).setParameter("type", i).getSingleResult();
        } catch (NoResultException e) {
            return new Loan();
        }
    }

    public void flustEt() {
        getEntityManager().flush();
    }

    public LoanSchedules findLoanSchedule(LoanSchedules currentSchedule) {
        return getEntityManager().find(LoanSchedules.class, currentSchedule.getId());
    }

    public void mergeLoan(Loan loan) {
        getEntityManager().merge(loan);
    }

    public void setAllLoanNonPending() {
        getEntityManager().createQuery("UPDATE Loan AS l SET l.pedding = FALSE WHERE l.pedding = TRUE").executeUpdate();
    }

    public Iterable<Loan> getAllLoans() {
        return getEntityManager().createQuery("SELECT l FROM Loan l WHERE l.loanStatus = FALSE").getResultList();
    }

    public List<DepositCash> getAllMembersDepositsWithDrawable(Members selected) {
        return getEntityManager().createQuery("SELECT d FROM DepositCash d WHERE d.memberNo =:m AND d.isWithdrawable = TRUE ORDER BY d.id DESC").setParameter("m", selected).getResultList();
    }

    public List<DepositCash> getAllMembersDepositsNonWithDrawable(Members selected) {
        return getEntityManager().createQuery("SELECT d FROM DepositCash d WHERE d.memberNo =:m AND d.isWithdrawable = FALSE ORDER BY d.id DESC").setParameter("m", selected).getResultList();
    }

    public void storeLoanRecovery(LoanRecovery lr) {
        getEntityManager().persist(lr);
    }

    public void removeGuarantor(Guarantors g) {
        getEntityManager().createQuery("DELETE FROM Guarantors s WHERE s.id =:sid").setParameter("sid", g.getId()).executeUpdate();
    }

    public void deleteSchedule() {
    }

    public void deleteSchedule(LoanSchedules l) {
        getEntityManager().createQuery("DELETE FROM LoanScheduleTransactions s WHERE s.scheduleId =:sid").setParameter("sid", l).executeUpdate();
        getEntityManager().createQuery("DELETE FROM LoanSchedules s WHERE s.id =:sid").setParameter("sid", l.getId()).executeUpdate();
    }

    public void updateSummary(PaymentSummaryItems newg) {
        getEntityManager().merge(newg);
    }

    public int getTotalWithDrawableIn(Members selected) {
        try {
            return (int) (long) getEntityManager().createQuery("SELECT SUM(d.amount) FROM DepositCash d WHERE d.memberNo =:m AND d.isWithdrawable = TRUE AND d.amount > 0").setParameter("m", selected).getSingleResult();
        } catch (NullPointerException exception) {
            return 0;
        }
    }

    public int getTotalWithDrawableOut(Members selected) {
        try {
            return (int) (long) getEntityManager().createQuery("SELECT SUM(d.amount) FROM DepositCash d WHERE d.memberNo =:m AND d.isWithdrawable = TRUE AND d.amount < 0").setParameter("m", selected).getSingleResult();
        } catch (NullPointerException exception) {
            return 0;
        }
    }

    public int getTotalShares(Members selected) {
        try {
            return (int) (long) getEntityManager().createQuery("SELECT SUM(d.amount) FROM DepositCash d WHERE d.memberNo =:m AND d.isWithdrawable = FALSE").setParameter("m", selected).getSingleResult();
        } catch (NullPointerException exception) {
            return 0;
        }
    }

    public int getTotalSharesOut(Members selected) {
        try {
            return (int) (long) getEntityManager().createQuery("SELECT SUM(d.amount) FROM DepositCash d WHERE d.memberNo =:m AND d.isWithdrawable = FALSE AND d.amount < 0").setParameter("m", selected).getSingleResult();
        } catch (NullPointerException exception) {
            return 0;
        }
    }

    public int getTotalSharesIn(Members selected) {
        try {
            return (int) (long) getEntityManager().createQuery("SELECT SUM(d.amount) FROM DepositCash d WHERE d.memberNo =:m AND d.isWithdrawable = FALSE AND d.amount > 0").setParameter("m", selected).getSingleResult();
        } catch (NullPointerException exception) {
            return 0;
        }
    }

    public LoanSchedules getScheduleCurrent(Members selected) {
        try {
            return (LoanSchedules) getEntityManager().createQuery("SELECT l FROM LoanSchedules l WHERE l.loanId.memeberId =:m AND  l.loanId.loanStatus = FALSE AND l.statusCleared.idscheduleStatus = 2 AND l.loanId.loantype.id = 2")
                    .setParameter("m", selected).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

}
